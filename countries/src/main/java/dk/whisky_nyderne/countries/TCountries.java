package dk.whisky_nyderne.countries;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by hans on 30-09-17.
 * Module with flags of 257 countries, and function for sorting and selection flags.
 */

public class TCountries {
    private static int Number_of_Flags = 259;
    private static TCountries _instance;
    //    private String country.setDecription("";
//    private int country.setDrawableID(0;
//    private int Region = 0;
    private Context context;
    private ArrayList<TCountry> countrylist = new ArrayList<>();

    /**
     * first time module used, move flag to array
     */
    private void init(Context _context) {
        context = _context;
        TCountry country;
        for (int i = 1; i < Number_of_Flags; i++) {
            country = getInfo(i);
            country.setActive(true);
            country.setId(i);

            country.setFlag_ImageByte(getBitmap_as_ByteArray(_context, country.getDrawableID()));

            countrylist.add(country);
        }
    }

    /**
     * sorts it all once
     */
    private void order() {
        Collections.sort(countrylist, new Comparator<TCountry>() {
            public int compare(TCountry c1, TCountry c2) {
                return c1.getDecription().compareTo(c2.getDecription());
            }
        });
    }

    private TCountries(Context _context) {
        init(_context);
        order();
    }

    private void DisableAll() {
        for (int i = 1; i < countrylist.size(); i++) {
            countrylist.get(i).setActive(false);
        }
    }

    private void setActive(int id) {

        for (int i = 1; i < countrylist.size(); i++) {
            if (countrylist.get(i).getId() == id) {
                countrylist.get(i).setActive(true);
            }
        }

    }

    public void setFilter(List<Integer> items) {
        DisableAll();
        for (int i = 0; i < items.size(); i++) {
            setActive(items.get(i));
        }
    }

    public void setFilter(String filter) {
        DisableAll();
        for (int i = 0; i < countrylist.size(); i++) {
            if (countrylist.get(i).getDecription().toLowerCase().contains(filter.toLowerCase()))
                countrylist.get(i).setActive(true);
        }
    }

    /**
     * Retrieve list of all active flags soted
     *
     * @return integer
     */
    public ArrayList<TCountry> getList() {
        ArrayList<TCountry> list = new ArrayList<>();
        try {
            for (int i = 1; i < countrylist.size(); i++) {
                if (countrylist.get(i).isActive()) {
                    list.add(countrylist.get(i));
                }
            }
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        return list;
    }

    public synchronized static TCountries getInstance(Context _context) {
        if (_instance == null) {
            _instance = new TCountries(_context);
        }
        _instance.context = _context;
        return _instance;
    }


    public byte[] getBitmap_as_ByteArray(Context _context, int DrawableID) {
        byte[] bitMapData = null;

        if (DrawableID != 0) {
            Resources res = _context.getResources();
            Drawable drawable = res.getDrawable(DrawableID);

            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            bitMapData = stream.toByteArray();
        }
        return bitMapData;
    }

    public Bitmap getBitmap_as_Bitmap(Context _context, int DrawableID) {
        Bitmap bitmap = null;
        if (DrawableID != 0) {
            Resources res = _context.getResources();
            Drawable drawable = res.getDrawable(DrawableID);

            bitmap = ((BitmapDrawable) drawable).getBitmap();
        }
        return bitmap;
    }


    public TCountry getInfo(int id) {
        TCountry country = new TCountry();
        country.setRegion_ID(0);

        switch (id) {
            case 1: {
                country.setDecription(context.getString(R.string.Afghanistan));
                country.setDrawableID(R.drawable.af_flag);
                break;
            }
            case 2: {
                country.setDecription(context.getString(R.string.Akrotiri));
                country.setDrawableID(R.drawable.ax_flag);
                break;
            }

            case 3: {
                country.setDecription(context.getString(R.string.Albania));
                country.setDrawableID(R.drawable.al_flag);
                break;
            }
            case 4: {
                country.setDecription(context.getString(R.string.Algeria));
                country.setDrawableID(R.drawable.ag_flag);
                break;
            }
            case 5: {
                country.setDecription(context.getString(R.string.American_Samoa));
                country.setDrawableID(R.drawable.aq_flag);
                break;
            }
            case 6: {
                country.setDecription(context.getString(R.string.Andorra));
                country.setDrawableID(R.drawable.an_flag);
                break;
            }
            case 7: {
                country.setDecription(context.getString(R.string.Angola));
                country.setDrawableID(R.drawable.ao_flag);
                break;
            }
            case 8: {
                country.setDecription(context.getString(R.string.Anguilla));
                country.setDrawableID(R.drawable.av_flag);
                break;
            }
            case 9: {
                country.setDecription(context.getString(R.string.Antarctica));
                country.setDrawableID(R.drawable.ay_flag);
                break;
            }
            case 10: {
                country.setDecription(context.getString(R.string.Antigua_and_Barbuda));
                country.setDrawableID(R.drawable.ac_flag);
                break;
            }
            case 11: {
                country.setDecription(context.getString(R.string.Argentina));
                country.setDrawableID(R.drawable.ar_flag);
                break;
            }
            case 12: {
                country.setDecription(context.getString(R.string.Armenia));
                country.setDrawableID(R.drawable.ao_flag);
                break;
            }
            case 13: {
                country.setDecription(context.getString(R.string.Aruba));
                country.setDrawableID(R.drawable.aa_flag);
                break;
            }
            case 14: {
                country.setDecription(context.getString(R.string.Ashmore_and_Cartier_Islands));
                country.setDrawableID(R.drawable.at_flag);
                break;
            }
            case 15: {
                country.setDecription(context.getString(R.string.Australia));
                country.setDrawableID(R.drawable.as_flag);
                break;
            }
            case 16: {
                country.setDecription(context.getString(R.string.Austria));
                country.setDrawableID(R.drawable.au_flag);
                break;
            }
            case 17: {
                country.setDecription(context.getString(R.string.Azerbaijan));
                country.setDrawableID(R.drawable.aj_flag);
                break;
            }
            case 18: {
                country.setDecription(context.getString(R.string.Bahamas_The));
                country.setDrawableID(R.drawable.bf_flag);
                break;
            }
            case 19: {
                country.setDecription(context.getString(R.string.Bahrain));
                country.setDrawableID(R.drawable.ba_flag);
                break;
            }
            case 20: {
                country.setDecription(context.getString(R.string.Barbados));
                country.setDrawableID(R.drawable.bb_flag);
                break;
            }
            case 21: {
                country.setDecription(context.getString(R.string.Bangladesh));
                country.setDrawableID(R.drawable.bg_flag);
                break;
            }
            case 22: {
                country.setDecription(context.getString(R.string.Bassas_da_India));
                country.setDrawableID(R.drawable.bs_flag);
                break;
            }
            case 23: {
                country.setDecription(context.getString(R.string.Belarus));
                country.setDrawableID(R.drawable.bo_flag);
                break;
            }
            case 24: {
                country.setDecription(context.getString(R.string.Belgium));
                country.setDrawableID(R.drawable.be_flag);
                break;
            }
            case 25: {
                country.setDecription(context.getString(R.string.Belize));
                country.setDrawableID(R.drawable.bh_flag);
                break;
            }
            case 26: {
                country.setDecription(context.getString(R.string.Benin));
                country.setDrawableID(R.drawable.bn_flag);
                break;
            }
            case 27: {
                country.setDecription(context.getString(R.string.Bermuda));
                country.setDrawableID(R.drawable.bd_flag);
                break;
            }
            case 28: {
                country.setDecription(context.getString(R.string.Bhutan));
                country.setDrawableID(R.drawable.bt_flag);
                break;
            }
            case 29: {
                country.setDecription(context.getString(R.string.Bolivia));
                country.setDrawableID(R.drawable.bl_flag);
                break;
            }
            case 30: {
                country.setDecription(context.getString(R.string.Bosnia_and_Herzegovina));
                country.setDrawableID(R.drawable.bk_flag);
                break;
            }
            case 31: {
                country.setDecription(context.getString(R.string.Botswana));
                country.setDrawableID(R.drawable.bc_flag);
                break;
            }
            case 32: {
                country.setDecription(context.getString(R.string.Bouvet_Island));
                country.setDrawableID(R.drawable.bv_flag);
                break;
            }
            case 33: {
                country.setDecription(context.getString(R.string.Brazil));
                country.setDrawableID(R.drawable.br_flag);
                break;
            }
            case 34: {
                country.setDecription(context.getString(R.string.British_Indian_Ocean_Territory));
                country.setDrawableID(R.drawable.io_flag);
                break;
            }
            case 35: {
                country.setDecription(context.getString(R.string.British_Virgin_Islands));
                country.setDrawableID(R.drawable.vi_flag);
                break;
            }
            case 36: {
                country.setDecription(context.getString(R.string.Brunei));
                country.setDrawableID(R.drawable.bx_flag);
                break;
            }
            case 37: {
                country.setDecription(context.getString(R.string.Bulgaria));
                country.setDrawableID(R.drawable.bu_flag);
                break;
            }
            case 38: {
                country.setDecription(context.getString(R.string.Burkina_Faso));
                country.setDrawableID(R.drawable.uv_flag);
                break;
            }
            case 39: {
                country.setDecription(context.getString(R.string.Burma));
                country.setDrawableID(R.drawable.bm_flag);
                break;
            }
            case 40: {
                country.setDecription(context.getString(R.string.Burundi));
                country.setDrawableID(R.drawable.by_flag);
                break;
            }
            case 41: {
                country.setDecription(context.getString(R.string.Cambodia));
                country.setDrawableID(R.drawable.cb_flag);
                break;
            }
            case 42: {
                country.setDecription(context.getString(R.string.Cameroon));
                country.setDrawableID(R.drawable.cm_flag);
                break;
            }
            case 43: {
                country.setDecription(context.getString(R.string.Canada));
                country.setDrawableID(R.drawable.ca_flag);
                break;
            }
            case 44: {
                country.setDecription(context.getString(R.string.Cape_Verde));
                country.setDrawableID(R.drawable.cv_flag);
                break;
            }
            case 45: {
                country.setDecription(context.getString(R.string.Cayman_Islands));
                country.setDrawableID(R.drawable.cj_flag);
                break;
            }
            case 46: {
                country.setDecription(context.getString(R.string.Central_African_Republic));
                country.setDrawableID(R.drawable.ct_flag);
                break;
            }
            case 47: {
                country.setDecription(context.getString(R.string.Chad));
                country.setDrawableID(R.drawable.cd_flag);
                break;
            }
            case 48: {
                country.setDecription(context.getString(R.string.Chile));
                country.setDrawableID(R.drawable.ci_flag);
                break;
            }
            case 49: {
                country.setDecription(context.getString(R.string.China));
                country.setDrawableID(R.drawable.ch_flag);
                break;
            }
            case 50: {
                country.setDecription(context.getString(R.string.Christmas_Island));
                country.setDrawableID(R.drawable.kt_flag);
                break;
            }
            case 51: {
                country.setDecription(context.getString(R.string.Clipperton_Island));
                country.setDrawableID(R.drawable.ip_flag);
                break;
            }
            case 52: {
                country.setDecription(context.getString(R.string.Cocos_Keeling_Islands));
                country.setDrawableID(R.drawable.ck_flag);
                break;
            }
            case 53: {
                country.setDecription(context.getString(R.string.Colombia));
                country.setDrawableID(R.drawable.co_flag);
                break;
            }
            case 54: {
                country.setDecription(context.getString(R.string.Comoros));
                country.setDrawableID(R.drawable.cn_flag);
                break;
            }
            case 55: {
                country.setDecription(context.getString(R.string.Congo_Democratic_Republic_of_the));
                country.setDrawableID(R.drawable.cg_flag);
                break;
            }
            case 56: {
                country.setDecription(context.getString(R.string.Congo_Republic_of_the));
                country.setDrawableID(R.drawable.cf_flag);
                break;
            }
            case 57: {
                country.setDecription(context.getString(R.string.Cook_Islands));
                country.setDrawableID(R.drawable.cw_flag);
                break;
            }
            case 58: {
                country.setDecription(context.getString(R.string.Coral_Sea_Islands));
                country.setDrawableID(R.drawable.cr_flag);
                break;
            }
            case 59: {
                country.setDecription(context.getString(R.string.Costa_Rica));
                country.setDrawableID(R.drawable.cs_flag);
                break;
            }
            case 60: {
                country.setDecription(context.getString(R.string.Cote_d_Ivoire));
                country.setDrawableID(R.drawable.iv_flag);
                break;
            }
            case 61: {
                country.setDecription(context.getString(R.string.Croatia));
                country.setDrawableID(R.drawable.hr_flag);
                break;
            }
            case 62: {
                country.setDecription(context.getString(R.string.Cuba));
                country.setDrawableID(R.drawable.cu_flag);
                break;
            }
            case 63: {
                country.setDecription(context.getString(R.string.Cyprus));
                country.setDrawableID(R.drawable.cy_flag);
                break;
            }
            case 64: {
                country.setDecription(context.getString(R.string.Czech_Republic));
                country.setDrawableID(R.drawable.ez_flag);
                break;
            }
            case 65: {
                country.setDecription(context.getString(R.string.Denmark));
                country.setDrawableID(R.drawable.da_flag);
                break;
            }
            case 66: {
                country.setDecription(context.getString(R.string.Dhekelia));
                country.setDrawableID(R.drawable.dx_flag);
                break;
            }
            case 67: {
                country.setDecription(context.getString(R.string.Djibouti));
                country.setDrawableID(R.drawable.dj_flag);
                break;
            }
            case 68: {
                country.setDecription(context.getString(R.string.Dominica));
                country.setDrawableID(R.drawable.do_flag);
                break;
            }
            case 69: {
                country.setDecription(context.getString(R.string.Dominican_Republic));
                country.setDrawableID(R.drawable.dr_flag);
                break;
            }
            case 70: {
                country.setDecription(context.getString(R.string.Ecuador));
                country.setDrawableID(R.drawable.ec_flag);
                break;
            }
            case 71: {
                country.setDecription(context.getString(R.string.Egypt));
                country.setDrawableID(R.drawable.eg_flag);
                break;
            }
            case 72: {
                country.setDecription(context.getString(R.string.El_Salvador));
                country.setDrawableID(R.drawable.es_flag);
                break;
            }
            case 73: {
                country.setDecription(context.getString(R.string.Equatorial_Guinea));
                country.setDrawableID(R.drawable.ek_flag);
                break;
            }
            case 74: {
                country.setDecription(context.getString(R.string.Eritrea));
                country.setDrawableID(R.drawable.er_flag);
                break;
            }
            case 75: {
                country.setDecription(context.getString(R.string.Estonia));
                country.setDrawableID(R.drawable.en_flag);
                break;
            }
            case 76: {
                country.setDecription(context.getString(R.string.Ethiopia));
                country.setDrawableID(R.drawable.et_flag);
                break;
            }
            case 77: {
                country.setDecription(context.getString(R.string.Europa_Island));
                country.setDrawableID(R.drawable.eu_flag);
                break;
            }
            case 78: {
                country.setDecription(context.getString(R.string.Falkland_Islands_Islas_Malvinas));
                country.setDrawableID(R.drawable.fk_flag);
                break;
            }
            case 79: {
                country.setDecription(context.getString(R.string.Faroe_Islands));
                country.setDrawableID(R.drawable.fo_flag);
                break;
            }
            case 80: {
                country.setDecription(context.getString(R.string.Fiji));
                country.setDrawableID(R.drawable.fj_flag);
                break;
            }
            case 81: {
                country.setDecription(context.getString(R.string.Finland));
                country.setDrawableID(R.drawable.fi_flag);
                break;
            }
            case 82: {
                country.setDecription(context.getString(R.string.France));
                country.setDrawableID(R.drawable.fr_flag);
                break;
            }
            case 83: {
                country.setDecription(context.getString(R.string.French_Guiana));
                country.setDrawableID(R.drawable.fg_flag);
                break;
            }
            case 84: {
                country.setDecription(context.getString(R.string.French_Polynesia));
                country.setDrawableID(R.drawable.fp_flag);
                break;
            }
            case 85: {
                country.setDecription(context.getString(R.string.French_Southern_and_Antarctic_Lands));
                country.setDrawableID(R.drawable.fs_flag);
                break;
            }
            case 86: {
                country.setDecription(context.getString(R.string.Gabon));
                country.setDrawableID(R.drawable.gb_flag);
                break;
            }
            case 87: {
                country.setDecription(context.getString(R.string.Gambia_The));
                country.setDrawableID(R.drawable.ga_flag);
                break;
            }
            case 88: {
                country.setDecription(context.getString(R.string.Gaza_Strip));
                country.setDrawableID(R.drawable.gz_flag);
                break;
            }
            case 89: {
                country.setDecription(context.getString(R.string.Georgia));
                country.setDrawableID(R.drawable.gg_flag);
                break;
            }
            case 90: {
                country.setDecription(context.getString(R.string.Germany));
                country.setDrawableID(R.drawable.gm_flag);
                break;
            }
            case 91: {
                country.setDecription(context.getString(R.string.Ghana));
                country.setDrawableID(R.drawable.gh_flag);
                break;
            }
            case 92: {
                country.setDecription(context.getString(R.string.Gibraltar));
                country.setDrawableID(R.drawable.gi_flag);
                break;
            }
            case 93: {
                country.setDecription(context.getString(R.string.Glorioso_Islands));
                country.setDrawableID(R.drawable.go_flag);
                break;
            }
            case 94: {
                country.setDecription(context.getString(R.string.Greece));
                country.setDrawableID(R.drawable.gr_flag);
                break;
            }
            case 95: {
                country.setDecription(context.getString(R.string.Greenland));
                country.setDrawableID(R.drawable.gl_flag);
                break;
            }
            case 96: {
                country.setDecription(context.getString(R.string.Grenada));
                country.setDrawableID(R.drawable.gj_flag);
                break;
            }
            case 97: {
                country.setDecription(context.getString(R.string.Guadeloupe));
                country.setDrawableID(R.drawable.gp_flag);
                break;
            }
            case 98: {
                country.setDecription(context.getString(R.string.Guam));
                country.setDrawableID(R.drawable.gq_flag);
                break;
            }
            case 99: {
                country.setDecription(context.getString(R.string.Guatemala));
                country.setDrawableID(R.drawable.gt_flag);
                break;
            }
            case 100: {
                country.setDecription(context.getString(R.string.Guernsey));
                country.setDrawableID(R.drawable.gk_flag);
                break;
            }
            case 101: {
                country.setDecription(context.getString(R.string.Guinea));
                country.setDrawableID(R.drawable.gv_flag);
                break;
            }
            case 102: {
                country.setDecription(context.getString(R.string.Guinea_Bissau));
                country.setDrawableID(R.drawable.pu_flag);
                break;
            }
            case 103: {
                country.setDecription(context.getString(R.string.Guyana));
                country.setDrawableID(R.drawable.gy_flag);
                break;
            }
            case 104: {
                country.setDecription(context.getString(R.string.Haiti));
                country.setDrawableID(R.drawable.ha_flag);
                break;
            }
            case 105: {
                country.setDecription(context.getString(R.string.Heard_Island_and_McDonald_Islands));
                country.setDrawableID(R.drawable.hm_flag);
                break;
            }
            case 106: {
                country.setDecription(context.getString(R.string.Holy_See_Vatican_City));
                country.setDrawableID(R.drawable.vt_flag);
                break;
            }
            case 107: {
                country.setDecription(context.getString(R.string.Honduras));
                country.setDrawableID(R.drawable.ho_flag);
                break;
            }
            case 108: {
                country.setDecription(context.getString(R.string.Hong_Kong));
                country.setDrawableID(R.drawable.hk_flag);
                break;
            }
            case 109: {
                country.setDecription(context.getString(R.string.Hungary));
                country.setDrawableID(R.drawable.hu_flag);
                break;
            }
            case 110: {
                country.setDecription(context.getString(R.string.Iceland));
                country.setDrawableID(R.drawable.ic_flag);
                break;
            }
            case 111: {
                country.setDecription(context.getString(R.string.India));
                country.setDrawableID(R.drawable.in_flag);
                break;
            }
            case 112: {
                country.setDecription(context.getString(R.string.Indonesia));
                country.setDrawableID(R.drawable.id_flag);
                break;
            }
            case 113: {
                country.setDecription(context.getString(R.string.Iran));
                country.setDrawableID(R.drawable.ir_flag);
                break;
            }
            case 114: {
                country.setDecription(context.getString(R.string.Iraq));
                country.setDrawableID(R.drawable.iz_flag);
                break;
            }
            case 115: {
                country.setDecription(context.getString(R.string.Ireland));
                country.setDrawableID(R.drawable.ei_flag);
                break;
            }
            case 116: {
                country.setDecription(context.getString(R.string.Isle_of_Man));
                country.setDrawableID(R.drawable.im_flag);
                break;
            }
            case 117: {
                country.setDecription(context.getString(R.string.Israel));
                country.setDrawableID(R.drawable.is_flag);
                break;
            }
            case 118: {
                country.setDecription(context.getString(R.string.Italy));
                country.setDrawableID(R.drawable.it_flag);
                break;
            }
            case 119: {
                country.setDecription(context.getString(R.string.Jamaica));
                country.setDrawableID(R.drawable.jm_flag);
                break;
            }
            case 120: {
                country.setDecription(context.getString(R.string.Jan_Mayen));
                country.setDrawableID(R.drawable.jn_flag);
                break;
            }
            case 121: {
                country.setDecription(context.getString(R.string.Japan));
                country.setDrawableID(R.drawable.ja_flag);
                break;
            }
            case 122: {
                country.setDecription(context.getString(R.string.Jersey));
                country.setDrawableID(R.drawable.je_flag);
                break;
            }
            case 123: {
                country.setDecription(context.getString(R.string.Jordan));
                country.setDrawableID(R.drawable.jo_flag);
                break;
            }
            case 124: {
                country.setDecription(context.getString(R.string.Juan_de_Nova_Island));
                country.setDrawableID(R.drawable.ju_flag);
                break;
            }
            case 125: {
                country.setDecription(context.getString(R.string.Kazakhstan));
                country.setDrawableID(R.drawable.kz_flag);
                break;
            }
            case 126: {
                country.setDecription(context.getString(R.string.Kenya));
                country.setDrawableID(R.drawable.ke_flag);
                break;
            }
            case 127: {
                country.setDecription(context.getString(R.string.Kiribati));
                country.setDrawableID(R.drawable.kr_flag);
                break;
            }
            case 128: {
                country.setDecription(context.getString(R.string.Korea_North));
                country.setDrawableID(R.drawable.kn_flag);
                break;
            }
            case 129: {
                country.setDecription(context.getString(R.string.Korea_South));
                country.setDrawableID(R.drawable.ks_flag);
                break;
            }
            case 130: {
                country.setDecription(context.getString(R.string.Kuwait));
                country.setDrawableID(R.drawable.ku_flag);
                break;
            }
            case 131: {
                country.setDecription(context.getString(R.string.Kyrgyzstan));
                country.setDrawableID(R.drawable.kg_flag);
                break;
            }
            case 132: {
                country.setDecription(context.getString(R.string.Laos));
                country.setDrawableID(R.drawable.la_flag);
                break;
            }
            case 133: {
                country.setDecription(context.getString(R.string.Latvia));
                country.setDrawableID(R.drawable.lg_flag);
                break;
            }
            case 134: {
                country.setDecription(context.getString(R.string.Lebanon));
                country.setDrawableID(R.drawable.le_flag);
                break;
            }
            case 135: {
                country.setDecription(context.getString(R.string.Lesotho));
                country.setDrawableID(R.drawable.lt_flag);
                break;
            }
            case 136: {
                country.setDecription(context.getString(R.string.Liberia));
                country.setDrawableID(R.drawable.li_flag);
                break;
            }
            case 137: {
                country.setDecription(context.getString(R.string.Libya));
                country.setDrawableID(R.drawable.ly_flag);
                break;
            }
            case 138: {
                country.setDecription(context.getString(R.string.Liechtenstein));
                country.setDrawableID(R.drawable.ls_flag);
                break;
            }
            case 139: {
                country.setDecription(context.getString(R.string.Lithuania));
                country.setDrawableID(R.drawable.lh_flag);
                break;
            }
            case 140: {
                country.setDecription(context.getString(R.string.Luxembourg));
                country.setDrawableID(R.drawable.lu_flag);
                break;
            }
            case 141: {
                country.setDecription(context.getString(R.string.Luxembourg));
                country.setDrawableID(R.drawable.mc_flag);
                break;
            }
            case 142: {
                country.setDecription(context.getString(R.string.Macedonia));
                country.setDrawableID(R.drawable.mk_flag);
                break;
            }
            case 143: {
                country.setDecription(context.getString(R.string.Madagascar));
                country.setDrawableID(R.drawable.ma_flag);
                break;
            }
            case 144: {
                country.setDecription(context.getString(R.string.Malawi));
                country.setDrawableID(R.drawable.mi_flag);
                break;
            }
            case 145: {
                country.setDecription(context.getString(R.string.Malaysia));
                country.setDrawableID(R.drawable.my_flag);
                break;
            }
            case 146: {
                country.setDecription(context.getString(R.string.Maldives));
                country.setDrawableID(R.drawable.mv_flag);
                break;
            }
            case 147: {
                country.setDecription(context.getString(R.string.Mali));
                country.setDrawableID(R.drawable.ml_flag);
                break;
            }
            case 148: {
                country.setDecription(context.getString(R.string.Malta));
                country.setDrawableID(R.drawable.mt_flag);
                break;
            }
            case 149: {
                country.setDecription(context.getString(R.string.Marshall_Islands));
                country.setDrawableID(R.drawable.rm_flag);
                break;
            }
            case 150: {
                country.setDecription(context.getString(R.string.Martinique));
                country.setDrawableID(R.drawable.mb_flag);
                break;
            }
            case 151: {
                country.setDecription(context.getString(R.string.Mauritania));
                country.setDrawableID(R.drawable.mr_flag);
                break;
            }
            case 152: {
                country.setDecription(context.getString(R.string.Mauritius));
                country.setDrawableID(R.drawable.mp_flag);
                break;
            }
            case 153: {
                country.setDecription(context.getString(R.string.Mayotte));
                country.setDrawableID(R.drawable.mf_flag);
                break;
            }
            case 154: {
                country.setDecription(context.getString(R.string.Mexico));
                country.setDrawableID(R.drawable.mx_flag);
                break;
            }
            case 155: {
                country.setDecription(context.getString(R.string.Micronesia));
                country.setDrawableID(R.drawable.fm_flag);
                break;
            }
            case 156: {
                country.setDecription(context.getString(R.string.Moldova));
                country.setDrawableID(R.drawable.md_flag);
                break;
            }
            case 157: {
                country.setDecription(context.getString(R.string.Monaco));
                country.setDrawableID(R.drawable.mn_flag);
                break;
            }
            case 158: {
                country.setDecription(context.getString(R.string.Mongolia));
                country.setDrawableID(R.drawable.mg_flag);
                break;
            }
            case 159: {
                country.setDecription(context.getString(R.string.Montserrat));
                country.setDrawableID(R.drawable.mh_flag);
                break;
            }
            case 160: {
                country.setDecription(context.getString(R.string.Morocco));
                country.setDrawableID(R.drawable.mo_flag);
                break;
            }
            case 161: {
                country.setDecription(context.getString(R.string.Mozambique));
                country.setDrawableID(R.drawable.mz_flag);
                break;
            }
            case 162: {
                country.setDecription(context.getString(R.string.Namibia));
                country.setDrawableID(R.drawable.wa_flag);
                break;
            }
            case 163: {
                country.setDecription(context.getString(R.string.Nauru));
                country.setDrawableID(R.drawable.nr_flag);
                break;
            }
            case 164: {
                country.setDecription(context.getString(R.string.Navassa_Island));
                country.setDrawableID(R.drawable.bq_flag);
                break;
            }
            case 165: {
                country.setDecription(context.getString(R.string.Nepal));
                country.setDrawableID(R.drawable.np_flag);
                break;
            }
            case 166: {
                country.setDecription(context.getString(R.string.Netherlands));
                country.setDrawableID(R.drawable.nl_flag);
                break;
            }
            case 167: {
                country.setDecription(context.getString(R.string.Netherlands_Antilles));
                country.setDrawableID(R.drawable.nt_flag);
                break;
            }
            case 168: {
                country.setDecription(context.getString(R.string.New_Caledonia));
                country.setDrawableID(R.drawable.nc_flag);
                break;
            }
            case 169: {
                country.setDecription(context.getString(R.string.New_Zealand));
                country.setDrawableID(R.drawable.nz_flag);
                break;
            }
            case 170: {
                country.setDecription(context.getString(R.string.Nicaragua));
                country.setDrawableID(R.drawable.nu_flag);
                break;
            }
            case 171: {
                country.setDecription(context.getString(R.string.Niger));
                country.setDrawableID(R.drawable.ng_flag);
                break;
            }
            case 172: {
                country.setDecription(context.getString(R.string.Nigeria));
                country.setDrawableID(R.drawable.ni_flag);
                break;
            }
            case 173: {
                country.setDecription(context.getString(R.string.Niue));
                country.setDrawableID(R.drawable.ne_flag);
                break;
            }
            case 174: {
                country.setDecription(context.getString(R.string.Norfolk_Island));
                country.setDrawableID(R.drawable.nf_flag);
                break;
            }
            case 175: {
                country.setDecription(context.getString(R.string.Northern_Mariana_Islands));
                country.setDrawableID(R.drawable.cq_flag);
                break;
            }
            case 176: {
                country.setDecription(context.getString(R.string.Norway));
                country.setDrawableID(R.drawable.no_flag);
                break;
            }
            case 177: {
                country.setDecription(context.getString(R.string.Oman));
                country.setDrawableID(R.drawable.mu_flag);
                break;
            }
            case 178: {
                country.setDecription(context.getString(R.string.Pakistan));
                country.setDrawableID(R.drawable.pk_flag);
                break;
            }
            case 179: {
                country.setDecription(context.getString(R.string.Palau));
                country.setDrawableID(R.drawable.ps_flag);
                break;
            }
            case 180: {
                country.setDecription(context.getString(R.string.Panama));
                country.setDrawableID(R.drawable.pm_flag);
                break;
            }
            case 181: {
                country.setDecription(context.getString(R.string.Papua_New_Guinea));
                country.setDrawableID(R.drawable.pp_flag);
                break;
            }
            case 182: {
                country.setDecription(context.getString(R.string.Paracel_Islands));
                country.setDrawableID(R.drawable.pf_flag);
                break;
            }
            case 183: {
                country.setDecription(context.getString(R.string.Paraguay));
                country.setDrawableID(R.drawable.pa_flag);
                break;
            }
            case 184: {
                country.setDecription(context.getString(R.string.Peru));
                country.setDrawableID(R.drawable.pe_flag);
                break;
            }
            case 185: {
                country.setDecription(context.getString(R.string.Philippines));
                country.setDrawableID(R.drawable.rp_flag);
                break;
            }
            case 186: {
                country.setDecription(context.getString(R.string.Pitcairn_Islands));
                country.setDrawableID(R.drawable.pc_flag);
                break;
            }
            case 187: {
                country.setDecription(context.getString(R.string.Poland));
                country.setDrawableID(R.drawable.pl_flag);
                break;
            }
            case 188: {
                country.setDecription(context.getString(R.string.Portugal));
                country.setDrawableID(R.drawable.po_flag);
                break;
            }
            case 189: {
                country.setDecription(context.getString(R.string.Puerto_Rico));
                country.setDrawableID(R.drawable.rq_flag);
                break;
            }
            case 190: {
                country.setDecription(context.getString(R.string.Qatar));
                country.setDrawableID(R.drawable.qa_flag);
                break;
            }
            case 191: {
                country.setDecription(context.getString(R.string.Reunion));
                country.setDrawableID(R.drawable.re_flag);
                break;
            }
            case 192: {
                country.setDecription(context.getString(R.string.Romania));
                country.setDrawableID(R.drawable.ro_flag);
                break;
            }
            case 193: {
                country.setDecription(context.getString(R.string.Russia));
                country.setDrawableID(R.drawable.rs_flag);
                break;
            }
            case 194: {
                country.setDecription(context.getString(R.string.Rwanda));
                country.setDrawableID(R.drawable.rw_flag);
                break;
            }
            case 195: {
                country.setDecription(context.getString(R.string.Saint_Helena));
                country.setDrawableID(R.drawable.sh_flag);
                break;
            }
            case 196: {
                country.setDecription(context.getString(R.string.Saint_Kitts_and_Nevis));
                country.setDrawableID(R.drawable.sc_flag);
                break;
            }
            case 197: {
                country.setDecription(context.getString(R.string.Saint_Lucia));
                country.setDrawableID(R.drawable.st_flag);
                break;
            }
            case 198: {
                country.setDecription(context.getString(R.string.Saint_Pierre_and_Miquelon));
                country.setDrawableID(R.drawable.sb_flag);
                break;
            }
            case 199: {
                country.setDecription(context.getString(R.string.Saint_Vincent_and_the_Grenadines));
                country.setDrawableID(R.drawable.vc_flag);
                break;
            }
            case 200: {
                country.setDecription(context.getString(R.string.Samoa));
                country.setDrawableID(R.drawable.ws_flag);
                break;
            }
            case 201: {
                country.setDecription(context.getString(R.string.San_Marino));
                country.setDrawableID(R.drawable.sm_flag);
                break;
            }
            case 202: {
                country.setDecription(context.getString(R.string.Sao_Tome_and_Principe));
                country.setDrawableID(R.drawable.tp_flag);
                break;
            }
            case 203: {
                country.setDecription(context.getString(R.string.Saudi_Arabia));
                country.setDrawableID(R.drawable.sa_flag);
                break;
            }
            case 204: {
                country.setDecription(context.getString(R.string.Senegal));
                country.setDrawableID(R.drawable.sg_flag);
                break;
            }
            case 205: {
                country.setDecription(context.getString(R.string.Serbia_and_Montenegro));
                country.setDrawableID(R.drawable.yi_flag);
                break;
            }
            case 206: {
                country.setDecription(context.getString(R.string.Seychelles));
                country.setDrawableID(R.drawable.se_flag);
                break;
            }
            case 207: {
                country.setDecription(context.getString(R.string.Sierra_Leone));
                country.setDrawableID(R.drawable.sl_flag);
                break;
            }
            case 208: {
                country.setDecription(context.getString(R.string.Singapore));
                country.setDrawableID(R.drawable.sn_flag);
                break;
            }
            case 209: {
                country.setDecription(context.getString(R.string.Slovakia));
                country.setDrawableID(R.drawable.lo_flag);
                break;
            }
            case 210: {
                country.setDecription(context.getString(R.string.Slovenia));
                country.setDrawableID(R.drawable.si_flag);
                break;
            }
            case 211: {
                country.setDecription(context.getString(R.string.Solomon_Islands));
                country.setDrawableID(R.drawable.bp_flag);
                break;
            }
            case 212: {
                country.setDecription(context.getString(R.string.Somalia));
                country.setDrawableID(R.drawable.so_flag);
                break;
            }
            case 213: {
                country.setDecription(context.getString(R.string.South_Africa));
                country.setDrawableID(R.drawable.sf_flag);
                break;
            }
            case 214: {
                country.setDecription(context.getString(R.string.South_Georgia_and_the_South_Sandwich_Islands));
                country.setDrawableID(R.drawable.sx_flag);
                break;
            }
            case 215: {
                country.setDecription(context.getString(R.string.Spain));
                country.setDrawableID(R.drawable.sp_flag);
                break;
            }
            case 216: {
                country.setDecription(context.getString(R.string.Spratly_Islands));
                country.setDrawableID(R.drawable.pg_flag);
                break;
            }
            case 217: {
                country.setDecription(context.getString(R.string.Sri_Lanka));
                country.setDrawableID(R.drawable.ce_flag);
                break;
            }
            case 218: {
                country.setDecription(context.getString(R.string.Sudan));
                country.setDrawableID(R.drawable.su_flag);
                break;
            }
            case 219: {
                country.setDecription(context.getString(R.string.Suriname));
                country.setDrawableID(R.drawable.ns_flag);
                break;
            }
            case 220: {
                country.setDecription(context.getString(R.string.Svalbard));
                country.setDrawableID(R.drawable.sv_flag);
                break;
            }
            case 221: {
                country.setDecription(context.getString(R.string.Swaziland));
                country.setDrawableID(R.drawable.wz_flag);
                break;
            }
            case 222: {
                country.setDecription(context.getString(R.string.Sweden));
                country.setDrawableID(R.drawable.sw_flag);
                break;
            }
            case 223: {
                country.setDecription(context.getString(R.string.Switzerland));
                country.setDrawableID(R.drawable.sz_flag);
                break;
            }
            case 224: {
                country.setDecription(context.getString(R.string.Syria));
                country.setDrawableID(R.drawable.sy_flag);
                break;
            }
            case 225: {
                country.setDecription(context.getString(R.string.Taiwan));
                country.setDrawableID(R.drawable.tw_flag);
                break;
            }
            case 226: {
                country.setDecription(context.getString(R.string.Tajikistan));
                country.setDrawableID(R.drawable.ti_flag);
                break;
            }
            case 227: {
                country.setDecription(context.getString(R.string.Tanzania));
                country.setDrawableID(R.drawable.tz_flag);
                break;
            }
            case 228: {
                country.setDecription(context.getString(R.string.Thailand));
                country.setDrawableID(R.drawable.th_flag);
                break;
            }
            case 229: {
                country.setDecription(context.getString(R.string.Timor_Leste));
                country.setDrawableID(R.drawable.tt_flag);
                break;
            }
            case 230: {
                country.setDecription(context.getString(R.string.Togo));
                country.setDrawableID(R.drawable.to_flag);
                break;
            }
            case 231: {
                country.setDecription(context.getString(R.string.Tokelau));
                country.setDrawableID(R.drawable.tl_flag);
                break;
            }
            case 232: {
                country.setDecription(context.getString(R.string.Tonga));
                country.setDrawableID(R.drawable.tn_flag);
                break;
            }
            case 233: {
                country.setDecription(context.getString(R.string.Trinidad_and_Tobago));
                country.setDrawableID(R.drawable.td_flag);
                break;
            }
            case 234: {
                country.setDecription(context.getString(R.string.Tromelin_Island));
                country.setDrawableID(R.drawable.te_flag);
                break;
            }
            case 235: {
                country.setDecription(context.getString(R.string.Tunisia));
                country.setDrawableID(R.drawable.ts_flag);
                break;
            }
            case 236: {
                country.setDecription(context.getString(R.string.Turkey));
                country.setDrawableID(R.drawable.tu_flag);
                break;
            }
            case 237: {
                country.setDecription(context.getString(R.string.Turkmenistan));
                country.setDrawableID(R.drawable.tx_flag);
                break;
            }
            case 238: {
                country.setDecription(context.getString(R.string.Turks_and_Caicos_Islands));
                country.setDrawableID(R.drawable.tk_flag);
                break;
            }
            case 239: {
                country.setDecription(context.getString(R.string.Tuvalu));
                country.setDrawableID(R.drawable.tv_flag);
                break;
            }
            case 240: {
                country.setDecription(context.getString(R.string.Uganda));
                country.setDrawableID(R.drawable.ug_flag);
                break;
            }
            case 241: {
                country.setDecription(context.getString(R.string.Ukraine));
                country.setDrawableID(R.drawable.up_flag);
                break;
            }
            case 242: {
                country.setDecription(context.getString(R.string.United_Arab_Emirates));
                country.setDrawableID(R.drawable.ae_flag);
                break;
            }
            case 243: {
                country.setDecription(context.getString(R.string.United_Kingdom));
                country.setDrawableID(R.drawable.uk_flag);
                break;
            }
            case 244: {
                country.setDecription(context.getString(R.string.United_States));
                country.setDrawableID(R.drawable.us_flag);
                break;
            }
            case 245: {
                country.setDecription(context.getString(R.string.Uruguay));
                country.setDrawableID(R.drawable.uy_flag);
                break;
            }
            case 246: {
                country.setDecription(context.getString(R.string.Uzbekistan));
                country.setDrawableID(R.drawable.uz_flag);
                break;
            }
            case 247: {
                country.setDecription(context.getString(R.string.Vanuatu));
                country.setDrawableID(R.drawable.nh_flag);
                break;
            }
            case 248: {
                country.setDecription(context.getString(R.string.Venezuela));
                country.setDrawableID(R.drawable.ve_flag);
                break;
            }
            case 249: {
                country.setDecription(context.getString(R.string.Vietnam));
                country.setDrawableID(R.drawable.vm_flag);
                break;
            }
            case 250: {
                country.setDecription(context.getString(R.string.Virgin_Islands));
                country.setDrawableID(R.drawable.vq_flag);
                break;
            }
            case 251: {
                country.setDecription(context.getString(R.string.Wake_Island));
                country.setDrawableID(R.drawable.wq_flag);
                break;
            }
            case 252: {
                country.setDecription(context.getString(R.string.Wallis_and_Futuna));
                country.setDrawableID(R.drawable.wf_flag);
                break;
            }
            case 253: {
                country.setDecription(context.getString(R.string.West_Bank));
                country.setDrawableID(R.drawable.we_flag);
                break;
            }
            case 254: {
                country.setDecription(context.getString(R.string.Western_Sahara));
                country.setDrawableID(R.drawable.wi_flag);
                break;
            }
            case 255: {
                country.setDecription(context.getString(R.string.Yemen));
                country.setDrawableID(R.drawable.ym_flag);
                break;
            }
            case 256: {
                country.setDecription(context.getString(R.string.Zambia));
                country.setDrawableID(R.drawable.za_flag);
                break;
            }
            case 257: {
                country.setDecription(context.getString(R.string.Zimbabwe));
                country.setDrawableID(R.drawable.zi_flag);
                break;
            }
            case 258: {
                country.setDecription(context.getString(R.string.Scotland));
                country.setDrawableID(R.drawable.sk_flag);

                country.setRegion_ID(1);
                TRegion region;

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(1);
                region.setDescription("Highland");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(2);
                region.setDescription("Speyside");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(3);
                region.setDescription("Islands");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(4);
                region.setDescription("Islay");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(5);
                region.setDescription("Lowlands");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(6);
                region.setDescription("Campbeltown");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(7);
                region.setDescription("Other");
                country.regionlist.add(region);

                region = new TRegion();
                region.setCountry_ID(258);
                region.setId(0);
                region.setDescription("All Regions");
                country.regionlist.add(region);

                break;
            }

        }
        return country;
    }

}
