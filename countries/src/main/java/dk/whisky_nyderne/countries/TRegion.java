package dk.whisky_nyderne.countries;

/**
 * Created by hans on 03-10-17.
 */

public class TRegion {
    private int id;
    private String Description;
    private int Country_ID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getCountry_ID() {
        return Country_ID;
    }

    public void setCountry_ID(int country_ID) {
        Country_ID = country_ID;
    }
}
