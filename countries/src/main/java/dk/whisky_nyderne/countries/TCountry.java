package dk.whisky_nyderne.countries;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;

/**
 * Created by hans on 01-10-17.
 */

public class TCountry {
   private  int id;
    private int Region_ID;
    private int DrawableID;
   private String Decription;
   private boolean Active = true;
   private Bitmap flag;
    public ArrayList<TRegion> regionlist;

    public TCountry() {
        regionlist = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDecription() {
        return Decription;
    }

    public void setDecription(String decription) {
        Decription = decription;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public Bitmap getFlag() {
        return flag;
    }

    public void setFlag_ImageByte(byte[] imageByte) {
        //Bitmap bmp;
        if (imageByte != null) {
            this.flag = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);

            //int h = this._image.getHeight() / 4; // height in pixels
            //int w = this._image.getWidth() / 4; // width in pixels
            //Bitmap scaled = Bitmap.createScaledBitmap(this._image, h, w, true);
            this.flag = Bitmap.createScaledBitmap(this.flag, 160, 120, true);


        }

    }

    public int getRegion_ID() {
        return Region_ID;
    }

    public void setRegion_ID(int region_ID) {
        Region_ID = region_ID;
    }

    public int getDrawableID() {
        return DrawableID;
    }

    public void setDrawableID(int drawableID) {
        DrawableID = drawableID;
    }
}
