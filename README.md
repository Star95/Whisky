[![N|Solid](Image/logo.png)](http://whisky-nyderne.dk)

# Information
The app is in early development and not usable.

# About
Whisky app to store personal assessments.
With this app you can rate whiskey you drink and see an average of how you rate your whiskey.

# License
GPL-2.0
