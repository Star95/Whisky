package dk.whisky_nyderne.whisky.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by hans on 19-09-17.
 */


public class DataBase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Whisky16.db";
    private static final int DATABASE_VERSION = 3;

    //** Tabeller **
    //**************
    public static final String BOTTLES_TABLE = "Bottles";
    public static final String RATINGS_TABLE = "Ratings";
    public static final String OWNER_TABLE = "Owner";
    public static final String WISH_LIST_TABLE = "Wish_list";
    public static final String DISTILLERY_TABLE = "Distillery";
    public static final String EVENTS_TABLE = "Events";
    public static final String BARREL_TABLE = "barrel";

    //** Views **
    //***********
    public static final String BOTTLES_LIST_TABLE = "Bottles_list";


    //** A **
    public static final String AGE_OF_WHISKY_COLUMN = "Age_of_whisky";
    public static final String ABV_COLUMN = "ABV";
    public static final String ADDRESS_COLUMN = "Address";


    //** B **
    public static final String BOTTLE_GUID_COLUMN = "Bottle_GUID";

    public static final String BOTTLING_DATE_COLUMN = "Bottling_date";
    public static final String BOTTLE_LABEL_COLUMN = "Bottle_label";
    public static final String BOTTLE_DESCRIPTION_COLUMN = "Bottle_description";
    public static final String BOTTLE_IMAGE_COLUMN = "Bottle_image";
    public static final String BOTTLE_PPM_COLUMN = "Bottle_ppm";
    public static final String BOTTLE_URI_COLUMN = "Bottle_uri";
    public static final String BALANCE_COLUMN = "Balance";
    public static final String BALANCE_DESCRIPTION_COLUMN = "Balance_Description";
    public static final String BOTTLE_SIZE_COLUMN = "Bottle_Size";
    public static final String BARREL_NO_COLUMN = "Barrel_no";


    //** C **
    public static final String COUNTRY_ID_COLUMN = "Country_ID";
    public static final String COUNTRY_COLUMN = "Country";


    //** D **
    public static final String DISTILLERY_COLUMN = "Distillery";
    public static final String DISTILLERY_GUID_COLUMN = "Distillery_GUID";

    public static final String DISTILLATION_DATE_COLUMN = "Distillation_date";
    public static final String DATETIME_COLUMN = "DateTime";
    public static final String DESCRIPTION_COLUMN = "Description";

    //** E **
    public static final String EMPTY_DATETIME_COLUMN = "Empty_datetime";
    //** F **
    public static final String FINISH_COLUMN = "Finish";
    public static final String FINISH_DESCRIPTION_COLUMN = "Finish_Description";
    public static final String FROM_DATETIME_COLUMN = "From_datetime";


    //** G **
    public static final String GUID_COLUMN = "GUID";
    public static final String GPS_N_COLUMN = "GPS_N";
    public static final String GPS_E_COLUMN = "GPS_E";

    //** H **
    public static final String HOMEPAGE_COLUMN = "Homepage";
    //** I **
    public static final String ID_COLUMN = "id";


    //** J **
    //** K **

    //** L **
    public static final String LAST_DATETIME_COLUMN = "Last_DateTime";

    //** M **

    //** N **
    public static final String NOSE_COLUMN = "Nose";
    public static final String NOSE_DESCRIPTION_COLUMN = "Nose_Description";
    public static final String NOTE_COLUMN = "Note";


    //** O **

    public static final String OPEN_DATETIME_COLUMN = "Open_DateTime";
    //** p **
    public static final String PHONE_COLUMN = "Phone";
    public static final String PRICE_COLUMN = "Price";

    //** Q **
    //** R **
    public static final String REGION_ID_COLUMN = "Region_ID";


    //** S **
    public static final String SIZE_COLUMN = "Size";

    //** T **
    public static final String TYPE_COLUMN = "Type";
    public static final String TASTE_COLUMN = "Taste";
    public static final String TASTE_DESCRIPTION_COLUMN = "Taste_Description";
    public static final String TO_DATETIME_COLUMN = "TO_datetime";


    //** U **
    public static final String USER_GUID_COLUMN = "User_GUID";

    //** V **
    //** W **
    public static final String WATER_COLUMN = "Water";
    public static final String WIKIPEDIA_COLUMN = "Wikipedia";

    //** X **
    //** Y **
    //** Z **
    public static final String ZIPCODE_COLUMN = "ZipCode";


    public static final String CREATE_BOTTLES_TABLE =
            "CREATE TABLE IF NOT EXISTS " + BOTTLES_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GUID_COLUMN + " TEXT, "
                    + COUNTRY_ID_COLUMN + " INT, "
                    + REGION_ID_COLUMN + " INT, "
                    + DISTILLERY_GUID_COLUMN + " TEXT, "
                    + TYPE_COLUMN + " INT, "
                    + AGE_OF_WHISKY_COLUMN + " INT, "
//                    + DISTILLATION_DATE_COLUMN + " DATETIME, "
//                    + BOTTLING_DATE_COLUMN + " DATETIME, "
                    + ABV_COLUMN + " DOUBLE, "
                    + BOTTLE_LABEL_COLUMN + " TEXT, "
                    + BOTTLE_DESCRIPTION_COLUMN + " TEXT, "
                    + BOTTLE_IMAGE_COLUMN + " BLOB, "
                    + BOTTLE_PPM_COLUMN + " INT, "
                    + BOTTLE_URI_COLUMN + " TEXT, "
                    + NOSE_DESCRIPTION_COLUMN + " TEXT, "
                    + TASTE_DESCRIPTION_COLUMN + " TEXT, "
                    + FINISH_DESCRIPTION_COLUMN + " TEXT, "
                    + BALANCE_DESCRIPTION_COLUMN + " TEXT, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";


    public static final String CREATE_RATINGS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + RATINGS_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GUID_COLUMN + " TEXT, "
                    + NOSE_COLUMN + " INT, "
                    + TASTE_COLUMN + " INT, "
                    + FINISH_COLUMN + " INT, "
                    + BALANCE_COLUMN + " INT, "
                    + BOTTLE_GUID_COLUMN + " TEXT, "
                    + USER_GUID_COLUMN + " TEXT, "
                    + NOTE_COLUMN + " TEXT, "
                    + WATER_COLUMN + " INT, "
                    + SIZE_COLUMN + " INT, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";


    public static final String CREATE_OWNER_TABLE =
            "CREATE TABLE IF NOT EXISTS " + OWNER_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GUID_COLUMN + " TEXT, "
                    + BOTTLE_GUID_COLUMN + " TEXT, "
                    + USER_GUID_COLUMN + " TEXT, "
                    + BOTTLE_SIZE_COLUMN + " FLOAT, "
                    + OPEN_DATETIME_COLUMN + " DATETIME, "
                    + EMPTY_DATETIME_COLUMN + " DATETIME, "
                    + TYPE_COLUMN + " INT, "
                    + PRICE_COLUMN + " INT, "
                    + NOTE_COLUMN + " INT, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";


    public static final String CREATE_WISH_LIST_TABLE =
            "CREATE TABLE IF NOT EXISTS " + WISH_LIST_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + BOTTLE_GUID_COLUMN + " TEXT, "
                    + USER_GUID_COLUMN + " TEXT, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";


    public static final String CREATE_DISTILLERY_TABLE =
            "CREATE TABLE IF NOT EXISTS " + DISTILLERY_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GUID_COLUMN + " TEXT, "
                    + COUNTRY_ID_COLUMN + " INT, "
                    + REGION_ID_COLUMN + " INT, "
                    + DISTILLERY_GUID_COLUMN + " TEXT, "//TODO : this line is wrong!!
                    + DESCRIPTION_COLUMN + " TEXT, "
                    + ADDRESS_COLUMN + " TEXT, "
                    + ZIPCODE_COLUMN + " TEXT, "
                    + COUNTRY_COLUMN + " TEXT, "
                    + PHONE_COLUMN + " TEXT, "
                    + GPS_N_COLUMN + " TEXT, "
                    + GPS_E_COLUMN + " TEXT, "
                    + HOMEPAGE_COLUMN + " TEXT, "
                    + WIKIPEDIA_COLUMN + " TEXT, "
                    + NOTE_COLUMN + " TEXT, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";

    public static final String CREATE_EVENTS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + EVENTS_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GUID_COLUMN + " TEXT, "
                    + DESCRIPTION_COLUMN + " TEXT, "
                    + FROM_DATETIME_COLUMN + " DATETIME, "
                    + TO_DATETIME_COLUMN + " DATETIME, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";


    public static final String CREATE_BOTTLES_LIST_TABLE =
            "CREATE VIEW IF NOT EXISTS " + BOTTLES_LIST_TABLE + " AS " +
                    " SELECT " +
                    "B." + ID_COLUMN + " AS " + ID_COLUMN + ", " +
                    "B." + GUID_COLUMN + " AS " + GUID_COLUMN + ", " +
                    "B." + DISTILLERY_GUID_COLUMN + " AS " + DISTILLERY_GUID_COLUMN + ", " +
                    "B." + COUNTRY_ID_COLUMN + " AS " + COUNTRY_ID_COLUMN + ", " +
                    "B." + REGION_ID_COLUMN + " AS " + REGION_ID_COLUMN + ", " +
                    "B." + AGE_OF_WHISKY_COLUMN + " AS " + AGE_OF_WHISKY_COLUMN + ", " +
                    "B." + ABV_COLUMN + " AS " + ABV_COLUMN + ", " +
                    "B." + TYPE_COLUMN + " AS " + TYPE_COLUMN + ", " +

                    "B." + BOTTLE_LABEL_COLUMN + " AS " + BOTTLE_LABEL_COLUMN + ", " +
                    "B." + BOTTLE_DESCRIPTION_COLUMN + " AS " + BOTTLE_DESCRIPTION_COLUMN + ", " +
                    "B." + BOTTLE_IMAGE_COLUMN + " AS " + BOTTLE_IMAGE_COLUMN + ", " +
                    "B." + BOTTLE_PPM_COLUMN + " AS " + BOTTLE_PPM_COLUMN + ", " +
//                    "B." + DISTILLATION_DATE_COLUMN + " AS " + DISTILLATION_DATE_COLUMN + ", " +
//                    "B." + BOTTLING_DATE_COLUMN + " AS " + BOTTLING_DATE_COLUMN + ", " +
                    "B." + BOTTLE_URI_COLUMN + " AS " + BOTTLE_URI_COLUMN + ", " +
                    "B." + NOSE_DESCRIPTION_COLUMN + " AS " + NOSE_DESCRIPTION_COLUMN + ", " +
                    "B." + TASTE_DESCRIPTION_COLUMN + " AS " + TASTE_DESCRIPTION_COLUMN + ", " +
                    "B." + FINISH_DESCRIPTION_COLUMN + " AS " + FINISH_DESCRIPTION_COLUMN + ", " +
                    "B." + BALANCE_DESCRIPTION_COLUMN + " AS " + BALANCE_DESCRIPTION_COLUMN + ", " +

                    " ROUND(AVG(R." + NOSE_COLUMN + "),0) AS " + NOSE_COLUMN + ", " +
                    " ROUND(AVG(R." + TASTE_COLUMN + "),0) AS " + TASTE_COLUMN + ", " +
                    " ROUND(AVG(R." + FINISH_COLUMN + "),0) AS " + FINISH_COLUMN + ", " +
                    " ROUND(AVG(R." + BALANCE_COLUMN + "),0) AS " + BALANCE_COLUMN + ", " +
                    " MAX(R." + DATETIME_COLUMN + ") AS " + LAST_DATETIME_COLUMN + "," +
                    " BR." + DESCRIPTION_COLUMN + " AS " + DESCRIPTION_COLUMN +
                    " FROM " +
                    "   " + BOTTLES_TABLE + " AS B " +
                    " LEFT  JOIN " + RATINGS_TABLE + " AS R ON B." + GUID_COLUMN + "=R." + BOTTLE_GUID_COLUMN +
                    " LEFT  JOIN " + DISTILLERY_TABLE + " AS BR ON B." + DISTILLERY_GUID_COLUMN + "=BR." + GUID_COLUMN +
                    " GROUP BY B." + GUID_COLUMN;



    public static final String CREATE_BARREL_TABLE =
            "CREATE TABLE IF NOT EXISTS " + BARREL_TABLE +
                    "("
                    + ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GUID_COLUMN + " TEXT, "
                    + BARREL_NO_COLUMN + " TEXT, "
                    + DISTILLATION_DATE_COLUMN + " TEXT, "
                    + BOTTLING_DATE_COLUMN + " TEXT, "
                    + NOTE_COLUMN + " TEXT, "
                    + DATETIME_COLUMN + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";




    /*
            + _COLUMN + " INT, "
            + _COLUMN + " TEXT, "
            + _COLUMN + " FLOAT, "
            + _COLUMN + " DOUBLE, "
            + _COLUMN + " BLOB, "
            + DATATIME_COLUMN + " DATETIME "
*/


    private static DataBase instance;

    public static synchronized DataBase getHelper(Context context) {
        if (instance == null)
            instance = new DataBase(context);
        return instance;
    }

    private DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        //if (!db.isReadOnly()) {
        // Enable foreign key constraints
        //      db.execSQL("PRAGMA foreign_keys=ON;");
        //}
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_BOTTLES_TABLE);
        db.execSQL(CREATE_RATINGS_TABLE);
        db.execSQL(CREATE_OWNER_TABLE);
        db.execSQL(CREATE_WISH_LIST_TABLE);
        db.execSQL(CREATE_DISTILLERY_TABLE);
        db.execSQL(CREATE_EVENTS_TABLE);
        db.execSQL(CREATE_BOTTLES_LIST_TABLE);
        db.execSQL(CREATE_BARREL_TABLE);


        default_data(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 2:
              //  db.execSQL("ALTER TABLE "+DataBase.OWNER_TABLE+" ADD column "+DataBase.PRICE_COLUMN+" Float Default 0;    ");
//                db.execSQL("ALTER TABLE "+DataBase.OWNER_TABLE+" ADD column "+DataBase.NOTE_COLUMN+" TEXT;                ");
//                db.execSQL(CREATE_BARREL_TABLE);

            case 3:
        }
    }


    private void default_data(SQLiteDatabase db) {

    }
}

