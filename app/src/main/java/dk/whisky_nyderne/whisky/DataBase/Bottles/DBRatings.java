package dk.whisky_nyderne.whisky.DataBase.Bottles;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TBottle_info_Alcohol;
import dk.whisky_nyderne.whisky.Data.THowMuch;
import dk.whisky_nyderne.whisky.Data.TRating;
import dk.whisky_nyderne.whisky.Data.TRating_info;
import dk.whisky_nyderne.whisky.DataBase.DBDAO;
import dk.whisky_nyderne.whisky.DataBase.DataBase;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

/**
 * Created by hans on 01-11-17.
 */

public class DBRatings extends DBDAO {


    private final Context _context;

    public DBRatings(Context context) {
        super(context);
        _context = context;
    }


    public int getNumberOfDays() {
        int number = 0;
        String query = "";
        query += "  Select ";
        query += " " + DataBase.ID_COLUMN;
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  GROUP BY Date(" + DataBase.DATETIME_COLUMN + ")";

        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {
                number += 1;
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getRanking", e.getMessage());
        }

        return number;
    }

    public long addRating(TRating rating) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        ContentValues values = new ContentValues();

        values.put(DataBase.GUID_COLUMN, UUID.randomUUID().toString());

        values.put(DataBase.NOSE_COLUMN, rating.getNOSE());
        values.put(DataBase.TASTE_COLUMN, rating.getTAST());
        values.put(DataBase.FINISH_COLUMN, rating.getFINISH());
        values.put(DataBase.BALANCE_COLUMN, rating.getBALANCE());
        values.put(DataBase.BOTTLE_GUID_COLUMN, rating.getGUID());
        values.put(DataBase.USER_GUID_COLUMN, rating.getUSER_GUID());
        values.put(DataBase.NOTE_COLUMN, rating.getNOTE());
        values.put(DataBase.WATER_COLUMN, rating.getWATER());
        values.put(DataBase.SIZE_COLUMN, rating.getSize());
        try {
            database.insert(DataBase.RATINGS_TABLE, null, values);
        } catch (Exception e) {
            Log.e("Add Rating", e.getMessage());
        }
        return 0;
    }

    private int getRanking(int type, String GUID, int avg) {
        int rank = 0;

        String query = "";
        query += "  Select ";
        query += " ROUND(AVG(" + DataBase.NOSE_COLUMN + ")) as avg1,";
        query += " ROUND(AVG(" + DataBase.TASTE_COLUMN + ")) as avg2,";
        query += " ROUND(AVG(" + DataBase.FINISH_COLUMN + ")) as avg3,";
        query += " ROUND(AVG(" + DataBase.BALANCE_COLUMN + ")) as avg4";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  Where ";
        query += DataBase.BOTTLE_GUID_COLUMN + "<>\"" + GUID + "\"";
        query += "  GROUP BY " + DataBase.GUID_COLUMN;
        query += "  ORDER BY avg" + String.valueOf(type);

        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {

                switch (type) {
                    case 1: {
                        if (c.getInt(0) < avg)
                            rank += 1;
                        break;
                    }
                    case 2: {
                        if (c.getInt(1) < avg)
                            rank += 1;
                        break;
                    }
                    case 3: {
                        if (c.getInt(2) < avg)
                            rank += 1;
                        break;
                    }
                    case 4: {
                        if (c.getInt(3) < avg)
                            rank += 1;
                        break;
                    }
                }

            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getRanking", e.getMessage());
        }

        return rank + 1;
    }


    public TRating_info getRating_Nose_Info(String GUID, int rating) {
        TRating_info rating_info = new TRating_info(rating);

        String query = "";
        query += "  Select ";
        query += " MAX(" + DataBase.NOSE_COLUMN + ") as max,";
        query += " MIN(" + DataBase.NOSE_COLUMN + ") as min,";
        query += " ROUND(AVG(" + DataBase.NOSE_COLUMN + ")) as avg";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  Where ";
        query += DataBase.BOTTLE_GUID_COLUMN + "=\"" + GUID + "\"";

        try {
            Cursor c = database.rawQuery(query, null);
            if (c.moveToNext()) {
                rating_info.setMax(c.getInt(0));
                rating_info.setMin(c.getInt(1));
                rating_info.setAvg(c.getInt(2));
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getRating_Nose_Info", e.getMessage());
        }


        rating_info.setRank(getRanking(1, GUID, (rating_info.getAvg() + rating) / 2));

        return rating_info;
    }

    public TRating_info getRating_Tast_Info(String GUID, int rating) {
        TRating_info rating_info = new TRating_info(rating);

        String query = "";
        query += "  Select ";
        query += " MAX(" + DataBase.TASTE_COLUMN + ") as max,";
        query += " MIN(" + DataBase.TASTE_COLUMN + ") as min,";
        query += " ROUND(AVG(" + DataBase.TASTE_COLUMN + ")) as avg";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  Where ";
        query += DataBase.BOTTLE_GUID_COLUMN + "=\"" + GUID + "\"";

        try {
            Cursor c = database.rawQuery(query, null);
            if (c.moveToNext()) {
                rating_info.setMax(c.getInt(0));
                rating_info.setMin(c.getInt(1));
                rating_info.setAvg(c.getInt(2));
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getRating_Nose_Info", e.getMessage());
        }


        rating_info.setRank(getRanking(2, GUID, (rating_info.getAvg() + rating) / 2));

        return rating_info;
    }

    public TRating_info getRating_Finish_Info(String GUID, int rating) {
        TRating_info rating_info = new TRating_info(rating);

        String query = "";
        query += "  Select ";
        query += " MAX(" + DataBase.FINISH_COLUMN + ") as max,";
        query += " MIN(" + DataBase.FINISH_COLUMN + ") as min,";
        query += " ROUND(AVG(" + DataBase.FINISH_COLUMN + ")) as avg";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  Where ";
        query += DataBase.BOTTLE_GUID_COLUMN + "=\"" + GUID + "\"";

        try {
            Cursor c = database.rawQuery(query, null);
            if (c.moveToNext()) {
                rating_info.setMax(c.getInt(0));
                rating_info.setMin(c.getInt(1));
                rating_info.setAvg(c.getInt(2));
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getRating_Nose_Info", e.getMessage());
        }


        rating_info.setRank(getRanking(3, GUID, (rating_info.getAvg() + rating) / 2));

        return rating_info;
    }

    public TRating_info getRating_Balance_Info(String GUID, int rating) {
        TRating_info rating_info = new TRating_info(rating);

        String query = "";
        query += "  Select ";
        query += " MAX(" + DataBase.BALANCE_COLUMN + ") as max,";
        query += " MIN(" + DataBase.BALANCE_COLUMN + ") as min,";
        query += " ROUND(AVG(" + DataBase.BALANCE_COLUMN + ")) as avg";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  Where ";
        query += DataBase.BOTTLE_GUID_COLUMN + "=\"" + GUID + "\"";

        try {
            Cursor c = database.rawQuery(query, null);
            if (c.moveToNext()) {
                rating_info.setMax(c.getInt(0));
                rating_info.setMin(c.getInt(1));
                rating_info.setAvg(c.getInt(2));
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getRating_Nose_Info", e.getMessage());
        }


        rating_info.setRank(getRanking(4, GUID, (rating_info.getAvg() + rating) / 2));

        return rating_info;
    }

    /**
     * @param GUID
     * @return
     */
    public List<TRating> getRatings(String GUID, Boolean DESC) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TRating> data = new ArrayList<TRating>();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.NOSE_COLUMN;
        query += "," + DataBase.TASTE_COLUMN;
        query += "," + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BALANCE_COLUMN;
        query += "," + DataBase.BOTTLE_GUID_COLUMN;
        query += "," + DataBase.USER_GUID_COLUMN;
        query += "," + DataBase.NOTE_COLUMN;
        query += "," + DataBase.WATER_COLUMN;
        query += "," + DataBase.SIZE_COLUMN;
        query += ", datetime(" + DataBase.DATETIME_COLUMN + ", 'localtime') as " + DataBase.DATETIME_COLUMN;

        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "   WHERE  " + DataBase.BOTTLE_GUID_COLUMN + " = \"" + GUID + "\"";
        query += "  ORDER BY " + DataBase.DATETIME_COLUMN;
        if (DESC)
            query += " DESC";
        else
            query += " ASC";


        Cursor c = database.rawQuery(query, null);

        while (c.moveToNext()) {
            TRating rating = new TRating();
            rating.setGUID(c.getString(0));
            rating.setNOSE(c.getInt(1));
            rating.setTAST(c.getInt(2));
            rating.setFINISH(c.getInt(3));
            rating.setBALANCE(c.getInt(4));
            rating.setBOTTLE_GUID(c.getString(5));
            rating.setUSER_GUID(c.getString(6));
            rating.setNOTE(c.getString(7));
            rating.setWATER(c.getInt(8));
            rating.setSize(c.getInt(9));
            rating.setDATETIME(dateTimeFormat.StringToDate(c.getString(10)));
            data.add(rating);
        }
        c.close();
        return data;
    }

    /**
     * Get Rank number of a bottle.
     *
     * @param GUID
     * @return int rank no.
     */
    public int getRankNo(String GUID) {
        int no = 0;
        boolean ok = false;
        String query = "";
        query += "  Select ";
        query += " " + DataBase.GUID_COLUMN;
        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE;
        query += "  ORDER BY (" + DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + ") DESC";
        Cursor c = database.rawQuery(query, null);
        while (c.moveToNext()) {
            if (!ok)
                no += 1;
            if (c.getString(0).equals(GUID))
                ok = true;
        }
        c.close();

        if (!ok)
            no = 0;
        return no;
    }


    public List<TBottle_info_Alcohol> getLast24Hours() {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TBottle_info_Alcohol> data = new ArrayList<TBottle_info_Alcohol>();
        String query = "";
        query += "  Select ";
        query += " " + DataBase.BOTTLE_GUID_COLUMN + ",";
        query += " " + DataBase.NOSE_COLUMN + ",";
        query += " " + DataBase.TASTE_COLUMN + ",";
        query += " " + DataBase.FINISH_COLUMN + ",";
        query += " " + DataBase.BALANCE_COLUMN + ",";
        query += " " + DataBase.SIZE_COLUMN + ",";
        query += " datetime(" + DataBase.DATETIME_COLUMN + ", 'localtime') as " + DataBase.DATETIME_COLUMN;
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "  Where ";
        query += DataBase.DATETIME_COLUMN + ">=datetime('now','-1 day')";
        query += "  ORDER BY " + DataBase.DATETIME_COLUMN + " ASC";

        DBBottles dbBottles = new DBBottles(_context);
        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {
                if (c.getString(0) != null && !c.getString(0).isEmpty()) {
                    TBottle_info_Alcohol bottle_info_alcohol = new TBottle_info_Alcohol();

                    TBottle_info bottle_info = new TBottle_info();
                    bottle_info = dbBottles.getBottles_Info(c.getString(0));
                    bottle_info_alcohol.setBottle(bottle_info.getBottle());
                    bottle_info_alcohol.setDistillery(bottle_info.getDistillery());

                    bottle_info_alcohol.getRating().setNOSE(c.getInt(1));
                    bottle_info_alcohol.getRating().setTAST(c.getInt(2));
                    bottle_info_alcohol.getRating().setFINISH(c.getInt(3));
                    bottle_info_alcohol.getRating().setBALANCE(c.getInt(4));
                    bottle_info_alcohol.getRating().setSize(c.getInt(5));
                    bottle_info_alcohol.getRating().setDATETIME(dateTimeFormat.StringToDate(c.getString(6)));
                    data.add(bottle_info_alcohol);
                }
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("", e.getMessage());
        }

        return data;

    }


    private int getNumber_Of_Types(String Year) {
        int number = 0;
        String query = "";
        query += "  Select ";
        query += " COUNT(" + DataBase.ID_COLUMN + ") ";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "    WHERE ";
        query += "     strftime('%Y'," + DataBase.DATETIME_COLUMN + ")=\"" + Year + "\"";

        query += "  GROUP BY " + DataBase.BOTTLE_GUID_COLUMN;
        Cursor c = database.rawQuery(query, null);
        while (c.moveToNext()) {
            number += 1;
        }
        c.close();

        return number;
    }

    public THowMuch getHowMuchInfo(String Year) {
        THowMuch howMuch = new THowMuch();

        String query = "";
        query += "  Select ";
        query += " SUM(" + DataBase.SIZE_COLUMN + "), ";
        query += " COUNT(" + DataBase.ID_COLUMN + ") ";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        query += "    WHERE ";
        query += "     strftime('%Y'," + DataBase.DATETIME_COLUMN + ")=\"" + Year + "\"";
        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        if (Objects.requireNonNull(c).moveToNext()) {
            howMuch.setVolume(c.getInt(0));
            howMuch.setNo_of_Drinks(c.getInt(1));
            howMuch.setNo_of_Types(getNumber_Of_Types(Year));
        }
        c.close();


        return howMuch;
    }


    public int getFirstYear(int DefaultFirstYear) {

        String query = "";
        query += "  Select ";
        query += " strftime('%Y', MIN(" + DataBase.DATETIME_COLUMN + ")) ";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        int result = 0;
        if (Objects.requireNonNull(c).moveToNext()) {
            result = c.getInt(0);
        }
        c.close();
        if (result==0)
            result=DefaultFirstYear;

        return result;
    }
    public int getNumber_Of_Ratings() {
        int number = 0;
        String query = "";
        query += "  Select ";
        query += " COUNT(" + DataBase.ID_COLUMN + ") ";
        query += "  FROM " + DataBase.RATINGS_TABLE;
        Cursor c = database.rawQuery(query, null);
        if (c.moveToNext()) {
            number =c.getInt(0);
        }
        c.close();

        return number;
    }


}
