package dk.whisky_nyderne.whisky.DataBase.Bottles;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TOwner;
import dk.whisky_nyderne.whisky.DataBase.DBDAO;
import dk.whisky_nyderne.whisky.DataBase.DataBase;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

/**
 * Created by hans on 20-02-18.
 */

public class DBOwnBottles extends DBDAO {
    private final Context _context;

    public DBOwnBottles(Context context) {
        super(context);
        _context = context;
    }

    public long addOwneBottle(TOwner owner) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        ContentValues values = new ContentValues();

        values.put(DataBase.GUID_COLUMN, UUID.randomUUID().toString());
        values.put(DataBase.BOTTLE_GUID_COLUMN, owner.getBOTTLE_GUID());
        values.put(DataBase.USER_GUID_COLUMN, owner.getUSER_GUID());
        values.put(DataBase.BOTTLE_SIZE_COLUMN, owner.getSize());
        values.put(DataBase.OPEN_DATETIME_COLUMN, dateTimeFormat.DateTimeToString(owner.getOpen_DateTime()));
        values.put(DataBase.EMPTY_DATETIME_COLUMN, dateTimeFormat.DateTimeToString(owner.getEmpty_Datetime()));
        values.put(DataBase.PRICE_COLUMN, owner.getPrice());
        values.put(DataBase.NOTE_COLUMN, owner.getNote());


        try {
            database.insert(DataBase.OWNER_TABLE, null, values);
        } catch (Exception e) {
            Log.e("Add Own bottle", e.getMessage());
        }
        return 0;
    }

    public List<TOwner> getOwnBottles(String Bottle_GUID) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        List<TOwner> data = new ArrayList<TOwner>();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.BOTTLE_GUID_COLUMN;
        query += "," + DataBase.USER_GUID_COLUMN;
        query += "," + DataBase.BOTTLE_SIZE_COLUMN;
        query += ", datetime(" + DataBase.OPEN_DATETIME_COLUMN + ", 'localtime') as " + DataBase.OPEN_DATETIME_COLUMN;
        query += ", datetime(" + DataBase.EMPTY_DATETIME_COLUMN + ", 'localtime') as " + DataBase.EMPTY_DATETIME_COLUMN;
        query += "," + DataBase.TYPE_COLUMN;
        query += ", datetime(" + DataBase.DATETIME_COLUMN + ", 'localtime') as " + DataBase.DATETIME_COLUMN;
        query += "," + DataBase.PRICE_COLUMN;
        query += "," + DataBase.NOTE_COLUMN;

        query += "  FROM " + DataBase.OWNER_TABLE;
        query += "   WHERE  " + DataBase.BOTTLE_GUID_COLUMN + " = \"" + Bottle_GUID + "\"";
        query += "  ORDER BY " + DataBase.DATETIME_COLUMN;
        query += "  DESC ";
        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        while (Objects.requireNonNull(c).moveToNext()) {

            TOwner rating = new TOwner();
            rating.setGUID(c.getString(0));
            rating.setBOTTLE_GUID(c.getString(1));
            rating.setUSER_GUID(c.getString(2));
            rating.setSize(c.getDouble(3));
            rating.setOpen_DateTime(dateTimeFormat.StringToDate(c.getString(4)));
            rating.setEmpty_Datetime(dateTimeFormat.StringToDate(c.getString(5)));
            rating.setType(c.getInt(6));
            rating.setDATETIME(dateTimeFormat.StringToDate(c.getString(7)));
            rating.setPrice(c.getDouble(8));
            rating.setNote(c.getString(9));
            data.add(rating);
        }
        c.close();
        return data;
    }


    public void Delete_Own_Bottle(String GUID) {
        String query = "DELETE FROM " + DataBase.OWNER_TABLE + " WHERE " + DataBase.GUID_COLUMN + "= \"" + GUID + "\"";
        try {
            database.execSQL(query);
        } catch (Exception e) {
            Log.e("Delete Own bottle", e.getMessage());

        }

    }

    public void setEmptyOwnBottle(String GUID, Date date) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        ContentValues values = new ContentValues();
        values.put(DataBase.EMPTY_DATETIME_COLUMN, dateTimeFormat.DateTimeToString(date));
        values.put(DataBase.TYPE_COLUMN, 2);
        try {
            database.update(DataBase.OWNER_TABLE, values, DataBase.GUID_COLUMN + "=?", new String[]{String.valueOf(GUID)});
        } catch (Exception e) {
            Log.e("Update Own bottle", e.getMessage());
        }

    }

    public void setOpenNewBottle(String GUID, Date date) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        ContentValues values = new ContentValues();
        values.put(DataBase.OPEN_DATETIME_COLUMN, dateTimeFormat.DateTimeToString(date));
        values.put(DataBase.TYPE_COLUMN, 1);
        try {
            database.update(DataBase.OWNER_TABLE, values, DataBase.GUID_COLUMN + "=?", new String[]{String.valueOf(GUID)});
        } catch (Exception e) {
            Log.e("Update Own bottle", e.getMessage());
        }

    }


    public List<TBottle_info> getOwnBottlesList(int Limit, boolean OrderRank, int ShowType) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TBottle_info> data = new ArrayList<TBottle_info>();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.GUID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.ABV_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.TYPE_COLUMN;

        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_URI_COLUMN;

        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.NOSE_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.TASTE_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BALANCE_COLUMN;
        query += ", datetime(" + DataBase.LAST_DATETIME_COLUMN + ", 'localtime') as " + DataBase.LAST_DATETIME_COLUMN;

        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.NOSE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.TASTE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.FINISH_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BALANCE_DESCRIPTION_COLUMN;

        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE;
        query += "   JOIN " + DataBase.OWNER_TABLE;
        query += "   ON " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.GUID_COLUMN + "=" + DataBase.OWNER_TABLE + "." + DataBase.BOTTLE_GUID_COLUMN;

        if (ShowType == 3)
            query += "  WHERE " + DataBase.OWNER_TABLE + "." + DataBase.TYPE_COLUMN + "<" + String.valueOf(ShowType);
        else if (ShowType == 0)
            query += "  WHERE " + DataBase.OWNER_TABLE + "." + DataBase.TYPE_COLUMN + " is NULL";
        else
            query += "  WHERE " + DataBase.OWNER_TABLE + "." + DataBase.TYPE_COLUMN + "=" + String.valueOf(ShowType);

        query += "   GROUP BY " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.GUID_COLUMN;

        if (OrderRank) {
            query += "  ORDER BY " + DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + " DESC";
            query += "  , " + DataBase.LAST_DATETIME_COLUMN + " ASC";
        } else {
            query += "  ORDER BY " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_LABEL_COLUMN + " DESC";
        }

        if (Limit != 0)
            query += "  LIMIT " + String.valueOf(Limit);
        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        while (Objects.requireNonNull(c).moveToNext()) {

            TBottle_info bottle = new TBottle_info();
            bottle.getBottle().setGUID(c.getString(0));
            bottle.getBottle().setDistillery_GUID(c.getString(1));
            bottle.getBottle().setCOUNTRY_ID(c.getInt(2));
            bottle.getBottle().setREGION_ID(c.getInt(3));
            bottle.getBottle().setAGE_OF_WHISKY(c.getInt(4));
            bottle.getBottle().setABV(c.getDouble(5));
            bottle.getBottle().setTYPE(c.getInt(6));
            bottle.getBottle().setBOTTLE_LABEL(c.getString(7));
            bottle.getBottle().setBOTTLE_DESCRIPTION(c.getString(8));
            try {
                bottle.getBottle().setBOTTLE_ImageByte(c.getBlob(9));
            } catch (Exception e) {
                bottle.getBottle().setBOTTLE_ImageByte(null);
                Log.e("", e.getMessage());
            }
            bottle.getBottle().setBOTTLE_PPM(c.getInt(10));

            bottle.getBottle().setBOTTLE_URL(c.getString(11));
            bottle.getRating().setNOSE(c.getInt(12));
            bottle.getRating().setTAST(c.getInt(13));
            bottle.getRating().setFINISH(c.getInt(14));
            bottle.getRating().setBALANCE(c.getInt(15));
//            bottle.setLast_DATETIME(c.getString(18));
            bottle.getDistillery().setDescription(c.getString(17));

            bottle.getBottle().setNose_Description(c.getString(18));
            bottle.getBottle().setTaste_Description(c.getString(19));
            bottle.getBottle().setFinish_Description(c.getString(20));
            bottle.getBottle().setBalance_Description(c.getString(21));

            data.add(bottle);
        }
        c.close();
        return data;
    }


}
