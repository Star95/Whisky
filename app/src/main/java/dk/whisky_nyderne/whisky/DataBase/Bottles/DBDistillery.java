package dk.whisky_nyderne.whisky.DataBase.Bottles;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import dk.whisky_nyderne.whisky.Data.TDistillery;
import dk.whisky_nyderne.whisky.Data.TDistilleryTop;
import dk.whisky_nyderne.whisky.DataBase.DBDAO;
import dk.whisky_nyderne.whisky.DataBase.DataBase;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

/**
 * Created by hans on 19-09-17.
 */

public class DBDistillery extends DBDAO {

    private final Context _context;

    public DBDistillery(Context context) {
        super(context);
        _context = context;
    }

    private boolean isDistillery(String GUID) {
        String query = "Select * FROM " + DataBase.DISTILLERY_TABLE + " WHERE " + DataBase.GUID_COLUMN + " = \"" + GUID + "\"";
        Cursor cursor = database.rawQuery(query, null);
        Boolean Res;
        Res = cursor.moveToFirst();
        cursor.close();

        return Res;
    }

    public long addDistillery(TDistillery distillery) {
        Boolean Edit = isDistillery(distillery.getGUID());

        ContentValues values = new ContentValues();
        if (!Edit)
            values.put(DataBase.GUID_COLUMN, UUID.randomUUID().toString());
        values.put(DataBase.COUNTRY_ID_COLUMN, distillery.getCOUNTRY_ID());
        values.put(DataBase.REGION_ID_COLUMN, distillery.getREGION_ID());
        values.put(DataBase.DISTILLERY_GUID_COLUMN, distillery.getDISTILLERY_GUID());
        values.put(DataBase.DESCRIPTION_COLUMN, distillery.getDescription());
        values.put(DataBase.ADDRESS_COLUMN, distillery.getAddress());
        values.put(DataBase.ZIPCODE_COLUMN, distillery.getZipCode());
        values.put(DataBase.COUNTRY_COLUMN, distillery.getCountry());
        values.put(DataBase.PHONE_COLUMN, distillery.getPhone());
        values.put(DataBase.GPS_N_COLUMN, distillery.getGPS_N());
        values.put(DataBase.GPS_E_COLUMN, distillery.getGPS_E());
        values.put(DataBase.NOTE_COLUMN, distillery.getNote());
        values.put(DataBase.HOMEPAGE_COLUMN, distillery.getHomePage());
        values.put(DataBase.WIKIPEDIA_COLUMN, distillery.getWikipedia());

        long r;
        if (Edit)
            r = database.update(DataBase.DISTILLERY_TABLE, values, DataBase.GUID_COLUMN + "=?", new String[]{distillery.getGUID()});
        else
            r = database.insert(DataBase.DISTILLERY_TABLE, null, values);

        return r;
    }


    public void Delete_Distillery(String GUID) {
        String query = "DELETE FROM " + DataBase.DISTILLERY_TABLE + " WHERE " + DataBase.GUID_COLUMN + "= \"" + GUID + "\"";
        try {
            database.execSQL(query);
        } catch (Exception e) {
            Log.e("Delete Distillery", e.getMessage());

        }

    }


    public List<TDistillery> getDistillery(int CountryID, int RegionID, String Filter) {
        List<TDistillery> List_Distillery = new ArrayList<TDistillery>();


        String query = "";
        query += "  Select ";
        query += DataBase.ID_COLUMN + ",";
        query += DataBase.GUID_COLUMN + ",";
        query += DataBase.COUNTRY_ID_COLUMN + ",";
        query += DataBase.REGION_ID_COLUMN + ",";
        query += DataBase.DISTILLERY_GUID_COLUMN + ",";
        query += DataBase.DESCRIPTION_COLUMN + ",";
        query += DataBase.ADDRESS_COLUMN + ",";
        query += DataBase.ZIPCODE_COLUMN + ",";
        query += DataBase.COUNTRY_COLUMN + ",";
        query += DataBase.PHONE_COLUMN + ",";
        query += DataBase.GPS_N_COLUMN + ",";
        query += DataBase.GPS_E_COLUMN + ",";
        query += DataBase.NOTE_COLUMN + ",";
        query += DataBase.HOMEPAGE_COLUMN + ",";
        query += DataBase.WIKIPEDIA_COLUMN + ",";

        query += DataBase.DATETIME_COLUMN;
        query += "  FROM " + DataBase.DISTILLERY_TABLE;
        query += "  Where ";
        query += DataBase.COUNTRY_ID_COLUMN + "=" + String.valueOf(CountryID);
        if (RegionID != 0)
            query += "  and   " + DataBase.REGION_ID_COLUMN + "=" + String.valueOf(RegionID);
        query += "  and   " + DataBase.DESCRIPTION_COLUMN + " like \"%" + Filter + "%\"";
        query += " order by " + DataBase.DESCRIPTION_COLUMN;

        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {
                TDistillery distillery = new TDistillery();
                distillery.setGUID(c.getString(1));
                distillery.setCOUNTRY_ID(c.getInt(2));
                distillery.setREGION_ID(c.getInt(3));
                distillery.setDISTILLERY_GUID(c.getString(4));
                distillery.setDescription(c.getString(5));
                distillery.setAddress(c.getString(6));
                distillery.setZipCode(c.getString(7));
                distillery.setCountry(c.getString(8));
                distillery.setPhone(c.getString(9));
                distillery.setGPS_N(c.getString(10));
                distillery.setGPS_E(c.getString(11));
                distillery.setNote(c.getString(12));
                distillery.setHomePage(c.getString(13));
                distillery.setWikipedia(c.getString(14));
                //brand.setDATETIME(c.getString(6));
                List_Distillery.add(distillery);
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getDistillery", e.getMessage());
        }

        return List_Distillery;
    }

    public List<TDistillery> getDistillery_Search(String Filter) {
        List<TDistillery> List_Distillery = new ArrayList<TDistillery>();


        String query = "";
        query += "  Select ";
        query += DataBase.ID_COLUMN + ",";
        query += DataBase.GUID_COLUMN + ",";
        query += DataBase.COUNTRY_ID_COLUMN + ",";
        query += DataBase.REGION_ID_COLUMN + ",";
        query += DataBase.DISTILLERY_GUID_COLUMN + ",";
        query += DataBase.DESCRIPTION_COLUMN + ",";
        query += DataBase.ADDRESS_COLUMN + ",";
        query += DataBase.ZIPCODE_COLUMN + ",";
        query += DataBase.COUNTRY_COLUMN + ",";
        query += DataBase.PHONE_COLUMN + ",";
        query += DataBase.GPS_N_COLUMN + ",";
        query += DataBase.GPS_E_COLUMN + ",";
        query += DataBase.NOTE_COLUMN + ",";
        query += DataBase.HOMEPAGE_COLUMN + ",";
        query += DataBase.WIKIPEDIA_COLUMN + ",";

        query += DataBase.DATETIME_COLUMN;
        query += "  FROM " + DataBase.DISTILLERY_TABLE;
        query += "  Where ";
        query += "  " + DataBase.DESCRIPTION_COLUMN + " like \"%" + Filter + "%\"";
        query += " order by " + DataBase.DESCRIPTION_COLUMN;

        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {
                TDistillery distillery = new TDistillery();
                distillery.setGUID(c.getString(1));
                distillery.setCOUNTRY_ID(c.getInt(2));
                distillery.setREGION_ID(c.getInt(3));
                distillery.setDISTILLERY_GUID(c.getString(4));
                distillery.setDescription(c.getString(5));
                distillery.setAddress(c.getString(6));
                distillery.setZipCode(c.getString(7));
                distillery.setCountry(c.getString(8));
                distillery.setPhone(c.getString(9));
                distillery.setGPS_N(c.getString(10));
                distillery.setGPS_E(c.getString(11));
                distillery.setNote(c.getString(12));
                distillery.setHomePage(c.getString(13));
                distillery.setWikipedia(c.getString(14));
                //brand.setDATETIME(c.getString(6));
                List_Distillery.add(distillery);
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getDistillery", e.getMessage());
        }

        return List_Distillery;
    }


    public TDistillery getDistillery(String GUID) {

        String query = "";
        query += "  Select ";
        query += DataBase.ID_COLUMN + ",";
        query += DataBase.GUID_COLUMN + ",";
        query += DataBase.COUNTRY_ID_COLUMN + ",";
        query += DataBase.REGION_ID_COLUMN + ",";
        query += DataBase.DISTILLERY_GUID_COLUMN + ",";
        query += DataBase.DESCRIPTION_COLUMN + ",";
        query += DataBase.ADDRESS_COLUMN + ",";
        query += DataBase.ZIPCODE_COLUMN + ",";
        query += DataBase.COUNTRY_COLUMN + ",";
        query += DataBase.PHONE_COLUMN + ",";
        query += DataBase.GPS_N_COLUMN + ",";
        query += DataBase.GPS_E_COLUMN + ",";
        query += DataBase.NOTE_COLUMN + ",";
        query += DataBase.HOMEPAGE_COLUMN + ",";
        query += DataBase.WIKIPEDIA_COLUMN + ",";

        query += DataBase.DATETIME_COLUMN;
        query += "  FROM " + DataBase.DISTILLERY_TABLE;
        query += "  Where ";
        query += DataBase.GUID_COLUMN + "=\"" + GUID + "\"";
        TDistillery distillery = null;
        try {
            Cursor c = database.rawQuery(query, null);
            if (c.moveToNext()) {
                distillery = new TDistillery();
                distillery.setGUID(c.getString(1));
                distillery.setCOUNTRY_ID(c.getInt(2));
                distillery.setREGION_ID(c.getInt(3));
                distillery.setDISTILLERY_GUID(c.getString(4));
                distillery.setDescription(c.getString(5));
                distillery.setAddress(c.getString(6));
                distillery.setZipCode(c.getString(7));
                distillery.setCountry(c.getString(8));
                distillery.setPhone(c.getString(9));
                distillery.setGPS_N(c.getString(10));
                distillery.setGPS_E(c.getString(11));
                distillery.setNote(c.getString(12));
                distillery.setHomePage(c.getString(13));
                distillery.setWikipedia(c.getString(14));

                //brand.setDATETIME(c.getString(6));
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getDistillery", e.getMessage());
        }

        return distillery;


    }


    /*
    To be on the top list, at least two different kinds of whiskey must be tasted from a distillery
     */
    public List<TDistilleryTop> getTopDistillery(int Limit, String Filter) {
        List<TDistilleryTop> List_Distillery = new ArrayList<TDistilleryTop>();
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        String query = "";
        query += "  Select ";
        query += " ROUND(AVG(" + DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + ")) as Result,";
        query += DataBase.DISTILLERY_GUID_COLUMN + ",";
        query += DataBase.DESCRIPTION_COLUMN + ",";
        query += DataBase.COUNTRY_ID_COLUMN + ",";
        query += "MAX( datetime(" + DataBase.LAST_DATETIME_COLUMN + ", 'localtime')) as " + DataBase.LAST_DATETIME_COLUMN;

        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE;
        query += "  Where ";
        query += DataBase.NOSE_COLUMN + ">0";
        query += "  and   " + DataBase.TASTE_COLUMN + ">0";
        query += "  and   " + DataBase.FINISH_COLUMN + ">0";
        query += "  and   " + DataBase.BALANCE_COLUMN + ">0";
        if (!Filter.isEmpty())
            query += "  and   " + DataBase.DESCRIPTION_COLUMN + " LIKE \"%" + Filter + "\"";

        query += " GROUP BY " + DataBase.DISTILLERY_GUID_COLUMN;
        query += " HAVING count(*) > 1 ";
        query += " order by Result DESC";
        if (Limit > 0)
            query += " LIMIT " + String.valueOf(Limit);



        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {
                    TDistilleryTop distillery = new TDistilleryTop();
                    distillery.setResult(c.getInt(0));
                    distillery.setGUID(c.getString(1));
                    distillery.setDescription(c.getString(2));
                    distillery.setCountryID(c.getInt(3));
                    distillery.setDateTime(dateTimeFormat.StringToDate(c.getString(4)));
                    List_Distillery.add(distillery);

            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getDistillery", e.getMessage());
        }

        return List_Distillery;
    }

}

