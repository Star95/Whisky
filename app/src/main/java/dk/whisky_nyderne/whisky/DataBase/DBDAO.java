package dk.whisky_nyderne.whisky.DataBase;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by hans on 19-09-17.
 *
 */

public class DBDAO {

    protected SQLiteDatabase database;
    private DataBase dbHelper;
    private final Context mContext;

    public DBDAO(Context context) {
        this.mContext = context;
        dbHelper = DataBase.getHelper(mContext);
        open();

    }

    public void open() throws SQLException {
        if(dbHelper == null)
            dbHelper = DataBase.getHelper(mContext);
        database = dbHelper.getWritableDatabase();
    }

    /*public void close() {
        dbHelper.close();
        database = null;
    }*/
}
