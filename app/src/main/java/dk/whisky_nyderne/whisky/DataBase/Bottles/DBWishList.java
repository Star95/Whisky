package dk.whisky_nyderne.whisky.DataBase.Bottles;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TWish_List;
import dk.whisky_nyderne.whisky.DataBase.DBDAO;
import dk.whisky_nyderne.whisky.DataBase.DataBase;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

public class DBWishList extends DBDAO {


    private final Context _context;

    public DBWishList(Context context) {
        super(context);
        _context = context;
    }


    public boolean isOnWishlist(String GUID) {
        String query = "Select * FROM " + DataBase.WISH_LIST_TABLE + " WHERE " + DataBase.BOTTLE_GUID_COLUMN + " = \"" + GUID + "\"";
        Cursor cursor = database.rawQuery(query, null);
        Boolean Res;
        Res = cursor.moveToFirst();
        cursor.close();

        return Res;
    }

    public long addWish(TWish_List wish_list) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        ContentValues values = new ContentValues();

//        values.put(DataBase.GUID_COLUMN, UUID.randomUUID().toString());
        values.put(DataBase.BOTTLE_GUID_COLUMN, wish_list.getBOTTLE_GUID());
        values.put(DataBase.USER_GUID_COLUMN, wish_list.getUSER_GUID());
        values.put(DataBase.DATETIME_COLUMN, dateTimeFormat.DateTimeToString(wish_list.getDATETIME()));
        try {
            database.insert(DataBase.WISH_LIST_TABLE, null, values);
        } catch (Exception e) {
            Log.e("Add Wish", e.getMessage());
        }
        return 0;
    }

    public void deleteWish(String GUID) {
        String query = "DELETE FROM " + DataBase.WISH_LIST_TABLE;
        query += " WHERE " + DataBase.BOTTLE_GUID_COLUMN + "=\"" + GUID + "\"";
        try {
            database.execSQL(query);
        } catch (Exception e) {
            Log.e("Delete Own bottle", e.getMessage());

        }

    }

    public List<TBottle_info> getWishList() {
        List<TBottle_info> data = new ArrayList<TBottle_info>();

        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.ABV_COLUMN;
        query += "," + DataBase.TYPE_COLUMN;

        query += "," + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLE_URI_COLUMN;

        query += "," + DataBase.NOSE_COLUMN;
        query += "," + DataBase.TASTE_COLUMN;
        query += "," + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BALANCE_COLUMN;
        query += ", datetime(" + DataBase.LAST_DATETIME_COLUMN + ", 'localtime') as " + DataBase.LAST_DATETIME_COLUMN;

        query += "," + DataBase.DESCRIPTION_COLUMN;
        query += "," + DataBase.NOSE_DESCRIPTION_COLUMN;
        query += "," + DataBase.TASTE_DESCRIPTION_COLUMN;
        query += "," + DataBase.FINISH_DESCRIPTION_COLUMN;
        query += "," + DataBase.BALANCE_DESCRIPTION_COLUMN;

        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE + "," + DataBase.WISH_LIST_TABLE;
        query += "  WHERE " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.GUID_COLUMN + "=" + DataBase.WISH_LIST_TABLE + "." + DataBase.BOTTLE_GUID_COLUMN;

        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        TBottle_info bottle = null;
        while (Objects.requireNonNull(c).moveToNext()) {

            bottle = new TBottle_info();
            bottle.getBottle().setGUID(c.getString(0));
            bottle.getBottle().setDistillery_GUID(c.getString(1));
            bottle.getBottle().setCOUNTRY_ID(c.getInt(2));
            bottle.getBottle().setREGION_ID(c.getInt(3));
            bottle.getBottle().setAGE_OF_WHISKY(c.getInt(4));
            bottle.getBottle().setABV(c.getDouble(5));
            bottle.getBottle().setTYPE(c.getInt(6));
            bottle.getBottle().setBOTTLE_LABEL(c.getString(7));
            bottle.getBottle().setBOTTLE_DESCRIPTION(c.getString(8));
            bottle.getBottle().setBOTTLE_ImageByte(c.getBlob(9));
            bottle.getBottle().setBOTTLE_PPM(c.getInt(10));
            bottle.getBottle().setBOTTLE_URL(c.getString(11));
            bottle.getRating().setNOSE(c.getInt(12));
            bottle.getRating().setTAST(c.getInt(13));
            bottle.getRating().setFINISH(c.getInt(14));
            bottle.getRating().setBALANCE(c.getInt(15));
//            bottle.setLast_DATETIME(c.getString(18));
            bottle.getDistillery().setDescription(c.getString(17));

            bottle.getBottle().setNose_Description(c.getString(18));
            bottle.getBottle().setTaste_Description(c.getString(19));
            bottle.getBottle().setFinish_Description(c.getString(20));
            bottle.getBottle().setBalance_Description(c.getString(21));
            data.add(bottle);
        }
        c.close();
        return data;
    }


}
