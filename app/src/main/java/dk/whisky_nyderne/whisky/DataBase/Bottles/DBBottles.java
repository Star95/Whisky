package dk.whisky_nyderne.whisky.DataBase.Bottles;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import dk.whisky_nyderne.whisky.Data.TBottle;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.DataBase.DBDAO;
import dk.whisky_nyderne.whisky.DataBase.DataBase;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

/**
 * Created by hans on 14-10-17.
 */

public class DBBottles extends DBDAO {


    private final Context _context;

    public DBBottles(Context context) {
        super(context);
        _context = context;
    }

    private boolean isBottle(String GUID) {
        String query = "Select * FROM " + DataBase.BOTTLES_TABLE + " WHERE " + DataBase.GUID_COLUMN + " = \"" + GUID + "\"";
        Cursor cursor = database.rawQuery(query, null);
        Boolean Res;
        Res = cursor.moveToFirst();
        cursor.close();

        return Res;
    }


    public void deleteBottle(String GUID) {
        String query = "DELETE FROM " + DataBase.BOTTLES_TABLE;
        query += " WHERE " + DataBase.GUID_COLUMN + "=\"" + GUID + "\"";
        try {
            database.execSQL(query);
        } catch (Exception e) {
            Log.e("Delete Own bottle", e.getMessage());

        }

    }

    public String addBottle(TBottle bottle) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        String GUID = UUID.randomUUID().toString();
        Boolean Edit = isBottle(bottle.getGUID());

        ContentValues values = new ContentValues();

        if (!Edit)
            values.put(DataBase.GUID_COLUMN, GUID);
        values.put(DataBase.COUNTRY_ID_COLUMN, bottle.getCOUNTRY_ID());
        values.put(DataBase.REGION_ID_COLUMN, bottle.getREGION_ID());
        values.put(DataBase.DISTILLERY_GUID_COLUMN, bottle.getDistillery_GUID());
        values.put(DataBase.TYPE_COLUMN, bottle.getTYPE());
        values.put(DataBase.AGE_OF_WHISKY_COLUMN, bottle.getAGE_OF_WHISKY());
        values.put(DataBase.ABV_COLUMN, bottle.getABV());
        values.put(DataBase.BOTTLE_LABEL_COLUMN, bottle.getBOTTLE_LABEL());
        values.put(DataBase.BOTTLE_DESCRIPTION_COLUMN, bottle.getBOTTLE_DESCRIPTION());
        values.put(DataBase.BOTTLE_IMAGE_COLUMN, bottle.getBottle_ImageByte());
        values.put(DataBase.BOTTLE_PPM_COLUMN, bottle.getBOTTLE_PPM());
        values.put(DataBase.BOTTLE_URI_COLUMN, bottle.getBOTTLE_URL());
        values.put(DataBase.NOSE_DESCRIPTION_COLUMN, bottle.getNose_Description());
        values.put(DataBase.TASTE_DESCRIPTION_COLUMN, bottle.getTaste_Description());
        values.put(DataBase.FINISH_DESCRIPTION_COLUMN, bottle.getFinish_Description());
        values.put(DataBase.BALANCE_DESCRIPTION_COLUMN, bottle.getBalance_Description());
        //values.put(DataBase.DATETIME_COLUMN, dateTimeFormat.DateTimeToString(bottle.getDATETIME()));
        try {
            if (Edit)
                database.update(DataBase.BOTTLES_TABLE, values, DataBase.GUID_COLUMN + "=?", new String[]{bottle.getGUID()});
            else
                database.insert(DataBase.BOTTLES_TABLE, null, values);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
        return GUID;
    }

    public List<Integer> getCountries() {
        List<Integer> countries = new ArrayList<Integer>();

        String query = "";
        query += "  Select ";
        query += " DISTINCT " + DataBase.COUNTRY_ID_COLUMN;
        query += "  FROM " + DataBase.BOTTLES_TABLE;
        query += "  ORDER BY " + DataBase.COUNTRY_ID_COLUMN;

        try {
            Cursor c = database.rawQuery(query, null);
            while (c.moveToNext()) {
                countries.add(c.getInt(0));
            }
            c.close();
        } catch (RuntimeException e) {
            Log.e("getCountries", e.getMessage());
        }

        return countries;
    }

    public List<TBottle> getBottles(int Country_ID, int Region_ID) {

        List<TBottle> data = new ArrayList<TBottle>();
        String query = "";
        query += "  Select ";
        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.TYPE_COLUMN;
        query += "," + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.DISTILLATION_DATE_COLUMN;
        query += "," + DataBase.BOTTLING_DATE_COLUMN;
        query += "," + DataBase.ABV_COLUMN;
        query += "," + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLE_URI_COLUMN;
        query += "," + DataBase.NOSE_COLUMN;
        query += "," + DataBase.TASTE_COLUMN;
        query += "," + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BALANCE_COLUMN;
        query += "  FROM " + DataBase.BOTTLES_TABLE;
        query += "  ORDER BY " + DataBase.COUNTRY_ID_COLUMN;

        Cursor c = database.rawQuery(query, null);

        while (c.moveToNext()) {
            TBottle bottle = new TBottle();
            bottle.setGUID(c.getString(0));
            bottle.setCOUNTRY_ID(c.getInt(1));
            bottle.setREGION_ID(c.getInt(2));
            bottle.setDistillery_GUID(c.getString(3));
            bottle.setTYPE(c.getInt(4));
            bottle.setAGE_OF_WHISKY(c.getInt(5));
//            bottle.setDISTILLATION_DATE(c.getString(6));
//            bottle.setBOTTLING_DATE(c.getString(7));
            bottle.setABV(c.getDouble(8));
            bottle.setBOTTLE_LABEL(c.getString(9));
            bottle.setBOTTLE_DESCRIPTION(c.getString(10));
//            bottle.setBOTTLE_IMAGE(c.getString(11));
            bottle.setBOTTLE_PPM(c.getInt(12));
            bottle.setBOTTLE_URL(c.getString(13));
            bottle.setNose_Description(c.getString(14));
            bottle.setTaste_Description(c.getString(15));
            bottle.setFinish_Description(c.getString(16));
            bottle.setBalance_Description(c.getString(17));

            data.add(bottle);
        }
        c.close();
        return data;
    }


    public List<TBottle_info> getBottles_TimeLine(int number, String Distillery_GUID) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TBottle_info> data = new ArrayList<TBottle_info>();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.GUID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.ABV_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.TYPE_COLUMN;

        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BOTTLE_URI_COLUMN;

        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.NOSE_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.TASTE_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BALANCE_COLUMN;
        query += ", datetime(" + DataBase.RATINGS_TABLE + "." + DataBase.DATETIME_COLUMN + ", 'localtime') as " + DataBase.DATETIME_COLUMN;

        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.NOSE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.TASTE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.FINISH_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.BALANCE_DESCRIPTION_COLUMN;

        query += "  FROM " + DataBase.RATINGS_TABLE + "," + DataBase.BOTTLES_LIST_TABLE;
        query += "  WHERE ";
        query += "  " + DataBase.BOTTLES_LIST_TABLE + "." + DataBase.GUID_COLUMN + " = " + DataBase.RATINGS_TABLE + "." + DataBase.BOTTLE_GUID_COLUMN;
        query += "  ORDER BY " + DataBase.RATINGS_TABLE + "."+ DataBase.DATETIME_COLUMN + " DESC";

        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        while (Objects.requireNonNull(c).moveToNext()) {

            TBottle_info bottle = new TBottle_info();
            bottle.getBottle().setGUID(c.getString(0));
            bottle.getBottle().setDistillery_GUID(c.getString(1));
            bottle.getBottle().setCOUNTRY_ID(c.getInt(2));
            bottle.getBottle().setREGION_ID(c.getInt(3));
            bottle.getBottle().setAGE_OF_WHISKY(c.getInt(4));
            bottle.getBottle().setABV(c.getDouble(5));
            bottle.getBottle().setTYPE(c.getInt(6));
            bottle.getBottle().setBOTTLE_LABEL(c.getString(7));
            bottle.getBottle().setBOTTLE_DESCRIPTION(c.getString(8));
            try {
                bottle.getBottle().setBOTTLE_ImageByte(c.getBlob(9));
            } catch (Exception e) {
                bottle.getBottle().setBOTTLE_ImageByte(null);
                Log.e("", e.getMessage());
            }
            bottle.getBottle().setBOTTLE_PPM(c.getInt(10));
            bottle.getBottle().setBOTTLE_URL(c.getString(11));
            bottle.getRating().setNOSE(c.getInt(12));
            bottle.getRating().setTAST(c.getInt(13));
            bottle.getRating().setFINISH(c.getInt(14));
            bottle.getRating().setBALANCE(c.getInt(15));
//            bottle.setLast_DATETIME(c.getString(18));
            try {
                bottle.getRating().setDATETIME(dateTimeFormat.StringToDate(c.getString(16)));
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
            bottle.getDistillery().setDescription(c.getString(17));

            bottle.getBottle().setNose_Description(c.getString(18));
            bottle.getBottle().setTaste_Description(c.getString(19));
            bottle.getBottle().setFinish_Description(c.getString(20));
            bottle.getBottle().setBalance_Description(c.getString(21));

            data.add(bottle);
        }
        c.close();
        return data;
    }


    /**
     * @param number
     * @param Distillery_GUID
     * @return
     */
    public List<TBottle_info> getBottles_Info(int number, String Distillery_GUID) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TBottle_info> data = new ArrayList<TBottle_info>();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.ABV_COLUMN;
        query += "," + DataBase.TYPE_COLUMN;

        query += "," + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLE_URI_COLUMN;

        query += "," + DataBase.NOSE_COLUMN;
        query += "," + DataBase.TASTE_COLUMN;
        query += "," + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BALANCE_COLUMN;
        query += ", datetime(" + DataBase.LAST_DATETIME_COLUMN + ", 'localtime') as " + DataBase.LAST_DATETIME_COLUMN;

        query += "," + DataBase.DESCRIPTION_COLUMN;
        query += "," + DataBase.NOSE_DESCRIPTION_COLUMN;
        query += "," + DataBase.TASTE_DESCRIPTION_COLUMN;
        query += "," + DataBase.FINISH_DESCRIPTION_COLUMN;
        query += "," + DataBase.BALANCE_DESCRIPTION_COLUMN;

        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE;
        if (number == 0) {
            query += "  WHERE " + DataBase.DISTILLERY_GUID_COLUMN + "=\"" + Distillery_GUID + "\"";
            query += "  ORDER BY " + DataBase.BOTTLE_LABEL_COLUMN;
        } else {
            query += "  WHERE " + DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + ">0";
            query += "  ORDER BY " + DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + " DESC";
            query += "  , " + DataBase.LAST_DATETIME_COLUMN + " DESC";
            query += "  LIMIT " + String.valueOf(number);
        }
        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        while (Objects.requireNonNull(c).moveToNext()) {

            TBottle_info bottle = new TBottle_info();
            bottle.getBottle().setGUID(c.getString(0));
            bottle.getBottle().setDistillery_GUID(c.getString(1));
            bottle.getBottle().setCOUNTRY_ID(c.getInt(2));
            bottle.getBottle().setREGION_ID(c.getInt(3));
            bottle.getBottle().setAGE_OF_WHISKY(c.getInt(4));
            bottle.getBottle().setABV(c.getDouble(5));
            bottle.getBottle().setTYPE(c.getInt(6));
            bottle.getBottle().setBOTTLE_LABEL(c.getString(7));
            bottle.getBottle().setBOTTLE_DESCRIPTION(c.getString(8));
            try {
                bottle.getBottle().setBOTTLE_ImageByte(c.getBlob(9));
            } catch (Exception e) {
                bottle.getBottle().setBOTTLE_ImageByte(null);
                Log.e("", e.getMessage());
            }
            bottle.getBottle().setBOTTLE_PPM(c.getInt(10));
            bottle.getBottle().setBOTTLE_URL(c.getString(11));
            bottle.getRating().setNOSE(c.getInt(12));
            bottle.getRating().setTAST(c.getInt(13));
            bottle.getRating().setFINISH(c.getInt(14));
            bottle.getRating().setBALANCE(c.getInt(15));
//            bottle.setLast_DATETIME(c.getString(18));
            try {
                bottle.getRating().setDATETIME(dateTimeFormat.StringToDate(c.getString(16)));
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
            bottle.getDistillery().setDescription(c.getString(17));

            bottle.getBottle().setNose_Description(c.getString(18));
            bottle.getBottle().setTaste_Description(c.getString(19));
            bottle.getBottle().setFinish_Description(c.getString(20));
            bottle.getBottle().setBalance_Description(c.getString(21));

            data.add(bottle);
        }
        c.close();
        return data;
    }

    public TBottle_info getBottles_Info(String Bottle_GUID) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.ABV_COLUMN;
        query += "," + DataBase.TYPE_COLUMN;

        query += "," + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLE_URI_COLUMN;

        query += "," + DataBase.NOSE_COLUMN;
        query += "," + DataBase.TASTE_COLUMN;
        query += "," + DataBase.FINISH_COLUMN;
        query += "," + DataBase.BALANCE_COLUMN;
        query += ", datetime(" + DataBase.LAST_DATETIME_COLUMN + ", 'localtime') as " + DataBase.LAST_DATETIME_COLUMN;

        query += "," + DataBase.DESCRIPTION_COLUMN;
        query += "," + DataBase.NOSE_DESCRIPTION_COLUMN;
        query += "," + DataBase.TASTE_DESCRIPTION_COLUMN;
        query += "," + DataBase.FINISH_DESCRIPTION_COLUMN;
        query += "," + DataBase.BALANCE_DESCRIPTION_COLUMN;

        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE;
        query += "  WHERE " + DataBase.GUID_COLUMN + "=\"" + Bottle_GUID + "\"";
        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        TBottle_info bottle = null;
        if (Objects.requireNonNull(c).moveToNext()) {

            bottle = new TBottle_info();
            bottle.getBottle().setGUID(c.getString(0));
            bottle.getBottle().setDistillery_GUID(c.getString(1));
            bottle.getBottle().setCOUNTRY_ID(c.getInt(2));
            bottle.getBottle().setREGION_ID(c.getInt(3));
            bottle.getBottle().setAGE_OF_WHISKY(c.getInt(4));
            bottle.getBottle().setABV(c.getDouble(5));
            bottle.getBottle().setTYPE(c.getInt(6));
            bottle.getBottle().setBOTTLE_LABEL(c.getString(7));
            bottle.getBottle().setBOTTLE_DESCRIPTION(c.getString(8));
            bottle.getBottle().setBOTTLE_ImageByte(c.getBlob(9));
            bottle.getBottle().setBOTTLE_PPM(c.getInt(10));
            bottle.getBottle().setBOTTLE_URL(c.getString(11));
            bottle.getRating().setNOSE(c.getInt(12));
            bottle.getRating().setTAST(c.getInt(13));
            bottle.getRating().setFINISH(c.getInt(14));
            bottle.getRating().setBALANCE(c.getInt(15));
//            bottle.setLast_DATETIME(c.getString(18));
            bottle.getDistillery().setDescription(c.getString(17));

            bottle.getBottle().setNose_Description(c.getString(18));
            bottle.getBottle().setTaste_Description(c.getString(19));
            bottle.getBottle().setFinish_Description(c.getString(20));
            bottle.getBottle().setBalance_Description(c.getString(21));

        }
        c.close();
        return bottle;
    }


    public List<TBottle_info> getSearchBottles(int number, String Label, int Nose, int Taste, int Finish, int Balance, int Total, int age, boolean Decrease, boolean Rated) {


        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TBottle_info> data = new ArrayList<TBottle_info>();
        String query = "";
        query += "  Select ";

        query += " " + DataBase.GUID_COLUMN;
        query += "," + DataBase.DISTILLERY_GUID_COLUMN;
        query += "," + DataBase.COUNTRY_ID_COLUMN;
        query += "," + DataBase.REGION_ID_COLUMN;
        query += "," + DataBase.AGE_OF_WHISKY_COLUMN;
        query += "," + DataBase.ABV_COLUMN;
        query += "," + DataBase.TYPE_COLUMN;

        query += "," + DataBase.BOTTLE_LABEL_COLUMN;
        query += "," + DataBase.BOTTLE_DESCRIPTION_COLUMN;
        query += "," + DataBase.BOTTLE_IMAGE_COLUMN;
        query += "," + DataBase.BOTTLE_PPM_COLUMN;
        query += "," + DataBase.BOTTLE_URI_COLUMN;

        query += ", IFNULL(" + DataBase.NOSE_COLUMN + ",0)";
        query += ", IFNULL(" + DataBase.TASTE_COLUMN + ",0)";
        query += ", IFNULL(" + DataBase.FINISH_COLUMN + ",0)";
        query += ", IFNULL(" + DataBase.BALANCE_COLUMN + ",0)";
        query += ", datetime(" + DataBase.LAST_DATETIME_COLUMN + ", 'localtime') as " + DataBase.LAST_DATETIME_COLUMN;

        query += "," + DataBase.DESCRIPTION_COLUMN;
        query += "," + DataBase.NOSE_DESCRIPTION_COLUMN;
        query += "," + DataBase.TASTE_DESCRIPTION_COLUMN;
        query += "," + DataBase.FINISH_DESCRIPTION_COLUMN;
        query += "," + DataBase.BALANCE_DESCRIPTION_COLUMN;

        query += "  FROM " + DataBase.BOTTLES_LIST_TABLE;
        query += "  WHERE ";
        query += "    ( " + DataBase.BOTTLE_LABEL_COLUMN + " LIKE \"%" + Label + "%\"";
        query += "     OR " + DataBase.BOTTLE_DESCRIPTION_COLUMN + " LIKE \"%" + Label + "%\"";
        query += "    ) ";

        if (Total > 0) {
            query += "  AND ";
            query += DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + ">=" + String.valueOf(Total);
        }
        if (Nose > 0) {
            query += "  AND ";
            query += DataBase.NOSE_COLUMN + ">=" + String.valueOf(Nose);
        }
        if (Taste > 0) {
            query += "  AND ";
            query += DataBase.TASTE_COLUMN + ">=" + String.valueOf(Taste);
        }
        if (Finish > 0) {
            query += "  AND ";
            query += DataBase.FINISH_COLUMN + ">=" + String.valueOf(Finish);
        }
        if (Balance > 0) {
            query += "  AND ";
            query += DataBase.BALANCE_COLUMN + ">=" + String.valueOf(Balance);
        }
        if (age > 0) {
            query += "  AND ";
            query += DataBase.AGE_OF_WHISKY_COLUMN + ">=" + String.valueOf(age);

        }

        if (Rated) {
            query += "  AND ";
            query += DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN + ">0";

        }

        query += "  ORDER BY " + DataBase.NOSE_COLUMN + "+" + DataBase.TASTE_COLUMN + "+" + DataBase.FINISH_COLUMN + "+" + DataBase.BALANCE_COLUMN;
        if (Decrease)
            query += " DESC";
        else
            query += " ASC";

        query += "  , " + DataBase.LAST_DATETIME_COLUMN + " ASC";

        if (number > 0)
            query += "  LIMIT " + String.valueOf(number);

        Cursor c = null;
        try {
            c = database.rawQuery(query, null);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        while (Objects.requireNonNull(c).moveToNext()) {

            TBottle_info bottle = new TBottle_info();
            bottle.getBottle().setGUID(c.getString(0));
            bottle.getBottle().setDistillery_GUID(c.getString(1));
            bottle.getBottle().setCOUNTRY_ID(c.getInt(2));
            bottle.getBottle().setREGION_ID(c.getInt(3));
            bottle.getBottle().setAGE_OF_WHISKY(c.getInt(4));
            bottle.getBottle().setABV(c.getDouble(5));
            bottle.getBottle().setTYPE(c.getInt(6));
            bottle.getBottle().setBOTTLE_LABEL(c.getString(7));
            bottle.getBottle().setBOTTLE_DESCRIPTION(c.getString(8));
            try {
                bottle.getBottle().setBOTTLE_ImageByte(c.getBlob(9));
            } catch (Exception e) {
                bottle.getBottle().setBOTTLE_ImageByte(null);
                Log.e("", e.getMessage());
            }
            bottle.getBottle().setBOTTLE_PPM(c.getInt(10));
            bottle.getBottle().setBOTTLE_URL(c.getString(11));
            bottle.getRating().setNOSE(c.getInt(12));
            bottle.getRating().setTAST(c.getInt(13));
            bottle.getRating().setFINISH(c.getInt(14));
            bottle.getRating().setBALANCE(c.getInt(15));
//            bottle.setLast_DATETIME(c.getString(18));
            bottle.getDistillery().setDescription(c.getString(17));

            bottle.getBottle().setNose_Description(c.getString(18));
            bottle.getBottle().setTaste_Description(c.getString(19));
            bottle.getBottle().setFinish_Description(c.getString(20));
            bottle.getBottle().setBalance_Description(c.getString(21));

            data.add(bottle);
        }
        c.close();
        return data;
    }


}
