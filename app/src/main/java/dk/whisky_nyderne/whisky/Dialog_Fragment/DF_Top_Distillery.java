package dk.whisky_nyderne.whisky.Dialog_Fragment;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Distillery_Top_List;
import dk.whisky_nyderne.whisky.Data.TDistilleryTop;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Top_Distillery;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.ListView_Tools;

public class DF_Top_Distillery extends DialogFragment {

    private View rootview;

    public static DF_Top_Distillery newInstance() {


        DF_Top_Distillery df_top_distillery = new DF_Top_Distillery();


        return df_top_distillery;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.df_top_distillery, null);
        rootview = view;
        addEvents();
        updateList();
        return view;
    }

    private void addEvents() {

    }

    private void updateList(){
        ListView LVTopDistillery = (ListView) rootview.findViewById(R.id.LVTopDistillery);

        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
        final List<TDistilleryTop> bottle_top_list = dbDistillery.getTopDistillery(100,"");

        Adapter_Distillery_Top_List customAdapterTop = new Adapter_Distillery_Top_List();

        for (int i = 0; i < bottle_top_list.size(); i++) {
            customAdapterTop.addItem(bottle_top_list.get(i));
        }
        LVTopDistillery.setAdapter(customAdapterTop);
        LVTopDistillery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                top_distillery.GUID = bottle_top_list.get(position).getGUID();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, top_distillery)
                        .commit();
                dismiss();
            }
        });

//        ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
//        lvt.setListViewHeightBasedOnItems(LVTopDistillery);

    }

}
