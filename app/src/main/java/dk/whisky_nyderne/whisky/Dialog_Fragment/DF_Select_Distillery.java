package dk.whisky_nyderne.whisky.Dialog_Fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dk.whisky_nyderne.whisky.R;

/**
 * Created by hans on 22-09-17.
 */

public class DF_Select_Distillery extends DialogFragment {

    private View rootview;

    public static DF_Select_Distillery newInstance(int myIndex) {

        DF_Select_Distillery get_amount = new DF_Select_Distillery();

        Bundle args = new Bundle();
        args.putInt("id", myIndex);
        get_amount.setArguments(args);

        return get_amount;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //int myInteger = getArguments().getInt("id");

        View view = inflater.inflate(R.layout.df_select_distillery, null);
        rootview = view;
        addEvents();

        return view;
    }

    private void addEvents() {

    }
}
