package dk.whisky_nyderne.whisky.Dialog_Fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dk.whisky_nyderne.whisky.R;

public class DF_Search extends DialogFragment {
    private View rootview;

    public static DF_Search newInstance() {

        DF_Search df_container = new DF_Search();


        return df_container;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.df_search, null);
        rootview = view;

        return view;

    }

}
