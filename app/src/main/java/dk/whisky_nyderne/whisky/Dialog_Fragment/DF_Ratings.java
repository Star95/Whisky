package dk.whisky_nyderne.whisky.Dialog_Fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Ratings;
import dk.whisky_nyderne.whisky.Data.TRating;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBRatings;
import dk.whisky_nyderne.whisky.R;

public class DF_Ratings extends DialogFragment {
    private View rootview;
    private String GUID;

    public static DF_Ratings newInstance(String Bottle_GUID) {

        DF_Ratings df_ratings = new DF_Ratings();

        Bundle args = new Bundle();
        args.putString("GUID", Bottle_GUID);
        df_ratings.setArguments(args);

        return df_ratings;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        GUID = getArguments().getString("GUID");

        View view = inflater.inflate(R.layout.df_ratings, null);
        rootview = view;
        update();
        return view;
    }

    private void update() {
        ListView LVRatings= rootview.findViewById(R.id.LVRatings);
        final List<TRating> list;
        DBRatings dbRatings = new DBRatings(rootview.getContext());
        list = dbRatings.getRatings(GUID,false);

        Adapter_Ratings customAdapter = new Adapter_Ratings();

        for (int i = 0; i <= list.size() - 1; i++) {
            customAdapter.addItem(list.get(i));
        }

        LVRatings.setAdapter(customAdapter);

    }
}
