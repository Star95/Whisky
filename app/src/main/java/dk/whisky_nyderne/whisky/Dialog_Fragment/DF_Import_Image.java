package dk.whisky_nyderne.whisky.Dialog_Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.InputStream;

import dk.whisky_nyderne.whisky.Fragment.Fragment_Create_Bottle;
import dk.whisky_nyderne.whisky.R;

import static dk.whisky_nyderne.whisky.MainActivity.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE;
import static dk.whisky_nyderne.whisky.MainActivity.SELECT_PHOTO_REQUEST_CODE;

/**
 * Created by hans on 24-10-17.
 */

public class DF_Import_Image extends DialogFragment {

    private View rootview;
    private int TargetImageID;

    public static DF_Import_Image newInstance(int TargetID) {

        DF_Import_Image df_import_image = new DF_Import_Image();

        Bundle args = new Bundle();
        args.putInt("TargetID", TargetID);
        df_import_image.setArguments(args);

        return df_import_image;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        TargetImageID = getArguments().getInt("TargetID");

        View view = inflater.inflate(R.layout.df_import_image, null);
        rootview = view;
        addEvents();

        return view;
    }

    private void addEvents() {

        Button BURL = (Button) rootview.findViewById(R.id.BURL);
        BURL.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                importURL();
                dismiss();
            }
        });
        Button BPhoto = (Button) rootview.findViewById(R.id.BPhoto);
        BPhoto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                importPhoto();
                dismiss();
            }
        });
        Button BGallery = (Button) rootview.findViewById(R.id.BGallery);
        BGallery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                importGallery();
                dismiss();

            }
        });
        Button BCansel = (Button) rootview.findViewById(R.id.BCansel);
        BCansel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismiss();

            }
        });


    }


    private void importURL() {


        final Dialog dialog = new Dialog(rootview.getContext(), android.R.style.Theme_Holo);

        dialog.setContentView(R.layout.dialog_url);
        dialog.setTitle("URL");

        Button dialog_ok = (Button) dialog.findViewById(R.id.dialog_ok);

        dialog_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView url = (TextView) dialog.findViewById(R.id.dialog_url);
                url.setText("http://frontpos.com/prev1.gif");
                final String image_url = url.getText().toString();

                final ImageLoader imageLoader = ImageLoader.getInstance();

                imageLoader.loadImage(image_url, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        SendResult(imageUri);
                    }
                });

                dialog.dismiss();
            }

        });

        dialog.show();

    }

    private void SendResult(String URI) {

        switch (TargetImageID) {
            case 1: {
                Fragment_Create_Bottle fragment = (Fragment_Create_Bottle) getFragmentManager().findFragmentById(R.id.container);
                fragment.CallBackPhoto(Uri.parse(URI));
                break;
            }
            case 2: {
                break;
            }
        }

    }



    private void importGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
     getActivity().startActivityForResult(photoPickerIntent, SELECT_PHOTO_REQUEST_CODE);

    }

    private void importPhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

}









