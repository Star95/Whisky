package dk.whisky_nyderne.whisky.Data;

import java.util.Date;

/**
 * Created by hans on 22-09-17.
 *
 */

public class TDistillery {

    private String GUID;
    private int COUNTRY_ID;
    private int REGION_ID;
    private String DISTILLERY_GUID;
    private String Description;
    private Date DATETIME;
    private String Address;
    private String ZipCode;
    private String Country;
    private String Phone;
    private String GPS_N;
    private String GPS_E;
    private String Note;
    private String HomePage;
    private String Wikipedia;


    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public int getCOUNTRY_ID() {
        return COUNTRY_ID;
    }

    public void setCOUNTRY_ID(int COUNTRY_ID) {
        this.COUNTRY_ID = COUNTRY_ID;
    }

    public int getREGION_ID() {
        return REGION_ID;
    }

    public void setREGION_ID(int REGION_ID) {
        this.REGION_ID = REGION_ID;
    }

    public String getDISTILLERY_GUID() {
        return DISTILLERY_GUID;
    }

    public void setDISTILLERY_GUID(String DISTILLERY_GUID) {
        this.DISTILLERY_GUID = DISTILLERY_GUID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public Date getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(Date DATETIME) {
        this.DATETIME = DATETIME;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getGPS_N() {
        return GPS_N;
    }

    public void setGPS_N(String GPS_N) {
        this.GPS_N = GPS_N;
    }

    public String getGPS_E() {
        return GPS_E;
    }

    public void setGPS_E(String GPS_E) {
        this.GPS_E = GPS_E;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getHomePage() {
        return HomePage;
    }

    public void setHomePage(String homePage) {
        HomePage = homePage;
    }

    public String getWikipedia() {
        return Wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        Wikipedia = wikipedia;
    }
}
