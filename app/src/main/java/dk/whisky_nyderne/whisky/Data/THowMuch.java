package dk.whisky_nyderne.whisky.Data;

public class THowMuch {
    private int Volume=0;
    private int No_of_Drinks=0;
    private int No_of_Types=0;

    public int getVolume() {
        return Volume;
    }

    public void setVolume(int volume) {
        Volume = volume;
    }

    public int getNo_of_Drinks() {
        return No_of_Drinks;
    }

    public void setNo_of_Drinks(int no_of_Drinks) {
        No_of_Drinks = no_of_Drinks;
    }

    public int getNo_of_Types() {
        return No_of_Types;
    }

    public void setNo_of_Types(int no_of_Types) {
        No_of_Types = no_of_Types;
    }
}
