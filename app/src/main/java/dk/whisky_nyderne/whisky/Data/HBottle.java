package dk.whisky_nyderne.whisky.Data;

public class HBottle {
    private TBottle_info bottle_info;
    private TDistillery distillery;
    private int type = 0;

    public HBottle() {
        bottle_info = new TBottle_info();
        distillery = new TDistillery();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public TBottle_info getBottle_info() {
        return bottle_info;
    }

    public void setBottle_info(TBottle_info bottle_info) {
        this.bottle_info = bottle_info;
    }

    public TDistillery getDistillery() {
        return distillery;
    }

    public void setDistillery(TDistillery distillery) {
        this.distillery = distillery;
    }
}
