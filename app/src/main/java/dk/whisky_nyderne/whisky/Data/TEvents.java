package dk.whisky_nyderne.whisky.Data;

import java.util.Date;

/**
 * Created by hans on 22-09-17.
 */

public class TEvents {

          String GUID;
            String DESCRIPTION;
            Date FROM_DATETIME;
            Date TO_DATETIME;
            Date DATETIME;

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public Date getFROM_DATETIME() {
        return FROM_DATETIME;
    }

    public void setFROM_DATETIME(Date FROM_DATETIME) {
        this.FROM_DATETIME = FROM_DATETIME;
    }

    public Date getTO_DATETIME() {
        return TO_DATETIME;
    }

    public void setTO_DATETIME(Date TO_DATETIME) {
        this.TO_DATETIME = TO_DATETIME;
    }

    public Date getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(Date DATETIME) {
        this.DATETIME = DATETIME;
    }
}
