package dk.whisky_nyderne.whisky.Data;

import java.util.Date;

/**
 * Created by hans on 22-09-17.
 */

public class TRating {

    private String GUID;
    private int NOSE = 1;
    private int TAST = 1;
    private int FINISH = 1;
    private int BALANCE = 1;
    private String BOTTLE_GUID;
    private String USER_GUID;
    private String NOTE;
    private int WATER;
    private int Size;
    private Date DATETIME;

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public int getNOSE() {
        return NOSE;
    }

    public void setNOSE(int NOSE) {
        this.NOSE = NOSE;
    }

    public int getTAST() {
        return TAST;
    }

    public void setTAST(int TAST) {
        this.TAST = TAST;
    }

    public int getFINISH() {
        return FINISH;
    }

    public void setFINISH(int FINISH) {
        this.FINISH = FINISH;
    }

    public int getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(int BALANCE) {
        this.BALANCE = BALANCE;
    }

    public String getBOTTLE_GUID() {
        return BOTTLE_GUID;
    }

    public void setBOTTLE_GUID(String BOTTLE_GUID) {
        this.BOTTLE_GUID = BOTTLE_GUID;
    }

    public String getUSER_GUID() {
        return USER_GUID;
    }

    public void setUSER_GUID(String USER_GUID) {
        this.USER_GUID = USER_GUID;
    }

    public String getNOTE() {
        return NOTE;
    }

    public void setNOTE(String NOTE) {
        this.NOTE = NOTE;
    }

    public int getWATER() {
        return WATER;
    }

    public void setWATER(int WATER) {
        this.WATER = WATER;
    }

    public Date getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(Date DATETIME) {
        this.DATETIME = DATETIME;
    }

    public int getAVG() {
        return this.NOSE + this.TAST + this.FINISH + this.BALANCE;
    }

    public int getSize() {
        return Size;
    }

    public void setSize(int size) {
        Size = size;
    }
}
