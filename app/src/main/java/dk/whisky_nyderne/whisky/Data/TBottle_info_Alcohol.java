package dk.whisky_nyderne.whisky.Data;

public class TBottle_info_Alcohol extends TBottle_info {
    private int RestPromille;
    private float Promille;

    public int getRestPromille() {
        return RestPromille;
    }

    public void setRestPromille(int restPromille) {
        RestPromille = restPromille;
    }

    public float getPromille() {
        return Promille;
    }

    public void setPromille(float Promille) {
        this.Promille = Promille;
    }

}
