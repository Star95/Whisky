package dk.whisky_nyderne.whisky.Data;

/**
 * Created by hans on 01-11-17.
 */

public class TRating_info {
    int max = 0;
    int min = 0;
    int avg = 0;
    int rank = 0;

    public TRating_info(int rating) {
        this.max = rating;
        this.min = rating;
        this.avg = rating;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getAvg() {
        return avg;
    }

    public void setAvg(int amount) {
        this.avg = amount;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
