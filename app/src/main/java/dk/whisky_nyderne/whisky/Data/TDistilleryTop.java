package dk.whisky_nyderne.whisky.Data;

import java.util.Date;

public class TDistilleryTop {
   private int Result;
   private String GUID;
   private String Description;
   private int CountryID;
   private Date DateTime;

    public int getResult() {
        return Result;
    }

    public void setResult(int result) {
        Result = result;
    }

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getCountryID() {
        return CountryID;
    }

    public Date getDateTime() {
        return DateTime;
    }

    public void setDateTime(Date dateTime) {
        DateTime = dateTime;
    }

    public void setCountryID(int countryID) {
        CountryID = countryID;
    }
}
