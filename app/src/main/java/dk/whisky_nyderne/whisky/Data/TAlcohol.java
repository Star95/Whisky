package dk.whisky_nyderne.whisky.Data;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TAlcohol {

    private boolean Femal;
    private float Weight;
    private int ml;
    private float Procent;
    private static final float Density = (float) 0.7873;
    private Date DateTime;


    public TAlcohol(boolean Femal, float Weight) {
        this.Femal = Femal;
        this.Weight = Weight;
    }

    public void SetSex(boolean Femal) {
        this.Femal = Femal;
    }

    public void SetWeight(float Weight) {
        this.Weight = Weight;
    }

    public void SetMl(int ml) {
        this.ml = ml;
    }

    public void SetProcent(float procent) {
        this.Procent = procent;
    }

    public void SetDatetime(Date datetime) {
        this.DateTime = datetime;
    }

    public Date GetDatetime() {
        return this.DateTime;
    }

    public float GetGramAlkohol() {
        float g;
        g = this.ml * (this.Procent/ 100) * Density ;
        return g;

    }

    public int getRestPromilleProcent() {
        Date tmp_Date = this.DateTime;
        float promille = getPromille();
        this.DateTime= new Date();
        float prePromille= getPromille();
        this.DateTime=tmp_Date;
        int r=0;
        try {
             r = (int) (100 / prePromille * promille);
        }catch (Exception e){
            r=0;
        }


        return r;
    }


    public float getPromille() {

        Date date = new Date();
        float timefromstart;
        float result;

        long r = ((date.getTime() / 60000) - (GetDatetime().getTime() / 60000));



        timefromstart = (float) r / 60;

        if (this.Femal)
            result = (float) (GetGramAlkohol() / (this.Weight * 0.55) - (0.015 * timefromstart));
        else
            result = (float) (GetGramAlkohol() / (this.Weight * 0.68) - (0.015 * (timefromstart)));

        return result;
    }
}
