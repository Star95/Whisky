package dk.whisky_nyderne.whisky.Data;


/**
 * Created by hans on 11-11-17.
 *
 */

public class TBottle_info{
    private TBottle bottle;
    private TRating rating;
    private TDistillery distillery;

    public TBottle_info() {
        bottle = new TBottle();
        rating = new TRating();
        distillery = new TDistillery();
    }

    public  TBottle getBottle() {
        return this.bottle;
    }

    public void setBottle(TBottle bottle) {
        this.bottle = bottle;
    }

    public TRating getRating() {
        return rating;
    }

    public  void setRating(TRating rating) {
        this.rating = rating;
    }

    public TDistillery getDistillery() {
        return distillery;
    }

    public void setDistillery(TDistillery distillery) {
        this.distillery = distillery;
    }
}
