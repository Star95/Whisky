package dk.whisky_nyderne.whisky.Data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.UUID;

/**
 * Created by hans on 22-09-17.
 *
 */

public class TBottle{

    private String GUID;
    private int COUNTRY_ID;
    private int REGION_ID;
    private String Distillery_GUID;
    private int TYPE;
    private int AGE_OF_WHISKY;
    private double ABV;
    private String BOTTLE_LABEL;
    private String BOTTLE_DESCRIPTION;
    private Bitmap BOTTLE_IMAGE;
    private int BOTTLE_PPM;
    private String BOTTLE_URI;
    private Date DATETIME;
    private String Nose_Description;
    private String Taste_Description;
    private String Finish_Description;
    private String Balance_Description;

    public TBottle() {
        this.GUID = UUID.randomUUID().toString();
    }

    public byte[] getBottle_ImageByte() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bm = this.BOTTLE_IMAGE;
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte imageInByte[] = stream.toByteArray();
        return imageInByte;

    }

    public void setBOTTLE_ImageByte(byte[] imageByte) {
        if (imageByte != null) {
            this.BOTTLE_IMAGE = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            imageByte = null;

            this.BOTTLE_IMAGE = Bitmap.createScaledBitmap(this.BOTTLE_IMAGE, 160, 120, true);

        }
    }

    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }


    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public int getCOUNTRY_ID() {
        return COUNTRY_ID;
    }

    public void setCOUNTRY_ID(int COUNTRY_ID) {
        this.COUNTRY_ID = COUNTRY_ID;
    }

    public int getREGION_ID() {
        return REGION_ID;
    }

    public void setREGION_ID(int REGION_ID) {
        this.REGION_ID = REGION_ID;
    }

    public String getDistillery_GUID() {
        return Distillery_GUID;
    }

    public void setDistillery_GUID(String distillery_GUID) {
        this.Distillery_GUID = distillery_GUID;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public int getAGE_OF_WHISKY() {
        return AGE_OF_WHISKY;
    }

    public void setAGE_OF_WHISKY(int AGE_OF_WHISKY) {
        this.AGE_OF_WHISKY = AGE_OF_WHISKY;
    }


    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public String getBOTTLE_LABEL() {
        return BOTTLE_LABEL;
    }

    public void setBOTTLE_LABEL(String BOTTLE_LABEL) {
        this.BOTTLE_LABEL = BOTTLE_LABEL;
    }

    public String getBOTTLE_DESCRIPTION() {
        return BOTTLE_DESCRIPTION;
    }

    public void setBOTTLE_DESCRIPTION(String BOTTLE_DESCRIPTION) {
        this.BOTTLE_DESCRIPTION = BOTTLE_DESCRIPTION;
    }

    public Bitmap getBOTTLE_IMAGE() {
        return BOTTLE_IMAGE;
    }

    public void setBOTTLE_IMAGE(Bitmap BOTTLE_IMAGE) {
        this.BOTTLE_IMAGE = BOTTLE_IMAGE;
    }

    public int getBOTTLE_PPM() {
        return BOTTLE_PPM;
    }

    public void setBOTTLE_PPM(int BOTTLE_PPM) {
        this.BOTTLE_PPM = BOTTLE_PPM;
    }

    public String getBOTTLE_URL() {
        return BOTTLE_URI;
    }

    public void setBOTTLE_URL(String BOTTLE_URL) {
        this.BOTTLE_URI = BOTTLE_URL;
    }

    public Date getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(Date DATETIME) {
        this.DATETIME = DATETIME;
    }

    public String getNose_Description() {
        return Nose_Description;
    }

    public void setNose_Description(String nose_Description) {
        Nose_Description = nose_Description;
    }

    public String getTaste_Description() {
        return Taste_Description;
    }

    public void setTaste_Description(String taste_Description) {
        Taste_Description = taste_Description;
    }

    public String getFinish_Description() {
        return Finish_Description;
    }

    public void setFinish_Description(String finish_Description) {
        Finish_Description = finish_Description;
    }

    public String getBalance_Description() {
        return Balance_Description;
    }

    public void setBalance_Description(String balance_Description) {
        Balance_Description = balance_Description;
    }
}
