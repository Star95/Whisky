package dk.whisky_nyderne.whisky.Data;

import com.akshaykale.swipetimeline.TimelineObject;

public class TimeLineItem implements TimelineObject {

    long timestamp;
    String name, uri;
    private TBottle_info bottle_info;

    public TimeLineItem(long l, String a, String url, TBottle_info _bottle_info) {
        timestamp = l;
        name = a;
        uri = url;
        bottle_info = _bottle_info;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String getTitle() {
        return name;
    }

    @Override
    public String getImageUrl() {
        return uri;
    }

    public TBottle_info getBottle_info() {
        return bottle_info;
    }

    public void setBottle_info(TBottle_info bottle_info) {
        this.bottle_info = bottle_info;
    }
}
