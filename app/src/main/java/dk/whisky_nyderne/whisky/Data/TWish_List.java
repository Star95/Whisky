package dk.whisky_nyderne.whisky.Data;

import java.util.Date;

/**
 * Created by hans on 22-09-17.
 */

public class TWish_List {
    String BOTTLE_GUID;
    String USER_GUID;
    Date DATETIME;

    public String getBOTTLE_GUID() {
        return BOTTLE_GUID;
    }

    public void setBOTTLE_GUID(String BOTTLE_GUID) {
        this.BOTTLE_GUID = BOTTLE_GUID;
    }

    public String getUSER_GUID() {
        return USER_GUID;
    }

    public void setUSER_GUID(String USER_GUID) {
        this.USER_GUID = USER_GUID;
    }

    public Date getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(Date DATETIME) {
        this.DATETIME = DATETIME;
    }
}
