package dk.whisky_nyderne.whisky.Data;

import java.util.Date;

/**
 * Created by hans on 22-09-17.
 */

public class TOwner {
    private String GUID;
    private String BOTTLE_GUID;
    private String USER_GUID;
    private Double Size = 0.0;
    private Date Open_DateTime;
    private Date Empty_Datetime;
    private Date DATETIME;
    private int type = 0;//0=Not open, 1=Open, 2=Empty
    private double Price=0;
    private String Note;

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public String getBOTTLE_GUID() {
        return BOTTLE_GUID;
    }

    public void setBOTTLE_GUID(String BOTTLE_GUID) {
        this.BOTTLE_GUID = BOTTLE_GUID;
    }

    public String getUSER_GUID() {
        return USER_GUID;
    }

    public void setUSER_GUID(String USER_GUID) {
        this.USER_GUID = USER_GUID;
    }

    public Double getSize() {
        return Size;
    }

    public void setSize(Double size) {
        Size = size;
    }

    public Date getOpen_DateTime() {
        return Open_DateTime;
    }

    public void setOpen_DateTime(Date open_DateTime) {
        Open_DateTime = open_DateTime;
    }

    public Date getEmpty_Datetime() {
        return Empty_Datetime;
    }

    public void setEmpty_Datetime(Date empty_Datetime) {
        Empty_Datetime = empty_Datetime;
    }

    public Date getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(Date DATETIME) {
        this.DATETIME = DATETIME;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }
}
