package dk.whisky_nyderne.whisky.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.TImage;

public class Adapter_Top_List_mini extends BaseAdapter {
    final ArrayList<TBottle_info> listArray;

    public Adapter_Top_List_mini() {
        listArray = new ArrayList<TBottle_info>();
    }

    public void addItem(TBottle_info bottle) {
        listArray.add(bottle);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TBottle_info bottle = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_top_list_mini, parent, false);

        TextView TVLabel =  view.findViewById(R.id.TVLabel);
        TextView TVDistilledName =  view.findViewById(R.id.TVDistilledName);
        TextView TVRankNumber =  view.findViewById(R.id.TVRankNumber);
        TextView TVTotal = view.findViewById(R.id.TVTotal);



        TVLabel.setText(bottle.getBottle().getBOTTLE_LABEL());
        TVDistilledName.setText(bottle.getDistillery().getDescription());
        TVRankNumber.setText(String.valueOf(index + 1));


        TImage image = TImage.getInstance();
        ImageView IVBottle =  view.findViewById(R.id.IVBottle);
        IVBottle.setImageBitmap(image.getRoundedCornerBitmap(bottle.getBottle().getBOTTLE_IMAGE(), 16));

        TVTotal.setText(String.valueOf(bottle.getRating().getAVG())+"p");

        return view;
    }

}