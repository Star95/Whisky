package dk.whisky_nyderne.whisky.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.whisky.Data.HBottle;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Color_Rating;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.TImage;

import static dk.whisky_nyderne.whisky.Tools.Color_Rating.getColor;

public class Adapter_Search_Bottle_Distillery extends BaseAdapter {

    private boolean toplist = false;
    final ArrayList<HBottle> listArray;
    //"#f69421"orange


    public Adapter_Search_Bottle_Distillery(boolean _toplist) {
        listArray = new ArrayList<HBottle>();
        toplist = _toplist;
    }


    public void addItem(HBottle bottle) {
        listArray.add(bottle);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final HBottle bottle = listArray.get(index);
        GradientDrawable gd;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (bottle.getType() == 1) {
            view = inflater.inflate(R.layout.row_bottles, parent, false);


            TextView TVNumber = (TextView) view.findViewById(R.id.TVNumber);
            TVNumber.setText(String.valueOf(index + 1));


            ImageView IVFlag = (ImageView) view.findViewById(R.id.IVFlag);


            if (bottle.getBottle_info().getDistillery() != null) {

                //** Country **
                //*************
                TCountries contries = TCountries.getInstance(view.getContext());
                TCountry country = contries.getInfo(bottle.getBottle_info().getBottle().getCOUNTRY_ID());


                IVFlag.setImageResource(country.getDrawableID());
                IVFlag.setMaxWidth(10);
                TImage image = TImage.getInstance();

                ImageView IVBottleImage = (ImageView) view.findViewById(R.id.IVBottleImage);
                IVBottleImage.setImageBitmap(image.getRoundedCornerBitmap(bottle.getBottle_info().getBottle().getBOTTLE_IMAGE(), 16));

                //** Distillery **
                //****************

                TextView TVDistillery_Name = (TextView) view.findViewById(R.id.TVDistillery_Name);
                TVDistillery_Name.setText(bottle.getBottle_info().getDistillery().getDescription());

                TextView TVPhone = (TextView) view.findViewById(R.id.TVPhone);
                TVPhone.setText("Phone : " + bottle.getBottle_info().getDistillery().getPhone());
                //** Distillery address **
                //************************
                TextView TVAddress = (TextView) view.findViewById(R.id.TVAddress);
                TVAddress.setText(bottle.getBottle_info().getDistillery().getAddress());

                //** Distillery Zip code and city name **
                //***************************************
                TextView TVZIPCODE = (TextView) view.findViewById(R.id.TVZIPCODE);
                TVZIPCODE.setText(bottle.getBottle_info().getDistillery().getZipCode() + " " + bottle.getBottle_info().getDistillery().getCountry());

            }

            TextView TVBottle_Label = (TextView) view.findViewById(R.id.TVBottle_Label);
            TVBottle_Label.setText(bottle.getBottle_info().getBottle().getBOTTLE_LABEL());

            TextView TVTotal_Value = (TextView) view.findViewById(R.id.TVTotal_Value);
            TVTotal_Value.setText(String.valueOf(bottle.getBottle_info().getRating().getAVG()));
            TVTotal_Value.setBackgroundResource(R.drawable.button_shape_orange);
            gd = (GradientDrawable) TVTotal_Value.getBackground().getCurrent();
            gd.setColor(Color.parseColor(Color_Rating.getColorTotal(bottle.getBottle_info().getRating().getAVG())));

            TVTotal_Value.setPadding(15, 0, 15, 0);


            TextView TVAge = (TextView) view.findViewById(R.id.TVAge);
            TVAge.setText(String.valueOf(bottle.getBottle_info().getBottle().getAGE_OF_WHISKY()) + " year");

            TextView TVPercentag = (TextView) view.findViewById(R.id.TVPercentag);
            TVPercentag.setText(String.valueOf(bottle.getBottle_info().getBottle().getABV()) + " %");


            TextView TVType = (TextView) view.findViewById(R.id.TVType);
            switch (bottle.getBottle_info().getBottle().getTYPE()) {
                case 0: {
                    TVType.setText("Blended");
                    break;
                }
                case 1: {
                    TVType.setText("Single malt");
                    break;
                }

            }

            TextView TVNose_Value = (TextView) view.findViewById(R.id.TVNose_Value);
            TVNose_Value.setText("N = " + String.valueOf(bottle.getBottle_info().getRating().getNOSE()));
            TVNose_Value.setBackgroundResource(R.drawable.button_shape_orange);
            gd = (GradientDrawable) TVNose_Value.getBackground().getCurrent();
            gd.setColor(Color.parseColor(getColor(bottle.getBottle_info().getRating().getNOSE())));


            TextView TVTast_Value = (TextView) view.findViewById(R.id.TVTast_Value);
            TVTast_Value.setText("T = " + String.valueOf(bottle.getBottle_info().getRating().getTAST()));
            TVTast_Value.setBackgroundResource(R.drawable.button_shape_orange);
            gd = (GradientDrawable) TVTast_Value.getBackground().getCurrent();
            gd.setColor(Color.parseColor(getColor(bottle.getBottle_info().getRating().getTAST())));


            TextView TVFinish_Value = (TextView) view.findViewById(R.id.TVFinish_Value);
            TVFinish_Value.setText("F = " + String.valueOf(bottle.getBottle_info().getRating().getFINISH()));
            TVFinish_Value.setBackgroundResource(R.drawable.button_shape_orange);
            gd = (GradientDrawable) TVFinish_Value.getBackground().getCurrent();
            gd.setColor(Color.parseColor(getColor(bottle.getBottle_info().getRating().getFINISH())));


            TextView TVBalance_Value = (TextView) view.findViewById(R.id.TVBalance_Value);
            TVBalance_Value.setText("B = " + String.valueOf(bottle.getBottle_info().getRating().getBALANCE()));
            TVBalance_Value.setBackgroundResource(R.drawable.button_shape_orange);
            gd = (GradientDrawable) TVBalance_Value.getBackground().getCurrent();
            gd.setColor(Color.parseColor(getColor(bottle.getBottle_info().getRating().getBALANCE())));


            if (toplist) {
                TextView TVTopText = (TextView) view.findViewById(R.id.TVTopText);
                TVTopText.setBackgroundResource(R.drawable.button_shape_orange);
                gd = (GradientDrawable) TVTopText.getBackground().getCurrent();
                gd.setColor(Color.parseColor("#666666"));

                switch (index) {
                    case 0: {
                        TVTopText.setText("Top 10");
                        break;
                    }
                    case 10: {
                        TVTopText.setText("Top 20");
                        break;
                    }
                    case 20: {
                        TVTopText.setText("Top 30");
                        break;
                    }
                    case 30: {
                        TVTopText.setText("Top 40");
                        break;
                    }
                    case 40: {
                        TVTopText.setText("Top 50");
                        break;
                    }
                    case 50: {
                        TVTopText.setText("Top 60");
                        break;
                    }
                    default: {
                        TVTopText.setVisibility(View.GONE);
                        gd.setColor(Color.parseColor("#f69421"));
                    }
                }
            } else {
                TextView TVTopText = (TextView) view.findViewById(R.id.TVTopText);
                TVNumber.setVisibility(View.GONE);
                if (index==0){
                    TVTopText.setVisibility(View.VISIBLE);
                    TVTopText.setText("Search result");
                    TVTopText.setBackgroundResource(R.drawable.button_shape_orange);
                    gd = (GradientDrawable) TVTopText.getBackground().getCurrent();
                    gd.setColor(Color.parseColor("#00000000"));

                }else {
                    TVTopText.setVisibility(View.GONE);

                }
            }
            TextView TVLastDateTime = view.findViewById(R.id.TVLastDateTime);
            TVLastDateTime.setVisibility(View.GONE);

        } else {
            view = inflater.inflate(R.layout.sub_distillery, parent, false);
            TextView TVDistillery_Name = (TextView) view.findViewById(R.id.TVDistillery_Name);
            TextView TVAddress = (TextView) view.findViewById(R.id.TVAddress);
            TextView TVZipcodeCity = (TextView) view.findViewById(R.id.TVZipcodeCity);
            TextView TVPhone = (TextView) view.findViewById(R.id.TVPhone);
            TextView TVDistlleryGPSN = (TextView) view.findViewById(R.id.TVDistlleryGPSN);
            TextView TVDistlleryGPSE = (TextView) view.findViewById(R.id.TVDistlleryGPSE);
            TextView TVNote = (TextView) view.findViewById(R.id.TVNote);

            TVDistillery_Name.setText(bottle.getDistillery().getDescription());
            TVAddress.setText(bottle.getDistillery().getAddress());
            TVZipcodeCity.setText(bottle.getDistillery().getZipCode() + " " + bottle.getDistillery().getCountry());
            TVPhone.setText("Phone : " + bottle.getDistillery().getPhone());
            TVDistlleryGPSN.setText(bottle.getDistillery().getGPS_N());
            TVDistlleryGPSE.setText(bottle.getDistillery().getGPS_E());
            TVNote.setText(bottle.getDistillery().getNote());
            //** Wikipedia **
            //***************
            ImageButton BWiki = (ImageButton) view.findViewById(R.id.BWiki);
            BWiki.setVisibility(View.GONE);
            //** HomePage **
            //**************
            ImageButton BHomePage = (ImageButton) view.findViewById(R.id.BHomePage);
            BHomePage.setVisibility(View.GONE);
            //** Google maps **
            //*****************
            ImageButton BNavi = (ImageButton) view.findViewById(R.id.BNavi);
            BNavi.setVisibility(View.GONE);

            ImageView IVEditDistillery = (ImageView) view.findViewById(R.id.IVEditDistillery);
            IVEditDistillery.setVisibility(View.GONE);


            TextView TVTopText = view.findViewById(R.id.TVTopText);
            if (index==0){
                TVTopText.setVisibility(View.VISIBLE);
                TVTopText.setText("Search result");
                TVTopText.setBackgroundResource(R.drawable.button_shape_orange);
                gd = (GradientDrawable) TVTopText.getBackground().getCurrent();
                gd.setColor(Color.parseColor("#00000000"));

            }else {
                TVTopText.setVisibility(View.GONE);

            }


        }


/*
        LinearLayout layout = view.findViewById(R.id.LLBottleInfo);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 50, 0, 0);
        layout.setLayoutParams(params);
*/
/*
        ImageView flag = (ImageView) view.findViewById(R.id.IVFlag);
        flag.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_language_20dp));
*/

        return view;
    }

}



