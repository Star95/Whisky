package dk.whisky_nyderne.whisky.Adapter;


import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Color_Rating;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.TImage;

import static dk.whisky_nyderne.whisky.Tools.Color_Rating.getColor;

public class Adapter_Timeline_list extends BaseAdapter {
    private int Number_Of_Bottles = 0;
    final ArrayList<TBottle_info> listArray;
    //"#f69421"orange


    public Adapter_Timeline_list(int NumberOfBottles) {
        listArray = new ArrayList<TBottle_info>();
        this.Number_Of_Bottles = NumberOfBottles;
    }


    public void addItem(TBottle_info bottle) {
        listArray.add(bottle);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TBottle_info bottle = listArray.get(index);
        GradientDrawable gd;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_bottles, parent, false);


        TextView TVNumber = (TextView) view.findViewById(R.id.TVNumber);
        TVNumber.setText(String.valueOf(Number_Of_Bottles - index));


        ImageView IVFlag = (ImageView) view.findViewById(R.id.IVFlag);


        if (bottle.getDistillery() != null) {

            //** Country **
            //*************
            TCountries contries = TCountries.getInstance(view.getContext());
            TCountry country = contries.getInfo(bottle.getBottle().getCOUNTRY_ID());


            IVFlag.setImageResource(country.getDrawableID());
            IVFlag.setMaxWidth(10);
            TImage image = TImage.getInstance();

            ImageView IVBottleImage = (ImageView) view.findViewById(R.id.IVBottleImage);
            IVBottleImage.setImageBitmap(image.getRoundedCornerBitmap(bottle.getBottle().getBOTTLE_IMAGE(), 16));

            //** Distillery **
            //****************

            TextView TVDistillery_Name = (TextView) view.findViewById(R.id.TVDistillery_Name);
            TVDistillery_Name.setText(bottle.getDistillery().getDescription());

            TextView TVPhone = (TextView) view.findViewById(R.id.TVPhone);
            TVPhone.setText("Phone : " + bottle.getDistillery().getPhone());
            //** Distillery address **
            //************************
            TextView TVAddress = (TextView) view.findViewById(R.id.TVAddress);
            TVAddress.setText(bottle.getDistillery().getAddress());

            //** Distillery Zip code and city name **
            //***************************************
            TextView TVZIPCODE = (TextView) view.findViewById(R.id.TVZIPCODE);
            TVZIPCODE.setText(bottle.getDistillery().getZipCode() + " " + bottle.getDistillery().getCountry());

        }

        TextView TVBottle_Label = (TextView) view.findViewById(R.id.TVBottle_Label);
        TVBottle_Label.setText(bottle.getBottle().getBOTTLE_LABEL());

        TextView TVTotal_Value = (TextView) view.findViewById(R.id.TVTotal_Value);
        TVTotal_Value.setText(String.valueOf(bottle.getRating().getAVG()));
        TVTotal_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVTotal_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColorTotal(bottle.getRating().getAVG())));

        TVTotal_Value.setPadding(15, 0, 15, 0);


        TextView TVAge = (TextView) view.findViewById(R.id.TVAge);
        TVAge.setText(String.valueOf(bottle.getBottle().getAGE_OF_WHISKY()) + " year");

        TextView TVPercentag = (TextView) view.findViewById(R.id.TVPercentag);
        TVPercentag.setText(String.valueOf(bottle.getBottle().getABV()) + " %");


        TextView TVType = (TextView) view.findViewById(R.id.TVType);
        switch (bottle.getBottle().getTYPE()) {
            case 0: {
                TVType.setText("Blended");
                break;
            }
            case 1: {
                TVType.setText("Single malt");
                break;
            }

        }

        TextView TVNose_Value = (TextView) view.findViewById(R.id.TVNose_Value);
        TVNose_Value.setText("N = " + String.valueOf(bottle.getRating().getNOSE()));
        TVNose_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVNose_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(bottle.getRating().getNOSE())));


        TextView TVTast_Value = (TextView) view.findViewById(R.id.TVTast_Value);
        TVTast_Value.setText("T = " + String.valueOf(bottle.getRating().getTAST()));
        TVTast_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVTast_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(bottle.getRating().getTAST())));


        TextView TVFinish_Value = (TextView) view.findViewById(R.id.TVFinish_Value);
        TVFinish_Value.setText("F = " + String.valueOf(bottle.getRating().getFINISH()));
        TVFinish_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVFinish_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(bottle.getRating().getFINISH())));


        TextView TVBalance_Value = (TextView) view.findViewById(R.id.TVBalance_Value);
        TVBalance_Value.setText("B = " + String.valueOf(bottle.getRating().getBALANCE()));
        TVBalance_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVBalance_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(bottle.getRating().getBALANCE())));


        TextView TVTopText = (TextView) view.findViewById(R.id.TVTopText);
        TVTopText.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVTopText.getBackground().getCurrent();
        gd.setColor(Color.parseColor("#666666"));

        switch (index) {
            case 0: {
                TVTopText.setText("Top 10");
                break;
            }
            case 10: {
                TVTopText.setText("Top 20");
                break;
            }
            case 20: {
                TVTopText.setText("Top 30");
                break;
            }
            case 30: {
                TVTopText.setText("Top 40");
                break;
            }
            case 40: {
                TVTopText.setText("Top 50");
                break;
            }
            case 50: {
                TVTopText.setText("Top 60");
                break;
            }
            case 60: {
                TVTopText.setText("Top 70");
                break;
            }
            case 70: {
                TVTopText.setText("Top 80");
                break;
            }
            case 80: {
                TVTopText.setText("Top 90");
                break;
            }
            case 90: {
                TVTopText.setText("Top 100");
                break;
            }
            default: {
                TVTopText.setVisibility(View.GONE);
                gd.setColor(Color.parseColor("#f69421"));
            }
        }

        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        int Year=0;
        String Month;
        switch (index) {
            case 0: {
                TVTopText.setVisibility(View.VISIBLE);
                Year = dateTimeFormat.YearFromDateToInteger(bottle.getRating().getDATETIME());
                Month = dateTimeFormat.MonthFromDateToText(bottle.getRating().getDATETIME());
                TVTopText.setText(String.valueOf(Year)+"\n"+Month);
                break;
            }
            default: {
                Year = dateTimeFormat.YearFromDateToInteger(listArray.get(index - 1).getRating().getDATETIME());
                if (Year == dateTimeFormat.YearFromDateToInteger(bottle.getRating().getDATETIME())) {
                    TVTopText.setVisibility(View.GONE);

                } else {
                    Year = dateTimeFormat.YearFromDateToInteger(bottle.getRating().getDATETIME());
                    TVTopText.setVisibility(View.VISIBLE);
                    Month = dateTimeFormat.MonthFromDateToText(bottle.getRating().getDATETIME());
                    TVTopText.setText(String.valueOf(Year)+"\n"+Month);
                }

            }

        }


        TextView TVLastDateTime = view.findViewById(R.id.TVLastDateTime);
        try {
            TVLastDateTime.setText(dateTimeFormat.DateTimeToString(bottle.getRating().getDATETIME()));
        }catch (Exception e){
            Log.e("",e.getMessage());
        }

/*
        ImageView flag = (ImageView) view.findViewById(R.id.IVFlag);
        flag.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_language_20dp));
*/

        return view;
    }

}


