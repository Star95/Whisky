package dk.whisky_nyderne.whisky.Adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import dk.whisky_nyderne.whisky.Data.TOwner;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

/**
 * Created by hans on 21-02-18.
 */

public class Adapter_Own_Bottles extends BaseAdapter {
    private final ArrayList<TOwner> listArray;

    public Adapter_Own_Bottles() {
        listArray = new ArrayList<TOwner>();
    }

    public void addItem(TOwner ownbottle) {
        listArray.add(ownbottle);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TOwner own = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_own_bottles, parent, false);

        TextView TVOpened = view.findViewById(R.id.TVOpened);
        TextView TVEmpty = view.findViewById(R.id.TVEmpty);
        TextView TVSize = view.findViewById(R.id.TVSize);
        TextView TVDays = view.findViewById(R.id.TVDays);
        TextView TVOwnPrice = view.findViewById(R.id.TVOwnPrice);
        TextView TVOwnNote = view.findViewById(R.id.TVOwnNote);

        LinearLayout LLEmpty = view.findViewById(R.id.LLEmpty);
        LinearLayout LLDays = view.findViewById(R.id.LLDays);


        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        /**
         * If Dates equals, bottle is not empty.
         */
        switch (own.getType()) {
            case 0: {// Not open
                LLEmpty.setVisibility(View.GONE);
                TVDays.setVisibility(View.GONE);
                LLDays.setVisibility(View.GONE);
                TVDays.setText(String.valueOf(0));
                TVOpened.setTextColor(Color.GREEN);
                TVSize.setTextColor(Color.GREEN);
                TVDays.setTextColor(Color.GREEN);
                TVEmpty.setTextColor(Color.GREEN);
                TVOpened.setText("Unopened");
                break;
            }
            case 1: { //Open
                LLEmpty.setVisibility(View.GONE);
                TVDays.setText(String.valueOf(dateTimeFormat.getDaysDifference(own.getOpen_DateTime(), new Date())));
                TVOpened.setTextColor(Color.WHITE);
                TVSize.setTextColor(Color.WHITE);
                TVDays.setTextColor(Color.WHITE);
                TVEmpty.setTextColor(Color.WHITE);
                TVOpened.setText(dateTimeFormat.DateToString(own.getOpen_DateTime()).toString() + " ");

                int month = 0;
                int week = 0;
                int year = 0;
                int days = 0;
                for (int i = 1; i < dateTimeFormat.getDaysDifference(own.getOpen_DateTime(), new Date()) + 1; i++) {
                    if ((i % 30) == 0)
                        month++;
                    if ((i % 7) == 0)
                        week++;
                    if ((i % 365) == 0)
                        ++year;
                    if ((i % 1) == 0)
                        days++;
                }
                TVOpened.setText(dateTimeFormat.DateToString(own.getOpen_DateTime()).toString());//+ " ("+String.valueOf(year)+" "+String.valueOf(month)+" "+String.valueOf(days));


                break;
            }
            case 2: {// Empth
                LLEmpty.setVisibility(View.VISIBLE);
                TVEmpty.setText(dateTimeFormat.DateToString(own.getEmpty_Datetime()).toString());
                TVDays.setText(String.valueOf(dateTimeFormat.getDaysDifference(own.getOpen_DateTime(), own.getEmpty_Datetime())));
                TVOpened.setTextColor(Color.RED);
                TVSize.setTextColor(Color.RED);
                TVDays.setTextColor(Color.RED);
                TVEmpty.setTextColor(Color.RED);
                TVOpened.setText(dateTimeFormat.DateToString(own.getOpen_DateTime()).toString());
                break;
            }

        }


        TVSize.setText(new DecimalFormat("#.##").format(own.getSize()) + " cl");
        TVOwnPrice.setText(new DecimalFormat("#.##").format(own.getPrice()));
        TVOwnNote.setText(own.getNote());

        return view;
    }

}

