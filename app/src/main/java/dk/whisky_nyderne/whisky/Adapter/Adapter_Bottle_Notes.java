package dk.whisky_nyderne.whisky.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.whisky.Data.TRating;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;

/**
 * Created by hans on 20-02-18.
 */

public class Adapter_Bottle_Notes extends BaseAdapter {
    final ArrayList<TRating> listArray;

    public Adapter_Bottle_Notes() {
        listArray = new ArrayList<TRating>();
    }

    public void addItem(TRating country) {
        listArray.add(country);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TRating rating = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_bottle_note, parent, false);


        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        TextView TVDateTime = (TextView) view.findViewById(R.id.TVDateTime);
        TVDateTime.setText(dateTimeFormat.DateToString(rating.getDATETIME()));

        TextView TVNote = (TextView) view.findViewById(R.id.TVNote);
        TVNote.setText(rating.getNOTE());


        return view;
    }

}
