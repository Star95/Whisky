package dk.whisky_nyderne.whisky.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TDistilleryTop;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.TImage;

public class Adapter_Distillery_Top_List  extends BaseAdapter {
    final ArrayList<TDistilleryTop> listArray;

    public Adapter_Distillery_Top_List() {
        listArray = new ArrayList<TDistilleryTop>();
    }

    public void addItem(TDistilleryTop distilleryTop) {
        listArray.add(distilleryTop);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TDistilleryTop distilleryTop = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_distillery_top, parent, false);

        ImageView IVTopDistillery =  view.findViewById(R.id.IVTopDistillery);
        TextView TVTopDistilledName =  view.findViewById(R.id.TVTopDistilledName);
        TextView TVTopTotal =  view.findViewById(R.id.TVTopTotal);
        TextView TVTopRankNumber =  view.findViewById(R.id.TVTopRankNumber);


        TVTopDistilledName.setText(distilleryTop.getDescription());
        TVTopTotal.setText(String.valueOf(distilleryTop.getResult())+"p");

        TVTopRankNumber.setText(String.valueOf(index + 1));

        TCountries contries = TCountries.getInstance(view.getContext());
        TCountry country = contries.getInfo(distilleryTop.getCountryID());


        IVTopDistillery.setImageResource(country.getDrawableID());
        IVTopDistillery.setMaxWidth(10);


        return view;
    }

}
