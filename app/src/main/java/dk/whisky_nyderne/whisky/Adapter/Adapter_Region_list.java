package dk.whisky_nyderne.whisky.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.countries.TRegion;
import dk.whisky_nyderne.whisky.R;

/**
 * Created by hans on 07-10-17.
 */

public class Adapter_Region_list extends BaseAdapter {
    final ArrayList<TRegion> listArray;

    public Adapter_Region_list() {
        listArray = new ArrayList<TRegion>();
    }

    public void addItem(TRegion region) {
        listArray.add(region);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }



    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TRegion region = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_country, parent, false);

        TextView Country_Name = (TextView) view.findViewById(R.id.TVCountry_Name);
        Country_Name.setText(region.getDescription());

        ImageView flag = (ImageView) view.findViewById(R.id.IVFlag);
        flag.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_language_20dp));


        return view;
    }

}

