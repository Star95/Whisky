package dk.whisky_nyderne.whisky.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.whisky.R;

/**
 * Created by hans on 02-10-17.
 */

public class Adapter_Country_list extends BaseAdapter {
    final ArrayList<TCountry> listArray;

    public Adapter_Country_list() {
        listArray = new ArrayList<TCountry>();
    }

    public void addItem(TCountry country) {
        listArray.add(country);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }



    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TCountry country = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_country, parent, false);

        TextView Country_Name = (TextView) view.findViewById(R.id.TVCountry_Name);
        Country_Name.setText(country.getDecription());

        ImageView flag = (ImageView) view.findViewById(R.id.IVFlag);
        flag.setImageBitmap(country.getFlag());


        return view;
    }

}
