package dk.whisky_nyderne.whisky.Adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TBottle_info_Alcohol;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.TImage;

public class Adapter_Last_24_Hours extends BaseAdapter {
    final ArrayList<TBottle_info_Alcohol> listArray;

    public Adapter_Last_24_Hours() {
        listArray = new ArrayList<TBottle_info_Alcohol>();
    }

    public void addItem(TBottle_info_Alcohol bottle) {
        listArray.add(bottle);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        final TBottle_info_Alcohol bottle = listArray.get(index);

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (bottle.getPromille() > 0) {
            view = inflater.inflate(R.layout.row_alcohol, parent, false);
            ProgressBar PBAlcohol = view.findViewById(R.id.PBAlcohol);
            PBAlcohol.setProgress(bottle.getRestPromille());
            TextView TVPromiller = view.findViewById(R.id.TVPromiller);
            TVPromiller.setText(new DecimalFormat("#.##").format(bottle.getPromille())+"‰");

            PBAlcohol.getProgressDrawable().setColorFilter( Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);

        } else
            view = inflater.inflate(R.layout.row_last_24_hours, parent, false);

        TextView TVLabel = (TextView) view.findViewById(R.id.TVLabel);
        TextView TVDateTime = (TextView) view.findViewById(R.id.TVDateTime);
        TextView TVRankNumber = (TextView) view.findViewById(R.id.TVRankNumber);
        TextView TVTotal = (TextView) view.findViewById(R.id.TVTotal);


        TVLabel.setText(bottle.getBottle().getBOTTLE_LABEL());

        TVDateTime.setText(dateTimeFormat.TimeToString(bottle.getRating().getDATETIME()));
        TVRankNumber.setText(String.valueOf(index + 1));


        TImage image = TImage.getInstance();
        ImageView IVBottle = (ImageView) view.findViewById(R.id.IVBottle);
        IVBottle.setImageBitmap(image.getRoundedCornerBitmap(bottle.getBottle().getBOTTLE_IMAGE(), 16));

        TVTotal.setText(String.valueOf(bottle.getRating().getAVG()) + "p");

        return view;
    }

}