package dk.whisky_nyderne.whisky.Adapter;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TRating;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Color_Rating;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.TImage;

import static dk.whisky_nyderne.whisky.Tools.Color_Rating.getColor;

public class Adapter_Ratings extends BaseAdapter {
    final ArrayList<TRating> listArray;

    public Adapter_Ratings() {
        listArray = new ArrayList<TRating>();
    }

    public void addItem(TRating rating) {
        listArray.add(rating);
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }


    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        final TRating rating = listArray.get(index);
        GradientDrawable gd;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.row_ratings, parent, false);

        TextView TVNose_Value = (TextView) view.findViewById(R.id.TVNose_Value);
        TextView TVTast_Value = (TextView) view.findViewById(R.id.TVTast_Value);
        TextView TVFinish_Value = (TextView) view.findViewById(R.id.TVFinish_Value);
        TextView TVBalance_Value = (TextView) view.findViewById(R.id.TVBalance_Value);
        TextView TVTotal_Value = (TextView) view.findViewById(R.id.TVTotal_Value);
        TextView TVDateTime = (TextView) view.findViewById(R.id.TVDateTime);
        TextView TVNote = view.findViewById(R.id.TVNote);

        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();


        TVNose_Value.setText("N=" + String.valueOf(rating.getNOSE()));
        TVTast_Value.setText("T=" + String.valueOf(rating.getTAST()));
        TVFinish_Value.setText("F=" + String.valueOf(rating.getFINISH()));
        TVBalance_Value.setText("B=" + String.valueOf(rating.getBALANCE()));
        TVTotal_Value.setText("Total=" + String.valueOf(rating.getNOSE() + rating.getTAST() + rating.getFINISH() + rating.getBALANCE()));
        TVDateTime.setText(dateTimeFormat.DateTimeToString(rating.getDATETIME()));
        TVNote.setText(rating.getNOTE());


        TVNose_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVNose_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(rating.getNOSE())));

        TVTast_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVTast_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(rating.getTAST())));

        TVFinish_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVFinish_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(rating.getFINISH())));

        TVBalance_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVBalance_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(getColor(rating.getBALANCE())));

        TVTotal_Value.setBackgroundResource(R.drawable.button_shape_orange);
        gd = (GradientDrawable) TVTotal_Value.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColorTotal(rating.getNOSE() + rating.getTAST() + rating.getFINISH() + rating.getBALANCE())));
        return view;
    }

}