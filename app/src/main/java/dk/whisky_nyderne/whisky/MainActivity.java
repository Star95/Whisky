package dk.whisky_nyderne.whisky;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Bottles;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Bottles_Information;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Create_Bottle;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Create_Distillery;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Draft_Bottles;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Menu;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Own_Bottles;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Rate_Bottle;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Search_Bottles;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Search_Bottles_Result;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Setup;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Time_Line;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Top_Distillery;
import dk.whisky_nyderne.whisky.Fragment.Fragment_Top_List;
import dk.whisky_nyderne.whisky.Tools.ExportImportDB;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final int SELECT_PHOTO_REQUEST_CODE = 12346;
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 12345;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, new Fragment_Menu())
                .commit();

        DBDistillery DBDistillery = new DBDistillery(this);
        DBDistillery.open();

        setTitle("Whisky");
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        NavigationBarClick(this);

        BottomNavigationView bottomNavigationView;
        bottomNavigationView =  findViewById(R.id.navigation);
        bottomNavigationView.setSelectedItemId(R.id.action_home);


        final Context context = this.getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                //TCountries countries =
                TCountries.getInstance(context);
            }
        }).start();


    }

    private void NavigationBarClick(final Context context) {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                gotoMainMenu();
                                break;
                            case R.id.action_toplist:
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        FragmentManager fragmentManager = getFragmentManager();
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.container, new Fragment_Top_List())
                                                .commit();
                                    }
                                }).start();

                                break;
                            case R.id.action_timeline:
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        FragmentManager fragmentManager = getFragmentManager();
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.container, new Fragment_Time_Line())
                                                .commit();
                                    }
                                }).start();
                                break;
                            case R.id.action_Search:
                                gotoSearch();
                                break;
                            case R.id.action_Bottles:
                                new Thread(new Runnable() {

                                    @Override
                                    public void run() {
                                        FragmentManager fragmentManager = getFragmentManager();
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.container, new Fragment_Bottles())
                                                .commit();
                                    }
                                }).start();
                                break;
                        }
                        // Show appbar / Toolbar
                        AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.appbar);
                        appBarLayout.setExpanded(true, true);

                        return true;
                    }
                });

    }

    private void gotoMainMenu() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Menu menu = new Fragment_Menu();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, menu)
                        .commit();
            }
        }).start();

    }

    public void gotoSearch() {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment_Search_Bottles search = new Fragment_Search_Bottles();
        search.number = 0;
        fragmentManager.beginTransaction()
                .replace(R.id.container, search)
                .commit();
    }

    private void showAlertDialog(String Description, final int function) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(false);
        builder.setMessage(Description);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                switch (function) {
                    case 1: {// Exit application
                        finish();
                        break;
                    }
                    case 2: { //Go to main menu
                        gotoMainMenu();
                    }

                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment currentFragment = this.getFragmentManager().findFragmentById(R.id.container);

            //** Bottle information **
            //************************
            if (currentFragment instanceof Fragment_Bottles_Information) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Menu) {
                showAlertDialog("Exit application ?", 1);
            } else if (currentFragment instanceof Fragment_Rate_Bottle) {
                showAlertDialog("Do you wish to discontinue your assessment ?", 2);
            } else if (currentFragment instanceof Fragment_Bottles) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Create_Bottle) {
                showAlertDialog("Do you want to cancel ?", 2);
            } else if (currentFragment instanceof Fragment_Create_Distillery) {
                showAlertDialog("Do you want to cancel ?", 2);
            } else if (currentFragment instanceof Fragment_Top_List) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Time_Line) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Draft_Bottles) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Search_Bottles) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Search_Bottles_Result) {
                gotoSearch();
            } else if (currentFragment instanceof Fragment_Top_Distillery) {
                gotoMainMenu();
            } else if (currentFragment instanceof Fragment_Own_Bottles) {
                gotoMainMenu();
            }

            // Show appbar / Toolbar
            AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.appbar);
            appBarLayout.setExpanded(true, true);

            // super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(true);
        }


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Setup setup = new Fragment_Setup();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, setup)
                    .commit();
            return true;
        }
        if (id == R.id.action_new_Search) {
            gotoSearch();
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Find_Bottle) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, new Fragment_Bottles())
                            .commit();
                }
            }).start();

        } else if (id == R.id.nav_Search) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, new Fragment_Search_Bottles())
                            .commit();
                }
            }).start();


        } else if (id == R.id.nav_New_Distillery) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new Fragment_Create_Distillery())
                    .commit();

        } else if (id == R.id.nav_New_bottle) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new Fragment_Create_Bottle())
                    .commit();

        } else if (id == R.id.nav_restore) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        2);
            } else {
                File direct = new File(Environment.getExternalStorageDirectory() + "/Whisky");

                if (!direct.exists()) {
                    if (direct.mkdir()) ;
                }

                ExportImportDB EIDB = new ExportImportDB();

                //EIDB.exportDB("bottles16");
                EIDB.importDB("bottles_new01.db");

            }
        } else if (id == R.id.nav_backup) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        2);
            } else {
                File direct = new File(Environment.getExternalStorageDirectory() + "/Whisky");

                if (!direct.exists()) {
                    if (direct.mkdir()) ;
                }

                ExportImportDB EIDB = new ExportImportDB();


                EIDB.exportDB("bottles_01");
            }
        } else if (id == R.id.nav_New_Draft_bottle) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new Fragment_Draft_Bottles())
                    .commit();
        } else if (id == R.id.nav_Own_bottles) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new Fragment_Own_Bottles())
                    .commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);



        return true;
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                File direct = new File(Environment.getExternalStorageDirectory() + "/Whisky");

                if (!direct.exists()) {
                    if (direct.mkdir()) ;
                }

                ExportImportDB EIDB = new ExportImportDB();

                EIDB.exportDB("bottles.db");


                // Permission granted.
            } else {
                // User refused to grant permission.
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if ((resultCode == Activity.RESULT_OK) && (null != data)) {
//                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                Fragment_Create_Bottle fragment = (Fragment_Create_Bottle) getFragmentManager().findFragmentById(R.id.container);
                fragment.CallBackPhoto(data.getData());

            }
        }
        if (requestCode == SELECT_PHOTO_REQUEST_CODE) {
            if ((resultCode == Activity.RESULT_OK) && (null != data)) {
                Uri selectedImage = data.getData();
/*
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                ImageLoader imageLoader = ImageLoader.getInstance();
*/
                Fragment_Create_Bottle fragment = (Fragment_Create_Bottle) getFragmentManager().findFragmentById(R.id.container);
                fragment.CallBackPhoto(selectedImage);

            }
        }
    }


}
