package dk.whisky_nyderne.whisky.Tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by hans on 24-10-17.
 */

public class TImage {
    private static final TImage ourInstance = new TImage();

    public static TImage getInstance() {
        return ourInstance;
    }

    private TImage() {
    }


    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public Bitmap ImageByte2Bitmap(byte[] imageByte, int w, int h) {
        Bitmap _image = null;
        if (imageByte != null) {
            _image = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            imageByte = null;

            //int h = this._image.getHeight() / 4; // height in pixels
            //int w = this._image.getWidth() / 4; // width in pixels
            //Bitmap scaled = Bitmap.createScaledBitmap(this._image, h, w, true);
            _image = Bitmap.createScaledBitmap(_image, w, h, true);

        }
        return _image;
    }

    public byte[] Bitmap2ImageByte(Bitmap _image) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bm = _image;
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte imageInByte[] = stream.toByteArray();
        return imageInByte;

    }

    public static Bitmap ImageByte2Bitmap(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static String Image2Base64(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

}
