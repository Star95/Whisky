package dk.whisky_nyderne.whisky.Tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.akshaykale.swipetimeline.ImageLoadingEngine;

public class ImageLoad implements ImageLoadingEngine {

    @Override
    public void onLoadImage(ImageView imageView, String uri) {
        Log.e("","");
        //imageView.setImageResource(R.drawable.ic_android_black_24dp);

        imageView.setImageBitmap(base64ToBitmap(uri));

        // Use any library you prefer to load the image into the view
        // For Ex: Glide, Picasso, UIL, etc.
    }
    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
}