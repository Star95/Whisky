package dk.whisky_nyderne.whisky.Tools;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import static dk.whisky_nyderne.whisky.DataBase.DataBase.DATABASE_NAME;

/**
 * Created by hans on 11-11-17.
 */

public class ExportImportDB extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    //importing database
    public void importDB(String filename) {


        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "dk.whisky_nyderne.whisky"
                        + "//databases//" + "Whisky16.db";
                String backupDBPath = "/Whisky/"+filename;//+".db";
                File backupDB = new File(data, currentDBPath);
                File currentDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

            }
        } catch (Exception e) {
            Log.e("Error", e.toString());

        }
    }

    public void showBackup(){

        String path = Environment.getExternalStorageDirectory().toString()+"/Whisky";
        Log.d("Files", "Path: " + path);
        File f = new File(path);
        File file[] = f.listFiles();
        Log.d("Files", "Size: "+ file.length);
        for (int i=0; i < file.length; i++)
        {
            Log.d("Files", "FileName:" + file[i].getName());
        }

    }


    //exporting database
    public void exportDB(String FileName) {

        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "dk.whisky_nyderne.whisky"
                        + "//databases//" + DATABASE_NAME;
                String backupDBPath = "/Whisky/"+FileName+".db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

            }
        } catch (Exception e) {
            Log.e("Error", e.toString());

        }
    }

}
