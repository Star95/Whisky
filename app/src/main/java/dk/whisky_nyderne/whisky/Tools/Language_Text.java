package dk.whisky_nyderne.whisky.Tools;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.Locale;
import java.util.Objects;

/**
 * Created by hans on 20-02-18.
 * To store multi language text in one field.
 */

public class Language_Text {
    private final String Language = "";

    private static Language_Text _instance;

    public synchronized static Language_Text getInstance(Context _context) {
        if (_instance == null) {
            _instance = new Language_Text();
            String Language = Locale.getDefault().getLanguage();
        }
        return _instance;
    }

    public Language_Text() {

    }

    private String getDescription(String Object) {
        String Language_Description = "";
        try {
            JSONObject obj = new JSONObject(Object);
            try {
                Language_Description = obj.getString(Language);
            } catch (Throwable t) {
                Language_Description = "";
            }

        } catch (Throwable t) {
            Log.e("Whisky", "Could not parse malformed JSON: ");
        }

        return Language_Description;
    }

    private String setDescription2Object(String Object, String Description) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(Object);
            obj.put(Language, Description);


        } catch (Throwable t) {
            Log.e("Whisky", "Could not parse malformed JSON: ");
        }
        return Objects.requireNonNull(obj).toString();
    }
}
