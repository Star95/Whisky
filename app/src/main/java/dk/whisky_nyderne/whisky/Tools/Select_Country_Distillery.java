package dk.whisky_nyderne.whisky.Tools;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.countries.TRegion;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Country_list;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Region_list;
import dk.whisky_nyderne.whisky.Data.TDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.R;

/**
 * Created by hans on 11-10-17.
 */

public class Select_Country_Distillery {

    private final List<Selected_Country_Distillery_Listener> listeners = new ArrayList<Selected_Country_Distillery_Listener>();
    private TCountry Selected_Country = null;
    private TRegion Selected_Region;
    private View rootview;
    private boolean _usedistillery = true;


    public void addListener(Selected_Country_Distillery_Listener listener, View view) {
        listeners.add(listener);
        rootview = view;
    }

    public void useDistillery(boolean usedistillery) {
        this._usedistillery = usedistillery;
    }

    void NotifySelectedCountry(TCountry country, TRegion region, TDistillery distillery) {

        for (Selected_Country_Distillery_Listener listener : listeners) {
            listener.onSelected(country, region, distillery);
        }
    }


    public void Show_Dialog_with_Contrys(String filter, List<Integer> filterlist, final String distilleryFilter) {
        final Dialog dialog = new Dialog(rootview.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(0));
        dialog.setContentView(R.layout.dialog_country_list);
        TextView TVHeader = (TextView) dialog.findViewById(R.id.TVHeader);
        TVHeader.setText("Contrys");


        ListView CountryList = (ListView) dialog.findViewById(R.id.LVCountryList);

        TCountries countries = TCountries.getInstance(rootview.getContext());
        if (!filterlist.isEmpty())
            countries.setFilter(filterlist);
        else
            countries.setFilter(filter);

        final Adapter_Country_list customAdapter = new Adapter_Country_list();
        ArrayList<TCountry> list;
        list = countries.getList();

        for (int i = 0; i < list.size(); i++) {
            customAdapter.addItem(list.get(i));
        }


        CountryList.setAdapter(customAdapter);


        CountryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                dialog.dismiss();

                Selected_Country = (TCountry) customAdapter.getItem(position);
                if (Selected_Country.regionlist.size() != 0) {
                    Show_Dialog_with_Regions(distilleryFilter);
                } else {
                    if (_usedistillery) {
                        Show_Dialog_with_Distillery(Selected_Country.getId(), 0, distilleryFilter);
                    } else {
                        NotifySelectedCountry(Selected_Country, null, null);
                    }
                }


            }
        });

        dialog.show();


    }

    private void Show_Dialog_with_Regions(final String distilleryFilter) {
        final Dialog dialog = new Dialog(rootview.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(0));
        dialog.setContentView(R.layout.dialog_country_list);
        TextView TVHeader = (TextView) dialog.findViewById(R.id.TVHeader);
        TVHeader.setText("Regions");

        ListView RegionList = (ListView) dialog.findViewById(R.id.LVCountryList);


        final Adapter_Region_list customAdapter = new Adapter_Region_list();
        final ArrayList<TRegion> list;
        list = Selected_Country.regionlist;

        for (int i = 0; i < list.size(); i++) {
            customAdapter.addItem(list.get(i));
        }


        RegionList.setAdapter(customAdapter);


        RegionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Selected_Region = list.get(position);
                if (_usedistillery) {
                    Show_Dialog_with_Distillery(Selected_Country.getId(), Selected_Region.getId(), distilleryFilter);
                } else {
                    NotifySelectedCountry(Selected_Country, Selected_Region, null);

                }

                dialog.dismiss();

            }
        });

        dialog.show();


    }


    void Show_Dialog_with_Distillery(final int Country_ID, final int Region_ID, String filter) {

        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());


        final List<TDistillery> list = dbDistillery.getDistillery(Country_ID, Region_ID, filter);

        if (list.size() > 0) {

            List<String> target = new ArrayList<String>();
            for (int t = 0; t < list.size(); t++) {
                target.add(list.get(t).getDescription());
            }


            final Dialog dialog = new Dialog(rootview.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //  dialog.setCancelable(false);
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(0));
            dialog.setContentView(R.layout.dialog_country_list);
            TextView TVHeader = (TextView) dialog.findViewById(R.id.TVHeader);
            TVHeader.setText("Distillery");


            ListView LVDistilleryList = (ListView) dialog.findViewById(R.id.LVCountryList);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(rootview.getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, target);


            LVDistilleryList.setAdapter(adapter);


            LVDistilleryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    NotifySelectedCountry(Selected_Country, Selected_Region, list.get(position));
                    dialog.dismiss();
                }
            });

            dialog.show();
        } else {
            try {
                NotifySelectedCountry(Selected_Country, Selected_Region, null);
            }catch (Exception e){
                Log.e("",e.getMessage());
            }

        }


    }


}
