package dk.whisky_nyderne.whisky.Tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by hans on 14-10-17.
 */

public class DateTimeFormat {
    private static final DateTimeFormat ourInstance = new DateTimeFormat();

    public static DateTimeFormat getInstance() {
        return ourInstance;
    }

    private DateTimeFormat() {
    }


    public String YearMdrDayToString(int year, int month, int day) {

        return DateTimeToString(YearMdrDayToDate(year, month, day));
    }

    public Date YearMdrDayToDate(int year, int month, int day) {

        return new GregorianCalendar(year, month, day).getTime();
    }

    public String DateTimeToString(Date date) {
        Calendar d = Calendar.getInstance();
        d.add(Calendar.DAY_OF_MONTH, -1);
        return (String) android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", date);

    }

    public String DateToString(Date date) {
        Calendar d = Calendar.getInstance();
        d.add(Calendar.DAY_OF_MONTH, -1);
        return (String) android.text.format.DateFormat.format("yyyy-MM-dd", date);

    }


    public Date StringToDate(String string_date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(string_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static int getDaysDifference(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null)
            return 0;

        return (int) ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
    }

    public String YearFromDateToString(Date date) {
        return (String) android.text.format.DateFormat.format("yyyy", date);

    }

    public int YearFromDateToInteger(Date date) {
        return Integer.valueOf((String) android.text.format.DateFormat.format("yyyy", date));

    }

    public String MonthFromDateToString(Date date) {
        return (String) android.text.format.DateFormat.format("MM", date);

    }

    public int MonthFromDateToInteger(Date date) {
        return Integer.valueOf((String) android.text.format.DateFormat.format("MM", date));

    }

    public String MonthFromDateToText(Date date) {
        return (String) android.text.format.DateFormat.format("MMMM", date);

    }

    public String DayFromDateToString(Date date) {
        return (String) android.text.format.DateFormat.format("dd", date);

    }


    public String TimeToString(Date date) {

        SimpleDateFormat simpDate;
        simpDate = new SimpleDateFormat("kk:mm:ss");
        return simpDate.format(date);

    }

    public static int getPreviousYear() {
        Calendar prevYear = Calendar.getInstance();
        prevYear.add(Calendar.YEAR, -1);
        return prevYear.get(Calendar.YEAR);
    }


}
