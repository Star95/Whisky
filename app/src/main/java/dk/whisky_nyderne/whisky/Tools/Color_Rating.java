package dk.whisky_nyderne.whisky.Tools;

public final class Color_Rating {
    public static final String ColorTerrible = "#990000";//"#33CC33";
    public static final String ColorBad = "#CC6600";
    public static final String ColorOkay = "#B1BCBE";
    public static final String ColorOkayTotal = "#666666";
    public static final String ColorGood = "#339900";//"#0066CC";
    public static final String ColorGreat = "#336600";//"#006600";


    public final static String getColor(int rating) {
        String result = ColorTerrible;
        if (rating <= 25)
            result = ColorGreat;
        if (rating <= 20)
            result = ColorGood;
        if (rating <= 15)
            result = ColorOkay;
        if (rating <= 10)
            result = ColorBad;
        if (rating <= 5)
            result = ColorTerrible;

        return result;
    }

    public final static String getColorTotal(int rating) {
        String result = ColorTerrible;
        if (rating <= 100)
            result = ColorGreat;
        if (rating <= 80)
            result = ColorGood;
        if (rating <= 60)
            result = ColorOkayTotal;
        if (rating <= 40)
            result = ColorBad;
        if (rating <= 20)
            result = ColorTerrible;

        return result;
    }

}
