package dk.whisky_nyderne.whisky.Tools;

import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.countries.TRegion;
import dk.whisky_nyderne.whisky.Data.TDistillery;

/**
 * Created by hans on 11-10-17.
 *
 */

public interface Selected_Country_Distillery_Listener {
    void onSelected(TCountry country, TRegion region, TDistillery distillery);
}
