package dk.whisky_nyderne.whisky.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.countries.TRegion;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Select_Country_Distillery;
import dk.whisky_nyderne.whisky.Tools.Selected_Country_Distillery_Listener;

/**
 * Created by hans on 02-10-17.
 */

public class Fragment_Create_Distillery extends Fragment implements Selected_Country_Distillery_Listener {
    //** Public **
    public boolean Edit = false;
    public static TBottle_info bottle_info = new TBottle_info();

    //** Private **
    private Select_Country_Distillery select_country;
    private int Selected_Country_ID = 0;
    private int Selected_Region_ID = 0;
    View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_create_distillery, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents();

        LinearLayout show_country = (LinearLayout) rootview.findViewById(R.id.View_Country);
        LinearLayout View_Distillery_Edit = (LinearLayout) rootview.findViewById(R.id.View_Distillery_Edit);
        LinearLayout View_Edit_Delete_Save = (LinearLayout) rootview.findViewById(R.id.View_Edit_Delete_Save);

        if (Edit) {
            show_country.setVisibility(View.GONE);
            View_Distillery_Edit.setVisibility(View.VISIBLE);
            View_Edit_Delete_Save.setVisibility(View.VISIBLE);

            LinearLayout View_Search = (LinearLayout) rootview.findViewById(R.id.View_Search);
            View_Search.setVisibility(View.GONE);

            LinearLayout View_Country_Edit = (LinearLayout) rootview.findViewById(R.id.View_Country_Edit);
            View_Country_Edit.setVisibility(View.GONE);

            setText_on_Edit();
            Selected_Country_ID = bottle_info.getDistillery().getCOUNTRY_ID();
            Selected_Region_ID = bottle_info.getDistillery().getREGION_ID();

        } else {
            show_country.setVisibility(View.GONE);
            View_Distillery_Edit.setVisibility(View.GONE);
            View_Edit_Delete_Save.setVisibility(View.GONE);

            TextView TVCountry_Name_Region = (TextView) rootview.findViewById(R.id.TVCountry_Name_Region);
            TVCountry_Name_Region.setText("");
        }
        getActivity().invalidateOptionsMenu();
    }

    private void setText_on_Edit() {
        EditText ETDistillery = (EditText) rootview.findViewById(R.id.ETDistillery);
        EditText ETDistillery_Address = (EditText) rootview.findViewById(R.id.ETDistillery_Address);
        EditText ETDistillery_ZipCode = (EditText) rootview.findViewById(R.id.ETDistillery_ZipCode);
        EditText ETDistillery_City = (EditText) rootview.findViewById(R.id.ETDistillery_City);
        EditText ETDistillery_Phone = (EditText) rootview.findViewById(R.id.ETDistillery_Phone);
        EditText ETDistillery_GPSN = (EditText) rootview.findViewById(R.id.ETDistillery_GPSN);
        EditText ETDistillery_GPSE = (EditText) rootview.findViewById(R.id.ETDistillery_GPSE);
        EditText ETDistillery_HomePage = (EditText) rootview.findViewById(R.id.ETDistillery_HomePage);
        EditText ETDistillery_Wikipedia = (EditText) rootview.findViewById(R.id.ETDistillery_Wikipedia);
        EditText ETDistillery_Note = (EditText) rootview.findViewById(R.id.ETDistillery_Note);

        ETDistillery.setText(bottle_info.getDistillery().getDescription());
        ETDistillery_Address.setText(bottle_info.getDistillery().getAddress());
        ETDistillery_ZipCode.setText(bottle_info.getDistillery().getZipCode());
        ETDistillery_City.setText(bottle_info.getDistillery().getCountry());
        ETDistillery_Phone.setText(bottle_info.getDistillery().getPhone());
        ETDistillery_GPSN.setText(bottle_info.getDistillery().getGPS_N());
        ETDistillery_GPSE.setText(bottle_info.getDistillery().getGPS_E());
        ETDistillery_HomePage.setText(bottle_info.getDistillery().getHomePage());
        ETDistillery_Wikipedia.setText(bottle_info.getDistillery().getWikipedia());
        ETDistillery_Note.setText(bottle_info.getDistillery().getNote());

    }

    public void onSelected(TCountry country, TRegion region, TDistillery distillery) {
        if (country != null) {
            Selected_Country_ID = country.getId();

            Country_selected();
            TextView countryname = (TextView) rootview.findViewById(R.id.TVCountry_Name);
            countryname.setText(country.getDecription());

            ImageView flag = (ImageView) rootview.findViewById(R.id.IVFlag);
            flag.setImageBitmap(country.getFlag());

            if (region != null) {
                TextView TVCountry_Name_Region = (TextView) rootview.findViewById(R.id.TVCountry_Name_Region);
                TVCountry_Name_Region.setText(region.getDescription());
                Selected_Region_ID = region.getId();
            } else {
                TextView TVCountry_Name_Region = (TextView) rootview.findViewById(R.id.TVCountry_Name_Region);
                TVCountry_Name_Region.setText("");
                Selected_Region_ID = 0;

            }
        }

    }


    private void Country_selected() {
        LinearLayout View_Distillery_Edit = (LinearLayout) rootview.findViewById(R.id.View_Distillery_Edit);
        View_Distillery_Edit.setVisibility(View.VISIBLE);
        LinearLayout View_Country = (LinearLayout) rootview.findViewById(R.id.View_Country);
        View_Country.setVisibility(View.VISIBLE);
        LinearLayout View_Country_Edit = (LinearLayout) rootview.findViewById(R.id.View_Country_Edit);
        View_Country_Edit.setVisibility(View.GONE);


        LinearLayout View_Edit_Delete_Save = (LinearLayout) rootview.findViewById(R.id.View_Edit_Delete_Save);
        View_Edit_Delete_Save.setVisibility(View.VISIBLE);

        LinearLayout View_Search = (LinearLayout) rootview.findViewById(R.id.View_Search);
        View_Search.setVisibility(View.GONE);


    }


    private void addEvents() {

        LinearLayout BSave = (LinearLayout) rootview.findViewById(R.id.B_Save);
        BSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                save_new_distillery();


            }
        });

        /**
         * search for a country
         */
        LinearLayout Search = (LinearLayout) rootview.findViewById(R.id.LL_Search);
        Search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText filter_countrys = (EditText) rootview.findViewById(R.id.ETFilterCountry);

                select_country = new Select_Country_Distillery();
                select_country.addListener(Fragment_Create_Distillery.this, rootview);
                select_country.useDistillery(false);
                select_country.Show_Dialog_with_Contrys(filter_countrys.getText().toString(), new ArrayList<Integer>(), "");

                LinearLayout Menu_Distillery = (LinearLayout) rootview.findViewById(R.id.Menu_Distillery);
                Menu_Distillery.setVisibility(View.VISIBLE);

            }
        });


        LinearLayout BDelete = (LinearLayout) rootview.findViewById(R.id.B_Delete);
        BDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView ETDistillery = (TextView) rootview.findViewById(R.id.ETDistillery);
//                DBDistillery dbBrands = new DBDistillery(rootview.getContext());
//                dbBrands.DeleteBrand(Selected_Country.getId(), Selected_RegionID, ETBrand.getText().toString());
                //TODO : Missing...
            }
        });


    }

    private void save_new_distillery() {

        EditText ETDistillery = (EditText) rootview.findViewById(R.id.ETDistillery);
        if (!ETDistillery.getText().toString().isEmpty()) {
            TDistillery distillery = new TDistillery();
            EditText ETDistillery_Address = (EditText) rootview.findViewById(R.id.ETDistillery_Address);
            EditText ETDistillery_ZipCode = (EditText) rootview.findViewById(R.id.ETDistillery_ZipCode);
            EditText ETDistillery_City = (EditText) rootview.findViewById(R.id.ETDistillery_City);
            EditText ETDistillery_Phone = (EditText) rootview.findViewById(R.id.ETDistillery_Phone);
            EditText ETDistillery_GPSN = (EditText) rootview.findViewById(R.id.ETDistillery_GPSN);
            EditText ETDistillery_GPSE = (EditText) rootview.findViewById(R.id.ETDistillery_GPSE);
            EditText ETDistillery_HomePage = (EditText) rootview.findViewById(R.id.ETDistillery_HomePage);
            EditText ETDistillery_Wikipedia = (EditText) rootview.findViewById(R.id.ETDistillery_Wikipedia);
            EditText ETDistillery_Note = (EditText) rootview.findViewById(R.id.ETDistillery_Note);

            distillery.setAddress(ETDistillery_Address.getText().toString());
            distillery.setZipCode(ETDistillery_ZipCode.getText().toString());
            distillery.setCountry(ETDistillery_City.getText().toString());
            distillery.setPhone(ETDistillery_Phone.getText().toString());
            distillery.setGPS_N(ETDistillery_GPSN.getText().toString());
            distillery.setGPS_E(ETDistillery_GPSE.getText().toString());
            distillery.setHomePage(ETDistillery_HomePage.getText().toString());
            distillery.setWikipedia(ETDistillery_Wikipedia.getText().toString());
            distillery.setNote(ETDistillery_Note.getText().toString());

            distillery.setDescription(ETDistillery.getText().toString());
            distillery.setCOUNTRY_ID(Selected_Country_ID);
            distillery.setREGION_ID(Selected_Region_ID);

            if (Edit) {
                distillery.setGUID(bottle_info.getDistillery().getGUID());

            }

            DBDistillery DBDistillery = new DBDistillery(rootview.getContext());
            DBDistillery.addDistillery(distillery);


            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new Fragment_Menu())
                    .commit();
            Toast.makeText(rootview.getContext(), "New Distillery is saved", Toast.LENGTH_SHORT);
        } else
            Toast.makeText(rootview.getContext(), "Your Distillery name is empty", Toast.LENGTH_SHORT);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_new_Search) {

            LinearLayout View_Distillery_Edit = (LinearLayout) rootview.findViewById(R.id.View_Distillery_Edit);
            View_Distillery_Edit.setVisibility(View.GONE);

            LinearLayout View_Edit_Delete_Save = (LinearLayout) rootview.findViewById(R.id.View_Edit_Delete_Save);
            View_Edit_Delete_Save.setVisibility(View.GONE);


            LinearLayout show_country = (LinearLayout) rootview.findViewById(R.id.View_Country);
            show_country.setVisibility(View.GONE);

            LinearLayout View_Country_Edit = (LinearLayout) rootview.findViewById(R.id.View_Country_Edit);
            View_Country_Edit.setVisibility(View.VISIBLE);

            LinearLayout View_Search = (LinearLayout) rootview.findViewById(R.id.View_Search);
            View_Search.setVisibility(View.VISIBLE);


            return true;
        }
        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }
}
