package dk.whisky_nyderne.whisky.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import dk.whisky_nyderne.whisky.R;

public class Fragment_Setup_MainPage extends Fragment {


    View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_setup_mainpage, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents();
        getActivity().invalidateOptionsMenu();

    }

    private void addEvents() {

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }
}
