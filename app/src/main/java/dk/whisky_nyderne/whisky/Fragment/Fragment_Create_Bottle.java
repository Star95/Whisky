package dk.whisky_nyderne.whisky.Fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.countries.TRegion;
import dk.whisky_nyderne.whisky.Data.TBottle;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.Dialog_Fragment.DF_Import_Image;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.Select_Country_Distillery;
import dk.whisky_nyderne.whisky.Tools.Selected_Country_Distillery_Listener;

import static dk.whisky_nyderne.whisky.Data.TSetup.DRAFT_NAME;
import static dk.whisky_nyderne.whisky.Data.TSetup.Thumbnail_Height;
import static dk.whisky_nyderne.whisky.Data.TSetup.Thumbnail_Width;

/**
 * Created by hans on 08-10-17.
 */

public class Fragment_Create_Bottle extends Fragment implements Selected_Country_Distillery_Listener {
    //** Public **
    public static TBottle_info bottle_info = new TBottle_info();
    public Boolean Edit = false;
    public TCountry _country;
    public TRegion _region;

    //** Private **
    private Select_Country_Distillery select_country;
    private Bitmap Selected_Thumbnail = null;
    private String Selected_URI = "";
    private Date BottledDate = null;

    View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_create_bottle, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents(view);
        LinearLayout show_country = rootview.findViewById(R.id.View_Country);
        LinearLayout show_distillery = rootview.findViewById(R.id.View_Distillery);
        LinearLayout View_bottle_main = rootview.findViewById(R.id.View_bottle_main);
        LinearLayout View_Save = rootview.findViewById(R.id.View_Save);
        LinearLayout View_Country = rootview.findViewById(R.id.View_Country);
        LinearLayout View_Distillery = rootview.findViewById(R.id.View_Distillery);
        LinearLayout View_Country_Edit = rootview.findViewById(R.id.View_Country_Edit);
        LinearLayout View_Search = rootview.findViewById(R.id.View_Search);

        if (!Edit) {
            show_country.setVisibility(View.GONE);
            show_distillery.setVisibility(View.GONE);
//            View_bottle_main.setVisibility(View.GONE);
//            View_Save.setVisibility(View.GONE);
            View_bottle_main.setVisibility(View.VISIBLE);
            View_Save.setVisibility(View.VISIBLE);
            _country = null;
            _region = null;
            bottle_info.setDistillery(null);
        } else {
            show_country.setVisibility(View.VISIBLE);
            show_distillery.setVisibility(View.VISIBLE);
            View_bottle_main.setVisibility(View.VISIBLE);
            View_Save.setVisibility(View.VISIBLE);
            View_Country.setVisibility(View.GONE);
            View_Distillery.setVisibility(View.GONE);
            View_Country_Edit.setVisibility(View.VISIBLE);
            View_Search.setVisibility(View.VISIBLE);
            setEditText();
        }
        bottle_info.getBottle().getGUID();
        getActivity().invalidateOptionsMenu();
    }

    private void setEditText() {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();


        EditText ETAge = rootview.findViewById(R.id.ETAge);
        EditText ETABV = rootview.findViewById(R.id.ETABV);
        EditText ETPPM = rootview.findViewById(R.id.ETPPM);
        EditText ETLabel = rootview.findViewById(R.id.ETLabel);
        EditText ETDescription = rootview.findViewById(R.id.ETDescription);
        ImageView IVBottleImage = rootview.findViewById(R.id.IVBottleImage);
        EditText ETNoseDescription = rootview.findViewById(R.id.ETNoseDescription);
        EditText ETTasteDescription = rootview.findViewById(R.id.ETTasteDescription);
        EditText ETFinishDescription = rootview.findViewById(R.id.ETFinishDescription);
        EditText ETBalanceDescription = rootview.findViewById(R.id.ETBalanceDescription);
        CheckBox CBSingleMalt = rootview.findViewById(R.id.CBSingleMalt);

        ETAge.setText(String.valueOf(bottle_info.getBottle().getAGE_OF_WHISKY()));
        ETABV.setText(String.valueOf(bottle_info.getBottle().getABV()));
        ETPPM.setText(String.valueOf(bottle_info.getBottle().getBOTTLE_PPM()));
        ETLabel.setText(bottle_info.getBottle().getBOTTLE_LABEL());
        ETDescription.setText(bottle_info.getBottle().getBOTTLE_DESCRIPTION());
        IVBottleImage.setImageBitmap(bottle_info.getBottle().getBOTTLE_IMAGE());

        ETNoseDescription.setText(bottle_info.getBottle().getNose_Description());
        ETTasteDescription.setText(bottle_info.getBottle().getTaste_Description());
        ETFinishDescription.setText(bottle_info.getBottle().getFinish_Description());
        ETBalanceDescription.setText(bottle_info.getBottle().getBalance_Description());

        if (bottle_info.getBottle().getTYPE() == 1)
            CBSingleMalt.setChecked(true);
        else
            CBSingleMalt.setChecked(false);

    }


    public void onSelected(TCountry country, TRegion region, TDistillery distillery) {
        _country = country;
        _region = region;
        bottle_info.setDistillery(distillery);

        if (distillery != null) {
            //** Country **
            //*************
            LinearLayout View_Country_Edit = rootview.findViewById(R.id.View_Country_Edit);
            View_Country_Edit.setVisibility(View.GONE);

            LinearLayout View_Country = rootview.findViewById(R.id.View_Country);
            View_Country.setVisibility(View.VISIBLE);

            TextView TVCountry_Name = rootview.findViewById(R.id.TVCountry_Name);
            TVCountry_Name.setText(country.getDecription());

            ImageView flag = rootview.findViewById(R.id.IVFlag);
            flag.setImageBitmap(country.getFlag());


            //** Region **
            //************
            TextView TVCountry_Name_Region = rootview.findViewById(R.id.TVCountry_Name_Region);
            if (region != null) {
                TVCountry_Name_Region.setText(region.getDescription().toString());
            } else
                TVCountry_Name_Region.setText("");


            //** Distillery **
            //****************

            LinearLayout Menu_Distillery = rootview.findViewById(R.id.View_Distillery_Edit);
            Menu_Distillery.setVisibility(View.GONE);

            LinearLayout View_Distillery = rootview.findViewById(R.id.View_Distillery);
            View_Distillery.setVisibility(View.VISIBLE);

            TextView TVDistillery_Name = rootview.findViewById(R.id.TVDistillery_Name);
            TextView TVDistlleryGPSN = rootview.findViewById(R.id.TVDistlleryGPSN);
            TextView TVDistlleryGPSE = rootview.findViewById(R.id.TVDistlleryGPSE);
            TextView TVAddress = rootview.findViewById(R.id.TVAddress);
            TextView TVZipcodeCity = rootview.findViewById(R.id.TVZipcodeCity);
            TextView TVPhone = rootview.findViewById(R.id.TVPhone);
            TextView TVNote = rootview.findViewById(R.id.TVNote);


            if (distillery != null) {
                TVDistillery_Name.setText(distillery.getDescription().toString());
                TVDistlleryGPSN.setText(distillery.getGPS_N());
                TVDistlleryGPSE.setText(distillery.getGPS_E());
                TVAddress.setText(distillery.getAddress());
                TVZipcodeCity.setText(distillery.getZipCode());
                TVPhone.setText(distillery.getPhone());
                TVNote.setText(distillery.getNote());
            } else {
                TVDistillery_Name.setText("");
                TVDistlleryGPSN.setText("");
                TVDistlleryGPSE.setText("");
                TVAddress.setText("");
                TVZipcodeCity.setText("");
                TVPhone.setText("");
                TVNote.setText("");
            }


            //** Layout **
            //************
            LinearLayout View_Search = rootview.findViewById(R.id.View_Search);
            View_Search.setVisibility(View.GONE);

            LinearLayout View_bottle_main = rootview.findViewById(R.id.View_bottle_main);
            View_bottle_main.setVisibility(View.VISIBLE);

            LinearLayout View_Save = rootview.findViewById(R.id.View_Save);
            View_Save.setVisibility(View.VISIBLE);
        }
    }

    public void CallBackPhoto(Uri URI) {
        Selected_URI = URI.toString();

        final ImageLoader imageLoader = ImageLoader.getInstance();


        imageLoader.loadImage(Selected_URI, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Log.e("", "");

                Bitmap scaled = Bitmap.createScaledBitmap(loadedImage, Thumbnail_Height, Thumbnail_Width, true);

                ImageView iv = rootview.findViewById(R.id.IVBottleImage);
                iv.setImageBitmap(scaled);
                Selected_Thumbnail = scaled;
            }
        });
    }

    /**
     * Dialog
     */
    private void RateBottleYesNo(final String GUID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(rootview.getContext());

        builder.setTitle("Do you want to rate this bottle?");
        builder.setMessage("The new bottle you have added, do you want to rate this?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Rate_Bottle rate_bottle = new Fragment_Rate_Bottle();
                rate_bottle.rating.setGUID(GUID);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, rate_bottle)
                        .commit();

            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new Fragment_Menu())
                        .commit();


            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    /**
     * Convert string to integer.
     *
     * @param sInt
     * @return integer
     */
    private int getStringToInt(String sInt) {
        int result = 0;
        if (!sInt.isEmpty()) {
            try {
                result = Integer.valueOf(sInt);
            } catch (Exception e) {
                result = 0;
            }

        }
        return result;
    }

    private void addEvents(View view) {
        LinearLayout LL_Search = rootview.findViewById(R.id.LL_Search);
        LL_Search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText ETDistillery = rootview.findViewById(R.id.ETDistillery);
                EditText ETCountry = rootview.findViewById(R.id.ETFilterCountry);
                select_country = new Select_Country_Distillery();
                select_country.addListener(Fragment_Create_Bottle.this, rootview);
                select_country.Show_Dialog_with_Contrys(ETCountry.getText().toString(), new ArrayList<Integer>(), ETDistillery.getText().toString());


            }
        });

        LinearLayout B_Photo = rootview.findViewById(R.id.B_Photo);
        B_Photo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DF_Import_Image newFragment = DF_Import_Image.newInstance(1);
                newFragment.show(getFragmentManager(), "dialog_photo");

//                Split_Items split_items = Split_Items.newInstance(0);
//                split_items.show(getFragmentManager().beginTransaction(), "DialogFragment");


            }
        });

        LinearLayout B_Save = rootview.findViewById(R.id.B_Save);
        B_Save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //  if (_country != null || Edit) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                EditText ETAge = rootview.findViewById(R.id.ETAge);
                EditText ETABV = rootview.findViewById(R.id.ETABV);
                EditText ETPPM = rootview.findViewById(R.id.ETPPM);
                EditText ETLabel = rootview.findViewById(R.id.ETLabel);
                EditText ETDescription = rootview.findViewById(R.id.ETDescription);
                ImageView IVBottleImage = rootview.findViewById(R.id.IVBottleImage);
                EditText ETNoseDescription = rootview.findViewById(R.id.ETNoseDescription);
                EditText ETTasteDescription = rootview.findViewById(R.id.ETTasteDescription);
                EditText ETFinishDescription = rootview.findViewById(R.id.ETFinishDescription);
                EditText ETBalanceDescription = rootview.findViewById(R.id.ETBalanceDescription);
                CheckBox CBSingleMalt = rootview.findViewById(R.id.CBSingleMalt);


                //** ABV **
                //*********
                double ABV = 0;
                try {
                    ABV = Double.parseDouble(ETABV.getText().toString());
                } catch (NumberFormatException ignored) {
                }
                bottle_info.getBottle().setABV(ABV);

                //** Age **
                //*********
                int AGE = 0;
                try {
                    AGE = Integer.valueOf(ETAge.getText().toString());
                } catch (NumberFormatException ignored) {

                }
                bottle_info.getBottle().setAGE_OF_WHISKY(AGE);

                //** PPM **
                //*********
                int PPM = 0;
                try {
                    PPM = Integer.valueOf(ETPPM.getText().toString());
                } catch (NumberFormatException ignored) {

                }
                bottle_info.getBottle().setBOTTLE_PPM(Integer.valueOf(PPM));

                //** Single malt **
                //*****************
                if (CBSingleMalt.isChecked())
                    bottle_info.getBottle().setTYPE(1);
                else
                    bottle_info.getBottle().setTYPE(0);

                DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();


                if (Edit) {
                    if (_country != null) {
                        bottle_info.getBottle().setCOUNTRY_ID(bottle_info.getDistillery().getCOUNTRY_ID());
                        bottle_info.getBottle().setREGION_ID(bottle_info.getDistillery().getREGION_ID());
                    }
                } else {
                    bottle_info.getBottle().setGUID(UUID.randomUUID().toString());
                    if (_country != null)
                        bottle_info.getBottle().setCOUNTRY_ID(_country.getId());

                    if (_region != null)
                        bottle_info.getBottle().setREGION_ID(_region.getId());
                }
                if (_country != null)
                    bottle_info.getBottle().setDistillery_GUID(bottle_info.getDistillery().getGUID());
                else if (!Edit)
                    bottle_info.getBottle().setDistillery_GUID(DRAFT_NAME);

                bottle_info.getBottle().setBOTTLE_DESCRIPTION(ETDescription.getText().toString());
                bottle_info.getBottle().setBOTTLE_LABEL(ETLabel.getText().toString());


                ImageView imageView = new ImageView(rootview.getContext());
                Bitmap bImage = BitmapFactory.decodeResource(rootview.getResources(), R.drawable.ic_camara);

                if (Selected_Thumbnail == null) {
                    if (!Edit)
                        bottle_info.getBottle().setBOTTLE_IMAGE(bImage);
                } else
                    bottle_info.getBottle().setBOTTLE_IMAGE(Selected_Thumbnail);


                bottle_info.getBottle().setNose_Description(ETNoseDescription.getText().toString());
                bottle_info.getBottle().setTaste_Description(ETTasteDescription.getText().toString());
                bottle_info.getBottle().setFinish_Description(ETFinishDescription.getText().toString());
                bottle_info.getBottle().setBalance_Description(ETBalanceDescription.getText().toString());

                DBBottles bottles = new DBBottles(rootview.getContext());
                String GUID = bottles.addBottle(bottle_info.getBottle());

                if (!Edit)
                    RateBottleYesNo(GUID);
                else {
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment_Bottles_Information fragment = new Fragment_Bottles_Information();
                    fragment.bottle_info = bottle_info;
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();
                }

                //   }


            }
        });
/*
        TextView TVBottledDate = (TextView) rootview.findViewById(R.id.TVBottledDate);
        TVBottledDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelectDate(1, 1, 2000, 2);
            }
        });
        TextView TVDistilledDate = (TextView) rootview.findViewById(R.id.TVDistilledDate);
        TVDistilledDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelectDate(1, 1, 2000, 1);
            }
        });
        */
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }
}



