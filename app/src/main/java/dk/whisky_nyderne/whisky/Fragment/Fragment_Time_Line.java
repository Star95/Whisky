package dk.whisky_nyderne.whisky.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.akshaykale.swipetimeline.TimelineFragment;
import com.akshaykale.swipetimeline.TimelineGroupType;
import com.akshaykale.swipetimeline.TimelineObject;
import com.akshaykale.swipetimeline.TimelineObjectClickListener;

import java.util.ArrayList;
import java.util.List;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottles;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Timeline_list;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TimeLineItem;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBRatings;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Color_Rating;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.ImageLoad;
import dk.whisky_nyderne.whisky.Tools.TImage;

import static dk.whisky_nyderne.whisky.Data.TSetup.Thumbnail_Height;

/**
 * Created by hans on 22-09-17.
 */

public class Fragment_Time_Line extends Fragment implements TimelineObjectClickListener {
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_time_line, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents();
        getActivity().invalidateOptionsMenu();
    }


    private void addEvents() {


        UpdateTimelineList timelineList = new UpdateTimelineList();
        timelineList.execute();
    }

    private Bitmap makeImage(Bitmap bitmap,int COUNTRY_ID) {

        Log.e("", String.valueOf(bitmap.getHeight()));
        Bitmap result = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);


        Canvas canvas = new Canvas(result);
        //canvas.drawBitmap(bitmap, 20,0, null);

        canvas.drawBitmap(bitmap, null, new Rect(60, 0, 200, 200), null);

        TCountries contries = TCountries.getInstance(rootview.getContext());
        TCountry country = contries.getInfo(COUNTRY_ID);

       Bitmap flag= contries.getBitmap_as_Bitmap(rootview.getContext(),country.getDrawableID());

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#212121"));
        paint.setStrokeWidth(3);
        canvas.drawRect(0, 0, 60, 200, paint);

       if (flag!=null) {
           canvas.drawBitmap(flag, null, new Rect(0, 0, 60, 40), null);
       }


        canvas.save();
/*        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setTextSize(40);        canvas.rotate(90.0f); //rotates 180 degrees
        canvas.drawText("Time", 10, 10, paint);
        canvas.restore(); //return to 0 degree
*/

        return result;


    }

    private ArrayList<TimelineObject> loadDataInTimeline() {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        List<TBottle_info> list;
        DBBottles dbBottles = new DBBottles(rootview.getContext());
        DBRatings dbRatings = new DBRatings(rootview.getContext());
        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());

        ArrayList<TimelineObject> objs = new ArrayList<>();
        TImage image = TImage.getInstance();
        list = dbBottles.getBottles_TimeLine(0, "");
        for (int i = 0; i <= list.size() - 1; i++) {
            list.get(i).setDistillery(dbDistillery.getDistillery(list.get(i).getBottle().getDistillery_GUID()));
            long longDate = list.get(i).getRating().getDATETIME().getTime();
            String stringbase64 = image.Image2Base64(makeImage(list.get(i).getBottle().getBOTTLE_IMAGE(),list.get(i).getBottle().getCOUNTRY_ID()));
            objs.add(new TimeLineItem(longDate, list.get(i).getBottle().getBOTTLE_LABEL(), stringbase64, list.get(i)));
        }

        return objs;
    }

    private static void removeAllFragments(FragmentManager fragmentManager) {
        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        }
    }

    @Override
    public void onTimelineObjectClicked(TimelineObject timelineObject) {
        Log.e("", "");
        try {
            /*transaction = ((FragmentActivity) rootview.getContext()).getSupportFragmentManager().beginTransaction();
            transaction.remove(mFragment);
            transaction.commit();
            */
            removeAllFragments(getFragmentManager());

            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
            bottle_into.bottle_info = ((TimeLineItem) timelineObject).getBottle_info();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, bottle_into)
                    .commit();


        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

    @Override
    public void onTimelineObjectLongClicked(TimelineObject timelineObject) {
        Log.e("", "");
        FragmentManager fragmentManager = getFragmentManager();
        Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
        top_distillery.GUID = ((TimeLineItem) timelineObject).getBottle_info().getDistillery().getGUID();
        fragmentManager.beginTransaction()
                .replace(R.id.container, top_distillery)
                .commit();

    }


    private class UpdateTimelineList extends AsyncTask<Integer, Void, TimelineFragment> {
        private List<TBottle_info> list;

        @Override
        protected TimelineFragment doInBackground(Integer... urls) {

// instantiate the TimelineFragment
            TimelineFragment mFragment = new TimelineFragment();

            //Set data
            mFragment.setData(loadDataInTimeline(), TimelineGroupType.DAY);
            mFragment.setTimelineHeaderSize(24);
            mFragment.setTimelineHeaderTextColour("WHITE");

            return mFragment;
        }

        @Override
        protected void onPostExecute(final TimelineFragment mFragment) {

            //Set configurations
            // mFragment.addOnClickListener();


            mFragment.setImageLoadEngine(new ImageLoad());

            FragmentTransaction transaction = ((FragmentActivity) rootview.getContext()).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, mFragment);
            transaction.commit();

            ProgressBar progressBarTopList = rootview.findViewById(R.id.progressBarTopList);
            progressBarTopList.setVisibility(View.GONE);

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }

}

