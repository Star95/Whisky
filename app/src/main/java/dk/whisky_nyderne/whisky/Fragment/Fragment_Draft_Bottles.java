package dk.whisky_nyderne.whisky.Fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.Nullable;

import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottles;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.R;

import static dk.whisky_nyderne.whisky.Data.TSetup.DRAFT_NAME;

public class Fragment_Draft_Bottles extends Fragment {

    View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_bottles, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents();
        getActivity().invalidateOptionsMenu();
    }


    private void RateBottleOrView(final TBottle_info bottle_info) {

        AlertDialog.Builder builder = new AlertDialog.Builder(rootview.getContext());

        builder.setTitle("What do you want to do?");
        builder.setMessage("This draft, would you like to rate, view or edit?");

        builder.setPositiveButton("Rate", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Rate_Bottle rate_bottle = new Fragment_Rate_Bottle();
                rate_bottle.rating.setGUID(bottle_info.getBottle().getGUID());
                fragmentManager.beginTransaction()
                        .replace(R.id.container, rate_bottle)
                        .commit();

            }
        });

        builder.setNegativeButton("Show", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                bottle_into.bottle_info = bottle_info;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, bottle_into)
                        .commit();


            }
        });

        builder.setNeutralButton("Delete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                DBBottles dbBottles = new DBBottles(rootview.getContext());
                dbBottles.deleteBottle(bottle_info.getBottle().getGUID());

                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Menu fragment = new Fragment_Menu();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
            }
        });





        AlertDialog alert = builder.create();
        alert.show();

    }



    private void addEvents() {
        ListView LVBottles = (ListView) rootview.findViewById(R.id.LVBottles);
        final List<TBottle_info> list;


        DBBottles dbBottles = new DBBottles(rootview.getContext());
//        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
        list = dbBottles.getBottles_Info(0, DRAFT_NAME);

        Adapter_Bottles customAdapter = new Adapter_Bottles(false);

        for (int i = 0; i <= list.size() - 1; i++) {
            customAdapter.addItem(list.get(i));
        }

        LVBottles.setAdapter(customAdapter);

        LVBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                RateBottleOrView(list.get(position));

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }
}
