package dk.whisky_nyderne.whisky.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottles;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Distillery_Top_List;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Last_24_Hours;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Own_Bottles;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Top_List_mini;
import dk.whisky_nyderne.whisky.Data.TAlcohol;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TBottle_info_Alcohol;
import dk.whisky_nyderne.whisky.Data.TDistilleryTop;
import dk.whisky_nyderne.whisky.Data.THowMuch;
import dk.whisky_nyderne.whisky.Data.TOwner;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBOwnBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBRatings;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBWishList;
import dk.whisky_nyderne.whisky.Dialog_Fragment.DF_Top_Distillery;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.ListView_Tools;

/**
 * Created by hans on 19-09-17.
 */


public class Fragment_Menu extends Fragment {
    private int Total_Days_of_rating = 0;
    private View rootview;
    private static int OpenTable = 0;
    private static int PayTable = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_menu, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProgressBar progressBar = rootview.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        setAlcohol(0, 0);
        addEvents();

        getDaysOfRating daysOfRating = new getDaysOfRating();
        daysOfRating.execute();

        setTopList topList = new setTopList();
        topList.execute();

        setHowMuch howMuch = new setHowMuch();
        howMuch.execute();

        setDistilleryTopList distilleryTopList = new setDistilleryTopList();
        distilleryTopList.execute();

        setWishList wishList = new setWishList();
        wishList.execute();

        setTypeOfDrinks typeOfDrinks = new setTypeOfDrinks();
        typeOfDrinks.execute();

        setLast24Hours last24Hours = new setLast24Hours();
        last24Hours.execute();


        getActivity().invalidateOptionsMenu();

    }

    private class setLast24Hours extends AsyncTask<Integer, Void, Adapter_Last_24_Hours> {
        private List<TBottle_info_Alcohol> bottle_last_24_hours_list;
        private float total_alcohol = 0;
        private float total_gram_alcohol = 0;

        @Override
        protected Adapter_Last_24_Hours doInBackground(Integer... urls) {

            TAlcohol alcohol = new TAlcohol(false, 98);//TODO : Add to setup


            DBRatings dbRatings = new DBRatings(rootview.getContext());

            bottle_last_24_hours_list = dbRatings.getLast24Hours();
            Adapter_Last_24_Hours customAdapter = new Adapter_Last_24_Hours();

            for (int i = 0; i < bottle_last_24_hours_list.size(); i++) {
                alcohol.SetMl(bottle_last_24_hours_list.get(i).getRating().getSize() * 10);
                alcohol.SetProcent((float) bottle_last_24_hours_list.get(i).getBottle().getABV());
                alcohol.SetDatetime(bottle_last_24_hours_list.get(i).getRating().getDATETIME());

                float promille = alcohol.getPromille();

                Log.e("", String.valueOf(promille));
                bottle_last_24_hours_list.get(i).setPromille(promille);
                bottle_last_24_hours_list.get(i).setRestPromille(alcohol.getRestPromilleProcent());
                if (promille > 0)
                    total_alcohol += promille;
                total_gram_alcohol += alcohol.GetGramAlkohol();
                customAdapter.addItem(bottle_last_24_hours_list.get(i));
            }


            return customAdapter;
        }

        @Override
        protected void onPostExecute(Adapter_Last_24_Hours customAdapter) {

            if (bottle_last_24_hours_list.size() > 0) {
            } else {
                LinearLayout View_Last_24Hours = (LinearLayout) rootview.findViewById(R.id.View_Last_24Hours);
                View_Last_24Hours.setVisibility(View.GONE);
            }


            ListView LVLast24Hours = rootview.findViewById(R.id.LVLast24Hours);

            LVLast24Hours.setAdapter(customAdapter);


            LVLast24Hours.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                    bottle_into.bottle_info = bottle_last_24_hours_list.get(position);
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, bottle_into)
                            .commit();

                }
            });

            ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
            lvt.setListViewHeightBasedOnItems(LVLast24Hours);
            setAlcohol(total_alcohol, total_gram_alcohol);

        }
    }


    private class getDaysOfRating extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... urls) {
            DBRatings dbRatings = new DBRatings(rootview.getContext());
            Total_Days_of_rating = dbRatings.getNumberOfDays();

            return 0;
        }

        @Override
        protected void onPostExecute(Integer i) {
            TextView TVDetails_Days_of_rating = rootview.findViewById(R.id.TVDetails_Days_of_rating);
            TVDetails_Days_of_rating.setText(String.valueOf(Total_Days_of_rating));
        }
    }

    private class setTypeOfDrinks extends AsyncTask<Integer, Void, Integer> {
        private int max_value = 0;
        private int Total_types_of_drinks = 0;

        @Override
        protected Integer doInBackground(Integer... urls) {

            DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
            DBRatings dbRatings = new DBRatings(rootview.getContext());


            com.jjoe64.graphview.GraphView Types_of_drinkes = rootview.findViewById(R.id.Types_of_drinkes);


            THowMuch howMuch;


            //** How_Much_Graph **
            //********************
            int this_year = dateTimeFormat.YearFromDateToInteger(new Date());
            int first_year = dbRatings.getFirstYear(this_year);

            BarGraphSeries<DataPoint> series = new BarGraphSeries<>();
            for (int year = first_year; year <= this_year; year++) {
                howMuch = dbRatings.getHowMuchInfo(String.valueOf(year));
                if (max_value < howMuch.getNo_of_Types())
                    max_value = howMuch.getNo_of_Types();
                series.appendData(new DataPoint(year, howMuch.getNo_of_Types()), true, 10);
                Total_types_of_drinks += howMuch.getNo_of_Types();
            }

            if (max_value > 0) {

                series.setColor(Color.parseColor("#339900"));

                series.setSpacing(50);
                series.setDrawValuesOnTop(true);
                series.setValuesOnTopColor(Color.WHITE);

                Types_of_drinkes.getViewport().setScalable(true);
                Types_of_drinkes.getViewport().setScrollable(true);

                Types_of_drinkes.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                    @Override
                    public String formatLabel(double value, boolean isValueX) {
                        if ((int) value != value)
                            return "";
                        else
                            return String.valueOf((int) value);
                    }
                });

                Types_of_drinkes.getViewport().setXAxisBoundsManual(true);
                Types_of_drinkes.getViewport().setMinX(first_year - 1);
                Types_of_drinkes.getViewport().setMaxX(this_year + 1);

                Types_of_drinkes.getViewport().setYAxisBoundsManual(true);
                Types_of_drinkes.getViewport().setMinY(0);
                Types_of_drinkes.getViewport().setMaxY(max_value * 1.05);

                Types_of_drinkes.addSeries(series);
            } else {
                Types_of_drinkes.setVisibility(View.GONE);
            }


            return 0;
        }

        @Override
        protected void onPostExecute(Integer i) {
            TextView TVDetails_Total_Types_of_Drinks = rootview.findViewById(R.id.TVDetails_Total_Types_of_Drinks);
            TVDetails_Total_Types_of_Drinks.setText(String.valueOf(Total_types_of_drinks));

        }
    }


    private class setWishList extends AsyncTask<Integer, Void, Adapter_Top_List_mini> {
        private List<TBottle_info> bottle_top_list;
        private int items = 0;

        @Override
        protected Adapter_Top_List_mini doInBackground(Integer... urls) {

            DBWishList dbWishList = new DBWishList(rootview.getContext());
            bottle_top_list = dbWishList.getWishList();
            items = bottle_top_list.size();
            Adapter_Top_List_mini customAdapter = new Adapter_Top_List_mini();

            for (int i = 0; i < bottle_top_list.size(); i++) {
                customAdapter.addItem(bottle_top_list.get(i));
            }

            return customAdapter;
        }

        @Override
        protected void onPostExecute(final Adapter_Top_List_mini customAdapter) {
            if (items > 0) {
                ListView LVWishList = (ListView) rootview.findViewById(R.id.LVWishList);
                LVWishList.setAdapter(customAdapter);
                LVWishList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                        bottle_into.bottle_info = bottle_top_list.get(position);
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, bottle_into)
                                .commit();

                    }
                });

                ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
                lvt.setListViewHeightBasedOnItems(LVWishList);
            } else {
                LinearLayout View_Wish_list = rootview.findViewById(R.id.View_Wish_list);
                View_Wish_list.setVisibility(View.GONE);
            }

        }
    }


    private class setDistilleryTopList extends AsyncTask<Integer, Void, Adapter_Distillery_Top_List> {
        private List<TDistilleryTop> bottle_top_list;

        @Override
        protected Adapter_Distillery_Top_List doInBackground(Integer... urls) {

            DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
            bottle_top_list = dbDistillery.getTopDistillery(3, "");

            Adapter_Distillery_Top_List customAdapterTop = new Adapter_Distillery_Top_List();

            for (int i = 0; i < bottle_top_list.size(); i++) {
                customAdapterTop.addItem(bottle_top_list.get(i));
            }

            return customAdapterTop;
        }

        @Override
        protected void onPostExecute(final Adapter_Distillery_Top_List customAdapterTop) {

            ListView LVTopDistilleryList = (ListView) rootview.findViewById(R.id.LVTopDistilleryList);
            LVTopDistilleryList.setAdapter(customAdapterTop);
            LVTopDistilleryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                    top_distillery.GUID = bottle_top_list.get(position).getGUID();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, top_distillery)
                            .commit();
                }
            });

            ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
            lvt.setListViewHeightBasedOnItems(LVTopDistilleryList);

        }
    }

    private class setTopList extends AsyncTask<Integer, Void, Adapter_Top_List_mini> {
        private List<TBottle_info> bottle_top_list;

        @Override
        protected Adapter_Top_List_mini doInBackground(Integer... urls) {

            DBBottles dbBottles = new DBBottles(rootview.getContext());
            bottle_top_list = dbBottles.getBottles_Info(3, "");

            Adapter_Top_List_mini customAdapter = new Adapter_Top_List_mini();

            for (int i = 0; i < bottle_top_list.size(); i++) {
                customAdapter.addItem(bottle_top_list.get(i));
            }
            return customAdapter;
        }

        @Override
        protected void onPostExecute(final Adapter_Top_List_mini customAdapter) {

            ListView LVTopBottlesList = (ListView) rootview.findViewById(R.id.LVTopBottlesList);
            LVTopBottlesList.setAdapter(customAdapter);
            LVTopBottlesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                    bottle_into.bottle_info = bottle_top_list.get(position);
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, bottle_into)
                            .commit();

                }
            });

            ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
            lvt.setListViewHeightBasedOnItems(LVTopBottlesList);

        }
    }


    private class setHowMuch extends AsyncTask<Integer, Void, Integer> {
        private int Total_number_of_Drinks = 0;
        private int Total_cl_of_Drinks = 0;

        @Override
        protected Integer doInBackground(Integer... urls) {

            DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
            DBRatings dbRatings = new DBRatings(rootview.getContext());


            com.jjoe64.graphview.GraphView How_Much_Graph = rootview.findViewById(R.id.How_Much_Graph);


            THowMuch howMuch;

            //** How_Much_Graph **
            //********************
            int this_year = dateTimeFormat.YearFromDateToInteger(new Date());
            int first_year = dbRatings.getFirstYear(this_year);

            BarGraphSeries<DataPoint> series = new BarGraphSeries<>();
            int max_value = 0;
            for (int year = first_year; year <= this_year; year++) {
                howMuch = dbRatings.getHowMuchInfo(String.valueOf(year));
                if (max_value < howMuch.getNo_of_Drinks())
                    max_value = howMuch.getNo_of_Drinks();
                series.appendData(new DataPoint(year, howMuch.getNo_of_Drinks()), true, 10);
                Total_number_of_Drinks += howMuch.getNo_of_Drinks();
                Total_cl_of_Drinks += howMuch.getVolume();
            }

            if (max_value > 0) {

                series.setColor(Color.parseColor("#ff9900"));

                series.setSpacing(50);
                series.setDrawValuesOnTop(true);
                series.setValuesOnTopColor(Color.WHITE);

                How_Much_Graph.getViewport().setScalable(true);
                How_Much_Graph.getViewport().setScrollable(true);


                How_Much_Graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                    @Override
                    public String formatLabel(double value, boolean isValueX) {
                        if ((int) value != value)
                            return "";
                        else
                            return String.valueOf((int) value);
                    }
                });

                How_Much_Graph.getViewport().setXAxisBoundsManual(true);
                How_Much_Graph.getViewport().setMinX(first_year - 1);
                How_Much_Graph.getViewport().setMaxX(this_year + 1);

                How_Much_Graph.getViewport().setYAxisBoundsManual(true);
                How_Much_Graph.getViewport().setMinY(0);
                How_Much_Graph.getViewport().setMaxY(max_value * 1.05);

                How_Much_Graph.addSeries(series);
            } else {
                How_Much_Graph.setVisibility(View.GONE);
            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer i) {

            TextView TVDetails_TotalNumberOfDrinks = rootview.findViewById(R.id.TVDetails_TotalNumberOfDrinks);
            TVDetails_TotalNumberOfDrinks.setText(String.valueOf(Total_number_of_Drinks));

            TextView TVDetails_Total_cl = rootview.findViewById(R.id.TVDetails_Total_cl);
            TVDetails_Total_cl.setText(String.valueOf(Total_cl_of_Drinks));

            TextView TVDetails_Averages = rootview.findViewById(R.id.TVDetails_Averages);

            try {
                TVDetails_Averages.setText(String.valueOf(Total_number_of_Drinks / Total_Days_of_rating));
            } catch (Exception e) {
                TVDetails_Averages.setText("0");
            }

        }
    }


    private void setAlcohol(float p, float gram_alcohol) {
        LinearLayout View_Alcohol = rootview.findViewById(R.id.View_Alcohol);
        if (p > 0) {
            TextView TVAlcohol_Procent = rootview.findViewById(R.id.TVAlcohol_Procent);
            TextView TVAlcoholItems = rootview.findViewById(R.id.TVAlcoholItems);
            TextView TVKCAL = rootview.findViewById(R.id.TVKCAL);
            TextView TVKJ = rootview.findViewById(R.id.TVKJ);
            TextView TVAlcohol_Info = rootview.findViewById(R.id.TVAlcohol_Info);
            TVAlcohol_Procent.setText(new DecimalFormat("#.####").format(p) + " ‰");

            try {
                TVAlcoholItems.setText(new DecimalFormat("#.#").format(gram_alcohol / 13) + " " + getString(R.string.Items));
            } catch (Exception e) {
                TVAlcoholItems.setText("");
            }
            TVKCAL.setText("kcal : " + new DecimalFormat("#.#").format(gram_alcohol * 7));
            TVKJ.setText("kj : " + new DecimalFormat("#.#").format(gram_alcohol * 27));

            TVAlcohol_Info.setText("");
            View_Alcohol.setVisibility(View.VISIBLE);
        } else {
            View_Alcohol.setVisibility(View.GONE);
        }


    }


    private void addEvents() {

        ImageView IVTopList = (ImageView) rootview.findViewById(R.id.IVTopList);
        IVTopList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, new Fragment_Top_List())
                                .commit();

                    }
                }).start();
            }
        });

        ImageView IVTopListDistillery = (ImageView) rootview.findViewById(R.id.IVTopListDistillery);
        IVTopListDistillery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        DF_Top_Distillery newFragment = DF_Top_Distillery.newInstance();
                        newFragment.show(getFragmentManager(), "top_distillery");

                    }
                }).start();
            }
        });


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(true);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(true);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(true);
        }
        item = menu.findItem(R.id.action_home);
        if (item != null) {
            item.setVisible(false);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_update: {
                setLast24Hours last24Hours = new setLast24Hours();
                last24Hours.execute();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
