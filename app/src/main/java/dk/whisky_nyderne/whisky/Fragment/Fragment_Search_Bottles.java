package dk.whisky_nyderne.whisky.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.Objects;

import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.R;

/**
 * Created by hans on 22-09-17.
 */

public class Fragment_Search_Bottles extends Fragment {
    public int number;
    public String Label;
    public int Nose;
    public int Tast;
    public int Finish;
    public int Balance;
    public int Total;
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_search_bottles, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents();
        setSwitchOFF();
        getActivity().invalidateOptionsMenu();
    }

    private void setSwitchOFF() {
        final SeekBar SBarNose = (SeekBar) rootview.findViewById(R.id.SBarNose);
        final SeekBar SBarTaste = (SeekBar) rootview.findViewById(R.id.SBarTaste);
        final SeekBar SBarFinish = (SeekBar) rootview.findViewById(R.id.SBarFinish);
        final SeekBar SBarBalance = (SeekBar) rootview.findViewById(R.id.SBarBalance);
        final SeekBar SBarTotal = (SeekBar) rootview.findViewById(R.id.SBarTotal);
        final SeekBar SBarAge = (SeekBar) rootview.findViewById(R.id.SBarAge);

        final TextView TVNoseValue = rootview.findViewById(R.id.TVNoseValue);
        final TextView TVTasteValue = rootview.findViewById(R.id.TVTasteValue);
        final TextView TVFinishValue = rootview.findViewById(R.id.TVFinishValue);
        final TextView TVBalanceValue = rootview.findViewById(R.id.TVBalanceValue);
        final TextView TVTotalValue = rootview.findViewById(R.id.TVTotalValue);
        final TextView TVAgeValue = rootview.findViewById(R.id.TVAgeValue);

        SBarNose.setVisibility(View.GONE);
        SBarTaste.setVisibility(View.GONE);
        SBarFinish.setVisibility(View.GONE);
        SBarBalance.setVisibility(View.GONE);
        SBarTotal.setVisibility(View.GONE);
        SBarAge.setVisibility(View.GONE);

        TVNoseValue.setVisibility(View.GONE);
        TVTasteValue.setVisibility(View.GONE);
        TVFinishValue.setVisibility(View.GONE);
        TVBalanceValue.setVisibility(View.GONE);
        TVTotalValue.setVisibility(View.GONE);
        TVAgeValue.setVisibility(View.GONE);
    }


    private void addEvents() {
        final Switch SWNose = rootview.findViewById(R.id.SWNose);
        final Switch SWTast = rootview.findViewById(R.id.SWTast);
        final Switch SWFinish = rootview.findViewById(R.id.SWFinish);
        final Switch SWBalance = rootview.findViewById(R.id.SWBalance);
        final Switch SWTotal = rootview.findViewById(R.id.SWTotal);
        final Switch SWLimit = rootview.findViewById(R.id.SWLimit);
        final Switch SWDecrease = rootview.findViewById(R.id.SWDecrease);
        final Switch SWRated = rootview.findViewById(R.id.SWRated);
        final Switch SWOnlyBottles = rootview.findViewById(R.id.SWOnlyBottles);
        final Switch SWAge = rootview.findViewById(R.id.SWAge);


        final SeekBar SBarNose = (SeekBar) rootview.findViewById(R.id.SBarNose);
        final SeekBar SBarTaste = (SeekBar) rootview.findViewById(R.id.SBarTaste);
        final SeekBar SBarFinish = (SeekBar) rootview.findViewById(R.id.SBarFinish);
        final SeekBar SBarBalance = (SeekBar) rootview.findViewById(R.id.SBarBalance);
        final SeekBar SBarTotal = (SeekBar) rootview.findViewById(R.id.SBarTotal);
        final SeekBar SBarAge = (SeekBar) rootview.findViewById(R.id.SBarAge);

        final TextView TVNoseValue = rootview.findViewById(R.id.TVNoseValue);
        final TextView TVTasteValue = rootview.findViewById(R.id.TVTasteValue);
        final TextView TVFinishValue = rootview.findViewById(R.id.TVFinishValue);
        final TextView TVBalanceValue = rootview.findViewById(R.id.TVBalanceValue);
        final TextView TVTotalValue = rootview.findViewById(R.id.TVTotalValue);
        final TextView TVAgeValue = rootview.findViewById(R.id.TVAgeValue);


        SWNose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SBarNose.setVisibility(View.VISIBLE);
                    TVNoseValue.setVisibility(View.VISIBLE);
                } else {
                    SBarNose.setVisibility(View.GONE);
                    TVNoseValue.setVisibility(View.GONE);
                }
            }
        });

        SBarNose.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TVNoseValue.setText(String.valueOf(progress * 5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        SWTast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SBarTaste.setVisibility(View.VISIBLE);
                    TVTasteValue.setVisibility(View.VISIBLE);
                } else {
                    SBarTaste.setVisibility(View.GONE);
                    TVTasteValue.setVisibility(View.GONE);
                }
            }
        });

        SBarTaste.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TVTasteValue.setText(String.valueOf(progress * 5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        SWFinish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SBarFinish.setVisibility(View.VISIBLE);
                    TVFinishValue.setVisibility(View.VISIBLE);
                } else {
                    SBarFinish.setVisibility(View.GONE);
                    TVFinishValue.setVisibility(View.GONE);
                }
            }
        });

        SBarFinish.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TVFinishValue.setText(String.valueOf(progress * 5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        SWBalance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SBarBalance.setVisibility(View.VISIBLE);
                    TVBalanceValue.setVisibility(View.VISIBLE);
                } else {
                    SBarBalance.setVisibility(View.GONE);
                    TVBalanceValue.setVisibility(View.GONE);
                }
            }
        });

        SBarBalance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TVBalanceValue.setText(String.valueOf(progress * 5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        SWTotal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SBarTotal.setVisibility(View.VISIBLE);
                    TVTotalValue.setVisibility(View.VISIBLE);
                } else {
                    SBarTotal.setVisibility(View.GONE);
                    TVTotalValue.setVisibility(View.GONE);
                }
            }
        });
        SBarTotal.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TVTotalValue.setText(String.valueOf(progress * 5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        SWAge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SBarAge.setVisibility(View.VISIBLE);
                    TVAgeValue.setVisibility(View.VISIBLE);
                } else {
                    SBarAge.setVisibility(View.GONE);
                    TVAgeValue.setVisibility(View.GONE);
                }
            }
        });
        SBarAge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TVAgeValue.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });



        Button BSearch = (Button) rootview.findViewById(R.id.BSearch);
        BSearch.setOnClickListener(new View.OnClickListener()

        {
            public void onClick(View v) {
                hideKeyboard( (Activity) rootview.getContext());

                new Thread(new Runnable() {

                    @Override
                    public void run() {

                        EditText ETLabel = (EditText) rootview.findViewById(R.id.ETLabel);

                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment_Search_Bottles_Result bottle_list = new Fragment_Search_Bottles_Result();
                        bottle_list.number = 0;
                        if (SWNose.isChecked())
                            bottle_list.Nose = SBarNose.getProgress() * 5;
                        else
                            bottle_list.Nose = 0;

                        if (SWTast.isChecked())
                            bottle_list.Tast = SBarTaste.getProgress() * 5;
                        else
                            bottle_list.Tast = 0;

                        if (SWFinish.isChecked())
                            bottle_list.Finish = SBarFinish.getProgress() * 5;
                        else
                            bottle_list.Finish = 0;

                        if (SWBalance.isChecked())
                            bottle_list.Balance = SBarBalance.getProgress() * 5;
                        else
                            bottle_list.Balance = 0;

                        if (SWTotal.isChecked())
                            bottle_list.Total = SBarTotal.getProgress() * 5;
                        else
                            bottle_list.Total = 0;

                        if (SWAge.isChecked())
                            bottle_list.Age = SBarAge.getProgress() ;
                        else
                            bottle_list.Age = 0;


                        if (SWLimit.isChecked())
                            bottle_list.number = 100;
                        else
                            bottle_list.number = 0;

                        if (SWOnlyBottles.isChecked())
                            bottle_list.OnlyBottles = true;
                        else
                            bottle_list.OnlyBottles = false;

                        bottle_list.Decrease = SWDecrease.isChecked();
                        bottle_list.Rated = SWRated.isChecked();


                        bottle_list.Label = ETLabel.getText().toString();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, bottle_list)
                                .commit();

                    }
                }).start();
            }
        });


    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }
}