package dk.whisky_nyderne.whisky.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.Nullable;

import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottles;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBOwnBottles;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Select_Country_Distillery;
import dk.whisky_nyderne.whisky.Tools.Selected_Country_Distillery_Listener;

public class Fragment_Own_Bottles extends Fragment {
    private boolean OrderByDescription = true;
    private int ShowBottlesType = 3;
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_own_bottles, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addUpdate(OrderByDescription);
        addEvent();
        setText();
        getActivity().invalidateOptionsMenu();
    }

    private void setText() {
        Button BOrderBy = rootview.findViewById(R.id.BOrderBy);
        Button BShowBottleTypes = rootview.findViewById(R.id.BShowBottleTypes);
        if (OrderByDescription)
            BOrderBy.setText("Sort Rank");
        else
            BOrderBy.setText("Sort Des.");
        switch (ShowBottlesType) {
            case 0: {
                BShowBottleTypes.setText("New");
                break;
            }
            case 1: {
                BShowBottleTypes.setText("Open");
                break;
            }
            case 2: {
                BShowBottleTypes.setText("Empty");
                break;
            }
            case 3: {
                BShowBottleTypes.setText("ALL");
                break;
            }
        }

    }

    private void addEvent() {
        Button BOrderBy = rootview.findViewById(R.id.BOrderBy);
        Button BShowBottleTypes = rootview.findViewById(R.id.BShowBottleTypes);
        BOrderBy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OrderByDescription = !OrderByDescription;
                addUpdate(OrderByDescription);
                setText();
            }
        });
        BShowBottleTypes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowBottlesType += 1;
                if (ShowBottlesType > 3)
                    ShowBottlesType = 0;
                addUpdate(OrderByDescription);
                setText();
            }
        });


    }


    private void addUpdate(boolean OrderRank) {
        ListView LVBottles = (ListView) rootview.findViewById(R.id.LVBottles);

        final List<TBottle_info> list;
        DBOwnBottles dbOwnBottles = new DBOwnBottles(rootview.getContext());
        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
        list = dbOwnBottles.getOwnBottlesList(0, OrderRank, ShowBottlesType);

        Adapter_Bottles customAdapter = new Adapter_Bottles(OrderRank);

        for (int i = 0; i <= list.size() - 1; i++) {
            list.get(i).setDistillery(dbDistillery.getDistillery(list.get(i).getBottle().getDistillery_GUID()));
            customAdapter.addItem(list.get(i));
        }
        try {
            LVBottles.setAdapter(customAdapter);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }

        LVBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                bottle_into.bottle_info = list.get(position);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, bottle_into)
                        .commit();
            }
        });
        LVBottles.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                        top_distillery.GUID = list.get(pos).getBottle().getDistillery_GUID();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, top_distillery)
                                .commit();

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_home);
        if (item != null) {
            item.setVisible(true);
        }


    }
}
