package dk.whisky_nyderne.whisky.Fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottles;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Search_Bottle_Distillery;
import dk.whisky_nyderne.whisky.Data.HBottle;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TDistillery;
import dk.whisky_nyderne.whisky.Data.TDistilleryTop;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.R;

public class Fragment_Search_Bottles_Result extends Fragment {

    public int number;
    public String Label;
    public int Nose;
    public int Tast;
    public int Finish;
    public int Balance;
    public int Total;
    public int Age;
    public boolean Decrease;
    public boolean Rated;
    public boolean OnlyBottles;
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_bottles, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UpdateSearch_Bottles_Result updateSearch_bottles_result = new UpdateSearch_Bottles_Result();
        updateSearch_bottles_result.execute();

       // UpdateResult();
        getActivity().invalidateOptionsMenu();
    }



    private class UpdateSearch_Bottles_Result extends AsyncTask<Integer, Void, Adapter_Search_Bottle_Distillery> {
        private List<HBottle> list;
        @Override
        protected Adapter_Search_Bottle_Distillery doInBackground(Integer... urls) {


            final List<TBottle_info> list_bottle;
            list = new ArrayList<HBottle>();
            List<TDistillery> list_distillery = null;
            Adapter_Search_Bottle_Distillery customAdapter = new Adapter_Search_Bottle_Distillery(false);

            final DBDistillery dbDistillery = new DBDistillery(rootview.getContext());

            if (!OnlyBottles)
                if (!Label.isEmpty()) {
                    list_distillery = dbDistillery.getDistillery_Search(Label);
                    for (int i = 0; i <= list_distillery.size() - 1; i++) {
                        HBottle hBottle = new HBottle();
                        hBottle.setType(2);
                        hBottle.setDistillery(list_distillery.get(i));
                        customAdapter.addItem(hBottle);
                        list.add(hBottle);
                    }

                }


            DBBottles dbBottles = new DBBottles(rootview.getContext());
            list_bottle = dbBottles.getSearchBottles(number, Label, Nose, Tast, Finish, Balance, Total,Age, Decrease, Rated);


            for (int i = 0; i <= list_bottle.size() - 1; i++) {
                list_bottle.get(i).setDistillery(dbDistillery.getDistillery(list_bottle.get(i).getBottle().getDistillery_GUID()));
                HBottle hBottle = new HBottle();
                hBottle.setType(1);
                hBottle.setBottle_info(list_bottle.get(i));
                customAdapter.addItem(hBottle);
                list.add(hBottle);
            }


            return customAdapter;
        }

        @Override
        protected void onPostExecute(final Adapter_Search_Bottle_Distillery customAdapter) {

            ListView LVBottles = (ListView) rootview.findViewById(R.id.LVBottles);
            LVBottles.setAdapter(customAdapter);

         //
            LVBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                    switch (list.get(position).getType()) {

                        case 1: {
                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                            bottle_into.bottle_info = list.get(position).getBottle_info();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, bottle_into)
                                    .commit();
                            break;
                        }
                        case 2: {
                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                            top_distillery.GUID = list.get(position).getDistillery().getGUID();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, top_distillery)
                                    .commit();
                            break;
                        }
                    }

                }
            });

            LVBottles.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {
                    switch (list.get(pos).getType()) {

                        case 1: {
                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                            top_distillery.GUID = list.get(pos).getBottle_info().getBottle().getDistillery_GUID();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, top_distillery)
                                    .commit();
                            break;
                        }
                        case 2: {
                            //** Distillery **
                            DeleteDistillery(list.get(pos).getDistillery().getGUID(),list.get(pos).getDistillery().getDescription());
                            break;
                        }
                    }

                    return true;
                }
            });


            ProgressBar progressBottles = rootview.findViewById(R.id.progressBottles);
            progressBottles.setVisibility(View.GONE);

        }
    }




    private void DeleteDistillery(final String GUID, String Description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(rootview.getContext());

        builder.setCancelable(false);
        builder.setMessage("Delete this distillery \""+Description+"\"\nThis can not be undone.");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
                dbDistillery.Delete_Distillery(GUID);

                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Menu menu = new Fragment_Menu();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, menu)
                        .commit();
                dialog.cancel();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();



    }
/*
    private void UpdateResult() {

        ListView LVBottles = (ListView) rootview.findViewById(R.id.LVBottles);
        final List<TBottle_info> list_bottle;
        final List<HBottle> list = new ArrayList<HBottle>();
        List<TDistillery> list_distillery = null;
        Adapter_Search_Bottle_Distillery customAdapter = new Adapter_Search_Bottle_Distillery(false);

        final DBDistillery dbDistillery = new DBDistillery(rootview.getContext());

        if (!OnlyBottles)
            if (!Label.isEmpty()) {
                list_distillery = dbDistillery.getDistillery_Search(Label);
                for (int i = 0; i <= list_distillery.size() - 1; i++) {
                    HBottle hBottle = new HBottle();
                    hBottle.setType(2);
                    hBottle.setDistillery(list_distillery.get(i));
                    customAdapter.addItem(hBottle);
                    list.add(hBottle);
                }

            }


        DBBottles dbBottles = new DBBottles(rootview.getContext());
        list_bottle = dbBottles.getSearchBottles(number, Label, Nose, Tast, Finish, Balance, Total,Age, Decrease, Rated);


        for (int i = 0; i <= list_bottle.size() - 1; i++) {
            list_bottle.get(i).setDistillery(dbDistillery.getDistillery(list_bottle.get(i).getBottle().getDistillery_GUID()));
            HBottle hBottle = new HBottle();
            hBottle.setType(1);
            hBottle.setBottle_info(list_bottle.get(i));
            customAdapter.addItem(hBottle);
            list.add(hBottle);
        }

        LVBottles.setAdapter(customAdapter);

        final List<TDistillery> finalList_distillery = list_distillery;
        LVBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                switch (list.get(position).getType()) {

                    case 1: {
                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                        bottle_into.bottle_info = list.get(position).getBottle_info();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, bottle_into)
                                .commit();
                        break;
                    }
                    case 2: {
                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                        top_distillery.GUID = list.get(position).getDistillery().getGUID();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, top_distillery)
                                .commit();
                        break;
                    }
                }

            }
        });

        LVBottles.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                switch (list.get(pos).getType()) {

                    case 1: {
                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment_Top_Distillery top_distillery = new Fragment_Top_Distillery();
                        top_distillery.GUID = list.get(pos).getBottle_info().getBottle().getDistillery_GUID();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, top_distillery)
                                .commit();
                        break;
                    }
                    case 2: {
                        //** Distillery **
                        DeleteDistillery(list.get(pos).getDistillery().getGUID(),list.get(pos).getDistillery().getDescription());
                        break;
                    }
                }

                return true;
            }
        });


    }
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }
}
