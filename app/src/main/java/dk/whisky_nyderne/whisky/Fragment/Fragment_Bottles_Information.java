package dk.whisky_nyderne.whisky.Fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Date;
import java.util.List;

import dk.whisky_nyderne.countries.TCountries;
import dk.whisky_nyderne.countries.TCountry;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottle_Notes;
import dk.whisky_nyderne.whisky.Adapter.Adapter_Own_Bottles;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.Data.TDistillery;
import dk.whisky_nyderne.whisky.Data.TOwner;
import dk.whisky_nyderne.whisky.Data.TRating;
import dk.whisky_nyderne.whisky.Data.TWish_List;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBOwnBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBRatings;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBWishList;
import dk.whisky_nyderne.whisky.Dialog_Fragment.DF_Ratings;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.Color_Rating;
import dk.whisky_nyderne.whisky.Tools.DateTimeFormat;
import dk.whisky_nyderne.whisky.Tools.ListView_Tools;
import dk.whisky_nyderne.whisky.Tools.TImage;

import static dk.whisky_nyderne.whisky.Data.TSetup.DRAFT_NAME;
import static dk.whisky_nyderne.whisky.Tools.Color_Rating.ColorBad;

/**
 * Created by hans on 20-11-17.
 */

public class Fragment_Bottles_Information extends Fragment {

    //** Public **
    public static TBottle_info bottle_info = new TBottle_info();
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_bottles_information, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ProgressDialog[] progress = new ProgressDialog[1];

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                progress[0] = ProgressDialog.show(rootview.getContext(), "Wait...", "Loading bottle information.", true);
                progress[0].show();
                Looper.loop();
            }
        }).start();


        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
        TDistillery distillery = null;
        if (!bottle_info.getBottle().getDistillery_GUID().equals(DRAFT_NAME))
            distillery = dbDistillery.getDistillery(bottle_info.getBottle().getDistillery_GUID());
        bottle_info.setDistillery(distillery);

        update();
        getActivity().invalidateOptionsMenu();
        progress[0].dismiss();
    }

    private void update() {
        setLayout();
        addEvents();
        setTextInfo();
        setRatings();
        setRatingNote();
        setOwnBottles();
        setRank();
        setWishlist();
    }


    private void setWishlist() {
        DBWishList dbWishList = new DBWishList(rootview.getContext());
        ImageView IVWishList = rootview.findViewById(R.id.IVWishList);

        if (dbWishList.isOnWishlist(bottle_info.getBottle().getGUID()))
            IVWishList.setVisibility(View.VISIBLE);
        else
            IVWishList.setVisibility(View.GONE);
    }

    private void setLayout() {
        if (bottle_info.getDistillery() == null) {
            LinearLayout View_Country = rootview.findViewById(R.id.View_Country);
            LinearLayout View_Distillery = rootview.findViewById(R.id.View_Distillery);

            View_Country.setVisibility(View.GONE);
            View_Distillery.setVisibility(View.GONE);
        }
    }

    private void setOwnBottles() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ListView LVOwnBottles = rootview.findViewById(R.id.LVOwnBottles);

                DBOwnBottles dbOwnBottles = new DBOwnBottles(rootview.getContext());
                final List<TOwner> ownerbottle = dbOwnBottles.getOwnBottles(bottle_info.getBottle().getGUID());

                Adapter_Own_Bottles customAdapter = new Adapter_Own_Bottles();

                for (int i = 0; i < ownerbottle.size(); i++) {
                    customAdapter.addItem(ownerbottle.get(i));
                }
                LVOwnBottles.setAdapter(customAdapter);
                LVOwnBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        Own_Bootle_Menu(ownerbottle.get(position));
                    }
                });
                ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
                lvt.setListViewHeightBasedOnItems(LVOwnBottles);
            }
        });

    }

    private void setRatingNote() {
        LinearLayout View_Rating_Notes = rootview.findViewById(R.id.View_Rating_Notes);
        ListView LVNotes = rootview.findViewById(R.id.LVNotes);
        View_Rating_Notes.setVisibility(View.GONE);
        Boolean use = false;

        DBRatings dbRatings = new DBRatings(rootview.getContext());
        List<TRating> ratings = dbRatings.getRatings(bottle_info.getBottle().getGUID(), true);

        Adapter_Bottle_Notes customAdapter = new Adapter_Bottle_Notes();

        for (int i = 0; i < ratings.size(); i++) {
            if (ratings.get(i).getNOTE() != null && !ratings.get(i).getNOTE().isEmpty()) {
                customAdapter.addItem(ratings.get(i));
                use = true;
            }
        }

        if (use) {
            View_Rating_Notes.setVisibility(View.VISIBLE);
            LVNotes.setAdapter(customAdapter);
            LVNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                }
            });
            ListView_Tools lvt = ListView_Tools.getInstance(rootview.getContext());
            lvt.setListViewHeightBasedOnItems(LVNotes);
        }


    }

    /**
     *
     */
    private void setRatings() {
        LinearLayout View_Rating_chart = rootview.findViewById(R.id.View_Rating_chart);
        View_Rating_chart.setVisibility(View.GONE);

        DBRatings dbRatings = new DBRatings(rootview.getContext());
        List<TRating> ratings = dbRatings.getRatings(bottle_info.getBottle().getGUID(), false);

        GraphView Rating_Graph = rootview.findViewById(R.id.Rating_Graph);
        GraphView Rating_Graph_Nose = rootview.findViewById(R.id.Rating_Graph_Nose);
        GraphView Rating_Graph_Taste = rootview.findViewById(R.id.Rating_Graph_Taste);
        GraphView Rating_Graph_Finish = rootview.findViewById(R.id.Rating_Graph_Finish);
        GraphView Rating_Graph_Balance = rootview.findViewById(R.id.Rating_Graph_Balance);

        Rating_Graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if ((int) value != value)
                    return "";
                else
                    return String.valueOf((int) value);
            }
        });
        Rating_Graph_Nose.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if ((int) value != value)
                    return "";
                else
                    return String.valueOf((int) value);
            }
        });
        Rating_Graph_Taste.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if ((int) value != value)
                    return "";
                else
                    return String.valueOf((int) value);
            }
        });
        Rating_Graph_Finish.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if ((int) value != value)
                    return "";
                else
                    return String.valueOf((int) value);
            }
        });
        Rating_Graph_Balance.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if ((int) value != value)
                    return "";
                else
                    return String.valueOf((int) value);
            }
        });


        LineGraphSeries<DataPoint> series_total = new LineGraphSeries();
        LineGraphSeries<DataPoint> series_nose = new LineGraphSeries();
        LineGraphSeries<DataPoint> series_tast = new LineGraphSeries();
        LineGraphSeries<DataPoint> series_finish = new LineGraphSeries();
        LineGraphSeries<DataPoint> series_balance = new LineGraphSeries();
        series_nose.setTitle("Nose");
        series_tast.setTitle("Tast");
        int total = 0;
        for (int i = 0; i < ratings.size(); i++) {
            total = ratings.get(i).getNOSE() + ratings.get(i).getTAST() + ratings.get(i).getFINISH() + ratings.get(i).getBALANCE();
            series_total.appendData(new DataPoint(i + 1, total), true, ratings.size());
            series_nose.appendData(new DataPoint(i + 1, ratings.get(i).getNOSE()), true, 100);
            series_tast.appendData(new DataPoint(i + 1, ratings.get(i).getTAST()), true, 100);
            series_finish.appendData(new DataPoint(i + 1, ratings.get(i).getFINISH()), true, 100);
            series_balance.appendData(new DataPoint(i + 1, ratings.get(i).getBALANCE()), true, 100);
            View_Rating_chart.setVisibility(View.VISIBLE);
        }
        series_total.setColor(Color.RED);
        series_nose.setColor(Color.GREEN);
        series_tast.setColor(Color.BLUE);
        series_finish.setColor(Color.YELLOW);
        series_balance.setColor(Color.BLACK);

        series_total.setAnimated(true);
        series_nose.setAnimated(true);
        series_tast.setAnimated(true);
        series_finish.setAnimated(true);
        series_balance.setAnimated(true);

        series_total.setDataPointsRadius(5);
        series_nose.setDataPointsRadius(5);
        series_tast.setDataPointsRadius(5);
        series_finish.setDataPointsRadius(5);
        series_balance.setDataPointsRadius(5);
        series_total.setDrawDataPoints(true);
        series_nose.setDrawDataPoints(true);
        series_tast.setDrawDataPoints(true);
        series_finish.setDrawDataPoints(true);
        series_balance.setDrawDataPoints(true);


        Rating_Graph.getViewport().setScalable(true);
        Rating_Graph.getViewport().setScrollable(true);
        Rating_Graph.getViewport().setXAxisBoundsManual(true);
        Rating_Graph.getViewport().setMinX(1);
        Rating_Graph.getViewport().setMaxX(ratings.size() + 1);

        Rating_Graph_Nose.getViewport().setScalable(true);
        Rating_Graph_Nose.getViewport().setScrollable(true);
        Rating_Graph_Nose.getViewport().setXAxisBoundsManual(true);
        Rating_Graph_Nose.getViewport().setMinX(1);
        Rating_Graph_Nose.getViewport().setMaxX(ratings.size() + 1);

        Rating_Graph_Taste.getViewport().setScalable(true);
        Rating_Graph_Taste.getViewport().setScrollable(true);
        Rating_Graph_Taste.getViewport().setXAxisBoundsManual(true);
        Rating_Graph_Taste.getViewport().setMinX(1);
        Rating_Graph_Taste.getViewport().setMaxX(ratings.size() + 1);

        Rating_Graph_Finish.getViewport().setScalable(true);
        Rating_Graph_Finish.getViewport().setScrollable(true);
        Rating_Graph_Finish.getViewport().setXAxisBoundsManual(true);
        Rating_Graph_Finish.getViewport().setMinX(1);
        Rating_Graph_Finish.getViewport().setMaxX(ratings.size() + 1);

        Rating_Graph_Balance.getViewport().setScalable(true);
        Rating_Graph_Balance.getViewport().setScrollable(true);
        Rating_Graph_Balance.getViewport().setXAxisBoundsManual(true);
        Rating_Graph_Balance.getViewport().setMinX(1);
        Rating_Graph_Balance.getViewport().setMaxX(ratings.size() + 1);

        Rating_Graph.addSeries(series_total);

        Rating_Graph_Nose.addSeries(series_nose);
        Rating_Graph_Taste.addSeries(series_tast);
        Rating_Graph_Finish.addSeries(series_finish);
        Rating_Graph_Balance.addSeries(series_balance);
    }

    /**
     * Get rank number from database and write it to rank section.
     */

    private void setRank() {

        LinearLayout View_Rank = rootview.findViewById(R.id.View_Rank);
        RatingBar ratingBar2 = rootview.findViewById(R.id.ratingBar2);


        LayerDrawable stars = (LayerDrawable) ratingBar2.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FFCC00"), PorterDuff.Mode.SRC_ATOP);

        //** Database **
        //**************
        DBRatings dbRatings = new DBRatings(rootview.getContext());
        int no = dbRatings.getRankNo(bottle_info.getBottle().getGUID());

        //** Rank **
        //**********
        TextView TVRank_Description = rootview.findViewById(R.id.TVRank_Description);

        TVRank_Description.setText(String.valueOf(no).toString());

        //** AVG ratings **
        //*****************
        TextView TVRankTotal = rootview.findViewById(R.id.TVRankTotal);
        TextView TVRankNose = rootview.findViewById(R.id.TVRankNose);
        TextView TVRankTast = rootview.findViewById(R.id.TVRankTast);
        TextView TVRankFinish = rootview.findViewById(R.id.TVRankFinish);
        TextView TVRankBalance = rootview.findViewById(R.id.TVRankBalance);

        TVRankTotal.setText(String.valueOf(bottle_info.getRating().getAVG()));
        TVRankNose.setText("N=" + String.valueOf(bottle_info.getRating().getNOSE()));
        TVRankTast.setText("T=" + String.valueOf(bottle_info.getRating().getTAST()));
        TVRankFinish.setText("F=" + String.valueOf(bottle_info.getRating().getFINISH()));
        TVRankBalance.setText("B=" + String.valueOf(bottle_info.getRating().getBALANCE()));

        double avg = bottle_info.getRating().getAVG();
        try {
            ratingBar2.setIsIndicator(true);
            ratingBar2.setRating((float) (avg / 100 * 5));
        } catch (Exception e) {
            ratingBar2.setRating(0);
        }

        GradientDrawable gd;

        //** Total **
        gd = (GradientDrawable) TVRankTotal.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColorTotal(bottle_info.getRating().getAVG())));

        //** Nose **
        gd = (GradientDrawable) TVRankNose.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColor(bottle_info.getRating().getNOSE())));
        //** Tast **
        gd = (GradientDrawable) TVRankTast.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColor(bottle_info.getRating().getTAST())));
        //** Finish **
        gd = (GradientDrawable) TVRankFinish.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColor(bottle_info.getRating().getFINISH())));
        //** Balance **
        gd = (GradientDrawable) TVRankBalance.getBackground().getCurrent();
        gd.setColor(Color.parseColor(Color_Rating.getColor(bottle_info.getRating().getBALANCE())));

    }

    private void setTextInfo() {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        //** Country **
        //*************
        TCountries contries = TCountries.getInstance(rootview.getContext());
        TCountry country = contries.getInfo(bottle_info.getBottle().getCOUNTRY_ID());

        TextView TVCountry_Name = rootview.findViewById(R.id.TVCountry_Name);
        TextView TVCountry_Name_Region = rootview.findViewById(R.id.TVCountry_Name_Region);

        ImageView IVFlag = rootview.findViewById(R.id.IVFlag);
        IVFlag.setImageResource(country.getDrawableID());


        TVCountry_Name.setText(country.getDecription());
        TVCountry_Name_Region.setText("");


        //** Distillery **
        //****************

        TextView TVDistillery_Name = rootview.findViewById(R.id.TVDistillery_Name);
        TextView TVAddress = rootview.findViewById(R.id.TVAddress);
        TextView TVZipcodeCity = rootview.findViewById(R.id.TVZipcodeCity);
        TextView TVPhone = rootview.findViewById(R.id.TVPhone);
        TextView TVDistlleryGPSN = rootview.findViewById(R.id.TVDistlleryGPSN);
        TextView TVDistlleryGPSE = rootview.findViewById(R.id.TVDistlleryGPSE);
        TextView TVNote = rootview.findViewById(R.id.TVNote);

        if (bottle_info.getDistillery() != null) {
            TVDistillery_Name.setText(bottle_info.getDistillery().getDescription());
            TVAddress.setText(bottle_info.getDistillery().getAddress());
            TVZipcodeCity.setText(bottle_info.getDistillery().getZipCode() + " " + bottle_info.getDistillery().getCountry());
            TVPhone.setText("Phone : " + bottle_info.getDistillery().getPhone());
            TVDistlleryGPSN.setText(bottle_info.getDistillery().getGPS_N());
            TVDistlleryGPSE.setText(bottle_info.getDistillery().getGPS_E());
            TVNote.setText(bottle_info.getDistillery().getNote());
        }


        //** Bottle **
        //************

        TextView TV_Lable = rootview.findViewById(R.id.TV_Lable);
        TextView TVLabel = rootview.findViewById(R.id.TVLabel);
        TextView TVABV = rootview.findViewById(R.id.TVABV);
        TextView TVAge = rootview.findViewById(R.id.TVAge);
        TextView TVPPM = rootview.findViewById(R.id.TVPPM);
        TextView TVType = rootview.findViewById(R.id.TVType);
        TextView TVInfo_Description = rootview.findViewById(R.id.TVInfo_Description);


        ImageView IVBottleImage = rootview.findViewById(R.id.IVBottleImage);
        LinearLayout View_Description = rootview.findViewById(R.id.View_Description);


        TV_Lable.setText(bottle_info.getBottle().getBOTTLE_LABEL());
        TVLabel.setText(bottle_info.getBottle().getBOTTLE_LABEL());
        TVABV.setText(String.valueOf(bottle_info.getBottle().getABV()));
        TVAge.setText(String.valueOf(bottle_info.getBottle().getAGE_OF_WHISKY()));
        TVPPM.setText(String.valueOf(bottle_info.getBottle().getBOTTLE_PPM()));
//        IVBottleImage.setImageBitmap(bottle_info.getBottle().getBOTTLE_IMAGE());
        TImage image = TImage.getInstance();
        IVBottleImage.setImageBitmap(image.getRoundedCornerBitmap(bottle_info.getBottle().getBOTTLE_IMAGE(), 16));


        if (bottle_info.getBottle().getTYPE() == 1)
            TVType.setText("Single malt");
        else
            TVType.setText("Blended");

        if (!bottle_info.getBottle().getBOTTLE_DESCRIPTION().isEmpty()) {
            View_Description.setVisibility(View.VISIBLE);
            TVInfo_Description.setText(bottle_info.getBottle().getBOTTLE_DESCRIPTION());
        } else {
            TVInfo_Description.setText("");
            View_Description.setVisibility(View.GONE);
        }

        //** Nose **
        //**********
        TextView TVNoseDescription = rootview.findViewById(R.id.TVNoseDescription);
        if (bottle_info.getBottle().getNose_Description().isEmpty()) {
            LinearLayout View_Nose_Description = rootview.findViewById(R.id.View_Nose_Description);
            TVNoseDescription.setText("");
        } else
            TVNoseDescription.setText(bottle_info.getBottle().getNose_Description());

        //** Taste **
        //***********
        TextView TVTasteDescription = rootview.findViewById(R.id.TVTasteDescription);
        if (bottle_info.getBottle().getTaste_Description().isEmpty()) {
            LinearLayout View_Taste_Description = rootview.findViewById(R.id.View_Taste_Description);
            TVTasteDescription.setText("");
        } else
            TVTasteDescription.setText(bottle_info.getBottle().getTaste_Description());

        //** Finish **
        //************
        TextView TVFinishDescription = rootview.findViewById(R.id.TVFinishDescription);
        if (bottle_info.getBottle().getFinish_Description().isEmpty()) {
            LinearLayout View_Finish_Description = rootview.findViewById(R.id.View_Finish_Description);
            TVFinishDescription.setText("");
        } else
            TVFinishDescription.setText(bottle_info.getBottle().getFinish_Description());

        //** Balance **
        //*************
        TextView TVBalanceDescription = rootview.findViewById(R.id.TVBalanceDescription);
        if (bottle_info.getBottle().getBalance_Description().isEmpty()) {
            LinearLayout View_Balance_Description = rootview.findViewById(R.id.View_Balance_Description);
            TVBalanceDescription.setText("");
        } else
            TVBalanceDescription.setText(bottle_info.getBottle().getBalance_Description());


    }


    private void addEvents() {
        GradientDrawable gd;

        if (bottle_info.getDistillery() != null) {

            //** Wikipedia **
            //***************
            ImageButton BWiki = rootview.findViewById(R.id.BWiki);
            if (bottle_info.getDistillery().getWikipedia().isEmpty())
                BWiki.setVisibility(View.GONE);

            gd = (GradientDrawable) BWiki.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#7f7f7f"));

            BWiki.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            String url = bottle_info.getDistillery().getWikipedia();
                            if (url != null) {
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://" + url;
                                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
                                startActivity(intent);
                            }
                        }
                    }).start();
                }
            });


            //** HomePage **
            //**************
            ImageButton BHomePage = rootview.findViewById(R.id.BHomePage);
            if (bottle_info.getDistillery().getHomePage().isEmpty())
                BHomePage.setVisibility(View.GONE);

            gd = (GradientDrawable) BHomePage.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#7f7f7f"));

            BHomePage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String url = bottle_info.getDistillery().getHomePage();
                            if (!url.startsWith("http://") && !url.startsWith("https://"))
                                url = "http://" + url;
                            Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
                            startActivity(intent);

                        }
                    }).start();
                }
            });


            //** Google maps **
            //*****************
            ImageButton BNavi = rootview.findViewById(R.id.BNavi);
            if (bottle_info.getDistillery().getHomePage().isEmpty())
                BNavi.setVisibility(View.GONE);

            gd = (GradientDrawable) BNavi.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#7f7f7f"));

            BNavi.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            String url = "http://maps.google.com/maps?q=" + bottle_info.getDistillery().getGPS_N() + "," + bottle_info.getDistillery().getGPS_E();
                            if (!url.startsWith("http://") && !url.startsWith("https://"))
                                url = "http://" + url;
                            Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
                            startActivity(intent);

                        }
                    }).start();
                }
            });

            //** Edit Distillery
            ImageView IVEditDistillery = rootview.findViewById(R.id.IVEditDistillery);
            IVEditDistillery.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment_Create_Distillery fragment = new Fragment_Create_Distillery();
                    fragment.Edit = true;
                    fragment.bottle_info = bottle_info;
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();

                }
            });

        }

        //** Ratings **
        //*************
        LinearLayout View_Rating = rootview.findViewById(R.id.B_Ratings);
        View_Rating.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DF_Ratings newFragment = DF_Ratings.newInstance(bottle_info.getBottle().getGUID());
                newFragment.show(getFragmentManager(), "dialog_show_ratings");
            }
        });

        LinearLayout View_Rate = rootview.findViewById(R.id.B_Rate);
        View_Rate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Rate_Bottle rate_bottle = new Fragment_Rate_Bottle();
                rate_bottle.rating = bottle_info.getRating();
                rate_bottle.rating.setGUID(bottle_info.getBottle().getGUID());
                fragmentManager.beginTransaction()
                        .replace(R.id.container, rate_bottle)
                        .commit();
            }
        });

        ImageButton BOwnBottle = rootview.findViewById(R.id.BOwnBottle);
        gd = (GradientDrawable) BOwnBottle.getBackground().getCurrent();
        gd.setColor(Color.parseColor("#7f7f7f"));


        BOwnBottle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addOwnBottle();
            }
        });


        //** Edit bottle
        ImageView IVEditBottle = rootview.findViewById(R.id.IVEditBottle);
        IVEditBottle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Create_Bottle fragment = new Fragment_Create_Bottle();
                fragment.Edit = true;
                fragment.bottle_info = bottle_info;
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();


            }
        });


        final ImageView IVWishList = rootview.findViewById(R.id.IVWishList);
        IVWishList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DBWishList dbWishList = new DBWishList(rootview.getContext());
                dbWishList.deleteWish(bottle_info.getBottle().getGUID());
                IVWishList.setVisibility(View.GONE);
            }
        });

    }

    private void addOwnBottle() {

        final Dialog dialog = new Dialog(rootview.getContext(), android.R.style.Theme_Holo);

        dialog.setContentView(R.layout.dialog_add_own_bottle);
        dialog.setTitle("Add own bottle");

        Button dialog_ok = dialog.findViewById(R.id.dialog_ok);
        Button BCansel = dialog.findViewById(R.id.BCansel);

        final EditText ETSize = dialog.findViewById(R.id.ETSize);
        final EditText ETPrice = dialog.findViewById(R.id.ETPrice);
        final EditText ETNote = dialog.findViewById(R.id.ETNote);

        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        /**
         * Dont save this bottle.
         */
        BCansel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dialog.dismiss();

            }

        });


        /**
         * Save own bottle information
         */
        dialog_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    final double size = Double.valueOf(ETSize.getText().toString());
                    final double Price = Double.valueOf(ETPrice.getText().toString());
                    final String Note = ETNote.getText().toString();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

                            TOwner ownbottle = new TOwner();
                            ownbottle.setBOTTLE_GUID(bottle_info.getBottle().getGUID());
                            ownbottle.setSize(size);
                            ownbottle.setPrice(Price);
                            ownbottle.setNote(Note);
                            ownbottle.setOpen_DateTime(dateTimeFormat.YearMdrDayToDate(2000, 0, 1));
                            ownbottle.setEmpty_Datetime(dateTimeFormat.YearMdrDayToDate(2000, 0, 1));
                            //ownbottle.getUSER_GUID("0000"); //TODO : Add user guid to own bottle, or a friends GUID
                            ownbottle.setType(0);

                            DBOwnBottles dbOwnBottles = new DBOwnBottles(rootview.getContext());
                            dbOwnBottles.addOwneBottle(ownbottle);
                            setOwnBottles();

                        }
                    }).start();
                } catch (Exception ignored) {

                }

                dialog.dismiss();
            }

        });

        dialog.show();

    }

    private void setOpenEmptyBottle(Dialog dialog, String GUID, int type) {
        EditText ETYear = dialog.findViewById(R.id.ETYear);
        EditText ETMonth = dialog.findViewById(R.id.ETMonth);
        EditText ETDay = dialog.findViewById(R.id.ETDay);
        final int Year = Integer.valueOf(ETYear.getText().toString());
        final int Month = Integer.valueOf(ETMonth.getText().toString());
        final int Day = Integer.valueOf(ETDay.getText().toString());
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();
        DBOwnBottles dbOwnBottles = new DBOwnBottles(rootview.getContext());

        switch (type) {
            case 1: {
                dbOwnBottles.setOpenNewBottle(GUID, dateTimeFormat.YearMdrDayToDate(Year, Month - 1, Day));
                break;
            }
            case 2: {
                dbOwnBottles.setEmptyOwnBottle(GUID, dateTimeFormat.YearMdrDayToDate(Year, Month - 1, Day));
                break;
            }

        }
        dialog.dismiss();
        setOwnBottles();


    }

    private void Own_Bootle_Menu(final TOwner ownerbottle) {

        final Dialog dialog = new Dialog(rootview.getContext(), android.R.style.Theme_Holo);

        dialog.setContentView(R.layout.dialog_own_bottle_menu);
        dialog.setTitle("Own bottle menu");

        final EditText ETYear = dialog.findViewById(R.id.ETYear);
        final EditText ETMonth = dialog.findViewById(R.id.ETMonth);
        final EditText ETDay = dialog.findViewById(R.id.ETDay);
        DateTimeFormat dateTimeFormat = DateTimeFormat.getInstance();

        ETYear.setText(dateTimeFormat.YearFromDateToString(new Date()));
        ETMonth.setText(dateTimeFormat.MonthFromDateToString(new Date()));
        ETDay.setText(dateTimeFormat.DayFromDateToString(new Date()));


        //** Set empty date to a open bottle. **
        //**************************************
        Button BEmptyBottle = dialog.findViewById(R.id.BEmptyBottle);
        BEmptyBottle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        setOpenEmptyBottle(dialog, ownerbottle.getGUID(), 2);
                    }
                }).start();
            }
        });

        //** Set open new bottle date  **
        //*******************************

        Button BOpenNewBottle = dialog.findViewById(R.id.BOpenNewBottle);
        BOpenNewBottle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        setOpenEmptyBottle(dialog, ownerbottle.getGUID(), 1);
                    }
                }).start();
            }
        });


        //** Delete own bottle. **
        //************************
        //TODO : Add Yes/No to this function.
        Button BDeleteOwnBottle = dialog.findViewById(R.id.BDeleteOwnBottle);
        BDeleteOwnBottle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        DBOwnBottles dbOwnBottles = new DBOwnBottles(rootview.getContext());
                        dbOwnBottles.Delete_Own_Bottle(ownerbottle.getGUID());
                        dialog.dismiss();
                        setOwnBottles();

                    }
                }).start();
            }
        });


        dialog.show();


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Setup setup = new Fragment_Setup();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, setup)
                    .commit();
            return true;
        }
        if (id == R.id.action_add_wish_list) {
            TWish_List wish_list = new TWish_List();
            wish_list.setBOTTLE_GUID(bottle_info.getBottle().getGUID());
            wish_list.setUSER_GUID("");
            wish_list.setDATETIME(new Date());


            DBWishList dbWishList = new DBWishList(rootview.getContext());
            dbWishList.addWish(wish_list);

            return true;
        }


        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(true);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(true);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(true);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }

    }


}
