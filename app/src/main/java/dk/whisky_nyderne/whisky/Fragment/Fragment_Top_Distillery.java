package dk.whisky_nyderne.whisky.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import java.util.List;

import dk.whisky_nyderne.whisky.Adapter.Adapter_Bottles;
import dk.whisky_nyderne.whisky.Data.TBottle_info;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBBottles;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBDistillery;
import dk.whisky_nyderne.whisky.R;

public class Fragment_Top_Distillery extends Fragment {
    public String GUID;
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_bottles, container, false);
        setHasOptionsMenu(true);
        ProgressBar progressBottles = rootview.findViewById(R.id.progressBottles);
        progressBottles.setVisibility(View.VISIBLE);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //viewDistilleryBottles(GUID);

        setDistilleryBottles distilleryBottles = new setDistilleryBottles();
        distilleryBottles.execute();


        getActivity().invalidateOptionsMenu();
    }


    private class setDistilleryBottles extends AsyncTask<String, Void, Adapter_Bottles> {
        private List<TBottle_info> list;


        @Override
        protected Adapter_Bottles doInBackground(String... not) {

            DBBottles dbBottles = new DBBottles(rootview.getContext());
            DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
            list = dbBottles.getBottles_Info(0, GUID);

            Adapter_Bottles customAdapter = new Adapter_Bottles(false);

            for (int i = 0; i <= list.size() - 1; i++) {
                list.get(i).setDistillery(dbDistillery.getDistillery(list.get(i).getBottle().getDistillery_GUID()));

                customAdapter.addItem(list.get(i));
            }


            return customAdapter;
        }

        @Override
        protected void onPostExecute(final Adapter_Bottles customAdapter) {
            ListView LVBottles = (ListView) rootview.findViewById(R.id.LVBottles);
            LVBottles.setAdapter(customAdapter);

            LVBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                    bottle_into.bottle_info = list.get(position);
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, bottle_into)
                            .commit();
                }
            });
            ProgressBar progressBottles = rootview.findViewById(R.id.progressBottles);
            progressBottles.setVisibility(View.GONE);

        }
    }



    private void viewDistilleryBottles(String GUID) {
        ListView LVBottles = (ListView) rootview.findViewById(R.id.LVBottles);
        final List<TBottle_info> list;


        DBBottles dbBottles = new DBBottles(rootview.getContext());
        DBDistillery dbDistillery = new DBDistillery(rootview.getContext());
        list = dbBottles.getBottles_Info(0, GUID);

        Adapter_Bottles customAdapter = new Adapter_Bottles(false);

        for (int i = 0; i <= list.size() - 1; i++) {
            list.get(i).setDistillery(dbDistillery.getDistillery(list.get(i).getBottle().getDistillery_GUID()));

            customAdapter.addItem(list.get(i));
        }

        LVBottles.setAdapter(customAdapter);

        LVBottles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                FragmentManager fragmentManager = getFragmentManager();
                Fragment_Bottles_Information bottle_into = new Fragment_Bottles_Information();
                bottle_into.bottle_info = list.get(position);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, bottle_into)
                        .commit();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_home);
        if (item != null) {
            item.setVisible(true);
        }


    }
}