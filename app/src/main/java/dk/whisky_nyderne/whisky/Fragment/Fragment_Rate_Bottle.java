package dk.whisky_nyderne.whisky.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.whinc.widget.ratingbar.RatingBar;

import dk.whisky_nyderne.whisky.Data.TRating;
import dk.whisky_nyderne.whisky.Data.TRating_info;
import dk.whisky_nyderne.whisky.DataBase.Bottles.DBRatings;
import dk.whisky_nyderne.whisky.R;
import dk.whisky_nyderne.whisky.Tools.WhiskySizeBar;

import static dk.whisky_nyderne.whisky.R.drawable.ic_trending_down_red_24dp;
import static dk.whisky_nyderne.whisky.R.drawable.ic_trending_up_green_24dp;

/**
 * Created by hans on 27-10-17.
 */

public class Fragment_Rate_Bottle extends Fragment {
    public static TRating rating = new TRating();
    private CountDownTimer countDownTimer;
    private int Rate_Position = 1;
    private View rootview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_rate_bottle, container, false);
        setHasOptionsMenu(true);
        return rootview;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addEvents();
        startCountDown();
        setLayout();

        //** Default value **
        //*******************
        rating.setSize(2);// 2cl size.
        getActivity().invalidateOptionsMenu();
    }

    private void setLayout() {

        setInformatioText(1);
        setNext(1);
    }

    private void StoreRating(int value) {
        switch (Rate_Position) {
            case 1: {
                rating.setNOSE(value);
                break;
            }
            case 2: {
                rating.setTAST(value);
                break;
            }
            case 3: {
                rating.setFINISH(value);
                break;
            }
            case 4: {
                rating.setBALANCE(value);
                break;
            }
        }
    }

    private void addRatingListener(final SmileRating smile, final RatingBar ratingBar, final TextView header) {
        header.setText("1p");

        smile.setNameForSmile(BaseRating.TERRIBLE, "Terrible");
        smile.setNameForSmile(BaseRating.BAD, "Bad");
        smile.setNameForSmile(BaseRating.OKAY, "Okay");
        smile.setNameForSmile(BaseRating.GOOD, "Good");
        smile.setNameForSmile(BaseRating.GREAT, "Great");

        smile.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                // level is from 1 to 5 (0 when none selected)
                // reselected is false when user selects different smiley that previously selected one
                // true when the same smiley is selected.
                // Except if it first time, then the value will be false.
                int value = level * 5 - 5 + ratingBar.getCount();
                header.setText(String.valueOf(value) + "p");
                StoreRating(value);

            }
        });

        ratingBar.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onChange(RatingBar view, int preCount, int curCount) {
                int points = (int) (smile.getRating() * 5 - 5 + curCount);
                header.setText(String.valueOf(points) + "p");
                StoreRating(points);
            }
        });
        smile.setSelectedSmile(BaseRating.TERRIBLE);

    }

    private void addEvents() {
        LinearLayout LLRestart = (LinearLayout) rootview.findViewById(R.id.LLRestart);

        LLRestart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                countDownTimer.cancel();
                startCountDown();
            }
        });


        WhiskySizeBar whiskysizebar = (WhiskySizeBar) rootview.findViewById(R.id.my_rating_bar);

        WhiskySizeBar.StarsRatingBarListener listener = new WhiskySizeBar.StarsRatingBarListener() {
            @Override
            public void rateChanged(float score) {
                TextView TVSize = (TextView) rootview.findViewById(R.id.TVSize);
                TVSize.setText(String.valueOf((int) (score * 2)) + " cl");
                rating.setSize(Integer.valueOf((int) (score*2)));

            }

        };

        whiskysizebar.setStarsRatingBarListener(listener);
        whiskysizebar.setIndicator(false);
        whiskysizebar.setStepByHalf(true);


        LinearLayout B_Right_Button = (LinearLayout) rootview.findViewById(R.id.B_Right_Button);

        B_Right_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Rate_Position += 1;
                setNext(Rate_Position);
            }
        });


        LinearLayout B_Left_Button = (LinearLayout) rootview.findViewById(R.id.B_Left_Button);

        B_Left_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LinearLayout View_Rate_Information = (LinearLayout) rootview.findViewById(R.id.View_Rate_Information);
                TextView TVLeft_Button = (TextView) rootview.findViewById(R.id.TVLeft_Button);
                if (View_Rate_Information.getVisibility() == View.GONE) {
                    View_Rate_Information.setVisibility(View.VISIBLE);
                    TVLeft_Button.setText("Hide info");

                } else {
                    View_Rate_Information.setVisibility(View.GONE);
                    TVLeft_Button.setText("Show info");

                }
            }
        });

        LinearLayout B_Save = (LinearLayout) rootview.findViewById(R.id.B_Save);
        B_Save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                EditText ETNote = (EditText) rootview.findViewById(R.id.ETNote);
                rating.setNOTE(ETNote.getText().toString());

                DBRatings dbRatings = new DBRatings(rootview.getContext());
                dbRatings.addRating(rating);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new Fragment_Menu())
                        .commit();


            }
        });


    }

    private void startCountDown() {
        final TextView TVCountDown = (TextView) rootview.findViewById(R.id.TVCountDown);
        countDownTimer = new CountDownTimer(300000, 1000) {

            public void onTick(long millisUntilFinished) {
                int secs = (int) millisUntilFinished / 1000;
                int mins = secs / 60;
                secs %= 60;

                TVCountDown.setText(String.format("%02d", mins) + ":" + String.format("%02d", secs));
            }

            public void onFinish() {
                LinearLayout View_Timer = (LinearLayout) rootview.findViewById(R.id.View_Timer);
                View_Timer.setVisibility(View.GONE);
                //TODO : Send a notification.
            }
        }.start();


    }

    private void setInformatioText(int type) {
        TextView TVInformationHeader = (TextView) rootview.findViewById(R.id.TVInformationHeader);
        TextView TVInformation = (TextView) rootview.findViewById(R.id.TVInformation);
        TextView TV_Rate_Nose_Value = (TextView) rootview.findViewById(R.id.TV_Rate_Nose_Value);
        TextView TV_Rate_Taste_Value = (TextView) rootview.findViewById(R.id.TV_Rate_Taste_Value);
        TextView TV_Rate_Finish_Value = (TextView) rootview.findViewById(R.id.TV_Rate_Finish_Value);
        TextView TV_Rate_Balance_Value = (TextView) rootview.findViewById(R.id.TV_Rate_Balance_Value);

        switch (type) {

            case 1: {
                TVInformationHeader.setText("Nose");
                TVInformation.setText(R.string.nose_description);


                break;
            }
            case 2: {
                TVInformationHeader.setText("Tast");
                TVInformation.setText(R.string.tast_description);
                TV_Rate_Nose_Value.setText(String.valueOf(rating.getNOSE()) + "p");

                DBRatings dbRatings = new DBRatings(rootview.getContext());
                TRating_info rating_info = dbRatings.getRating_Nose_Info(rating.getGUID(), rating.getNOSE());

                //** Max **
                TextView TVMax_Value = (TextView) rootview.findViewById(R.id.TVNose_Max_Value);
                TVMax_Value.setText(String.valueOf(rating_info.getMax()));
                //** Min **
                TextView TVMin_Value = (TextView) rootview.findViewById(R.id.TVNose_Min_Value);
                TVMin_Value.setText(String.valueOf(rating_info.getMin()));
                //** Rank **
                TextView TVRank_Value = (TextView) rootview.findViewById(R.id.TVNose_Rank_Value);
                TVRank_Value.setText(String.valueOf(rating_info.getRank()));
                //** avg **
                TextView TVAverage_Value = (TextView) rootview.findViewById(R.id.TVNose_Average_Value);
                TVAverage_Value.setText(String.valueOf(rating_info.getAvg()));

                ImageView IVNose_UpDown = (ImageView) rootview.findViewById(R.id.IVNose_UpDown);

                if (rating.getNOSE() == rating_info.getAvg()) {
                    IVNose_UpDown.setVisibility(View.INVISIBLE);
                } else if (rating.getNOSE() < rating_info.getAvg()) {
                    IVNose_UpDown.setVisibility(View.VISIBLE);
                    IVNose_UpDown.setImageResource(ic_trending_down_red_24dp);
                } else {
                    IVNose_UpDown.setVisibility(View.VISIBLE);
                    IVNose_UpDown.setImageResource(ic_trending_up_green_24dp);
                }


                break;
            }
            case 3: {
                TVInformationHeader.setText("Finish");
                TVInformation.setText(R.string.finish_description);
                TV_Rate_Taste_Value.setText(String.valueOf(rating.getTAST()) + "p");

                DBRatings dbRatings = new DBRatings(rootview.getContext());
                TRating_info rating_info = dbRatings.getRating_Tast_Info(rating.getGUID(), rating.getTAST());

                //** Max **
                TextView TVMax_Value = (TextView) rootview.findViewById(R.id.TVTast_Max_Value);
                TVMax_Value.setText(String.valueOf(rating_info.getMax()));
                //** Min **
                TextView TVMin_Value = (TextView) rootview.findViewById(R.id.TVTast_Min_Value);
                TVMin_Value.setText(String.valueOf(rating_info.getMin()));
                //** Rank **
                TextView TVRank_Value = (TextView) rootview.findViewById(R.id.TVTast_Rank_Value);
                TVRank_Value.setText(String.valueOf(rating_info.getRank()));
                //** avg **
                TextView TVAverage_Value = (TextView) rootview.findViewById(R.id.TVTast_Average_Value);
                TVAverage_Value.setText(String.valueOf(rating_info.getAvg()));

                ImageView IVUpDown = (ImageView) rootview.findViewById(R.id.IVTast_UPDOWN);

                if (rating.getTAST() == rating_info.getAvg()) {
                    IVUpDown.setVisibility(View.INVISIBLE);
                } else if (rating.getTAST() < rating_info.getAvg()) {
                    IVUpDown.setVisibility(View.VISIBLE);
                    IVUpDown.setImageResource(ic_trending_down_red_24dp);
                } else {
                    IVUpDown.setVisibility(View.VISIBLE);
                    IVUpDown.setImageResource(ic_trending_up_green_24dp);
                }


                break;
            }
            case 4: {
                TVInformationHeader.setText("Balance");
                TVInformation.setText(R.string.balance_description);
                TV_Rate_Finish_Value.setText(String.valueOf(rating.getFINISH()) + "p");

                DBRatings dbRatings = new DBRatings(rootview.getContext());
                TRating_info rating_info = dbRatings.getRating_Finish_Info(rating.getGUID(), rating.getFINISH());

                //** Max **
                TextView TVMax_Value = (TextView) rootview.findViewById(R.id.TVFinish_Max_Value);
                TVMax_Value.setText(String.valueOf(rating_info.getMax()));
                //** Min **
                TextView TVMin_Value = (TextView) rootview.findViewById(R.id.TVFinish_Min_Value);
                TVMin_Value.setText(String.valueOf(rating_info.getMin()));
                //** Rank **
                TextView TVRank_Value = (TextView) rootview.findViewById(R.id.TVFinish_Rank_Value);
                TVRank_Value.setText(String.valueOf(rating_info.getRank()));
                //** avg **
                TextView TVAverage_Value = (TextView) rootview.findViewById(R.id.TVFinish_Average_Value);
                TVAverage_Value.setText(String.valueOf(rating_info.getAvg()));

                ImageView IVUpDown = (ImageView) rootview.findViewById(R.id.IVFinish_UpDown);

                if (rating.getFINISH() == rating_info.getAvg()) {
                    IVUpDown.setVisibility(View.INVISIBLE);
                } else if (rating.getFINISH() < rating_info.getAvg()) {
                    IVUpDown.setVisibility(View.VISIBLE);
                    IVUpDown.setImageResource(ic_trending_down_red_24dp);
                } else {
                    IVUpDown.setVisibility(View.VISIBLE);
                    IVUpDown.setImageResource(ic_trending_up_green_24dp);
                }


                break;
            }
            case 5: {
                TV_Rate_Balance_Value.setText(String.valueOf(rating.getBALANCE()) + "p");
                DBRatings dbRatings = new DBRatings(rootview.getContext());
                TRating_info rating_info = dbRatings.getRating_Balance_Info(rating.getGUID(), rating.getBALANCE());

                //** Max **
                TextView TVMax_Value = (TextView) rootview.findViewById(R.id.TVBalance_Max_Value);
                TVMax_Value.setText(String.valueOf(rating_info.getMax()));
                //** Min **
                TextView TVMin_Value = (TextView) rootview.findViewById(R.id.TVBalance_Min_Value);
                TVMin_Value.setText(String.valueOf(rating_info.getMin()));
                //** Rank **
                TextView TVRank_Value = (TextView) rootview.findViewById(R.id.TVBalance_Rank_Value);
                TVRank_Value.setText(String.valueOf(rating_info.getRank()));
                //** avg **
                TextView TVAverage_Value = (TextView) rootview.findViewById(R.id.TVBalance_Average_Value);
                TVAverage_Value.setText(String.valueOf(rating_info.getAvg()));

                ImageView IVUpDown = (ImageView) rootview.findViewById(R.id.IVBalance_UpDown);

                if (rating.getBALANCE() == rating_info.getAvg()) {
                    IVUpDown.setVisibility(View.INVISIBLE);
                } else if (rating.getBALANCE() < rating_info.getAvg()) {
                    IVUpDown.setVisibility(View.VISIBLE);
                    IVUpDown.setImageResource(ic_trending_down_red_24dp);
                } else {
                    IVUpDown.setVisibility(View.VISIBLE);
                    IVUpDown.setImageResource(ic_trending_up_green_24dp);
                }


                break;
            }
            default: {

            }

        }


    }

    private void setNext(int type) {
        switch (type) {

            case 1: {
                RatingBar ratingBarValue = (RatingBar) rootview.findViewById(R.id.ratingBarValue_nose);
                SmileRating smile_rating = (SmileRating) rootview.findViewById(R.id.smile_rating_nose);
                TextView TVValue = (TextView) rootview.findViewById(R.id.TVValue_nose);
                addRatingListener(smile_rating, ratingBarValue, TVValue);

                break;
            }
            case 2: {
                LinearLayout View_Size = (LinearLayout) rootview.findViewById(R.id.View_Size);
                LinearLayout View_Rate_Nose_Value = (LinearLayout) rootview.findViewById(R.id.View_Rate_Nose_Value);
                LinearLayout View_Rate_Nose_Result = (LinearLayout) rootview.findViewById(R.id.View_Rate_Nose_Result);
                LinearLayout View_Tast = (LinearLayout) rootview.findViewById(R.id.View_Tast);


                View_Size.setVisibility(View.GONE);
                View_Rate_Nose_Value.setVisibility(View.GONE);
                View_Rate_Nose_Result.setVisibility(View.VISIBLE);
                View_Tast.setVisibility(View.VISIBLE);


                RatingBar ratingBarValue = (RatingBar) rootview.findViewById(R.id.ratingBarValue_tast);
                SmileRating smile_rating = (SmileRating) rootview.findViewById(R.id.smile_rating_tast);
                TextView TVValue = (TextView) rootview.findViewById(R.id.TVValue_tast);
                addRatingListener(smile_rating, ratingBarValue, TVValue);

                break;
            }
            case 3: {
                LinearLayout View_Rate_Tast_Value = (LinearLayout) rootview.findViewById(R.id.View_Rate_Tast_Value);
                LinearLayout View_Rate_Taste_Result = (LinearLayout) rootview.findViewById(R.id.View_Rate_Taste_Result);
                LinearLayout View_Finish = (LinearLayout) rootview.findViewById(R.id.View_Finish);


                View_Rate_Tast_Value.setVisibility(View.GONE);
                View_Rate_Taste_Result.setVisibility(View.VISIBLE);

                View_Finish.setVisibility(View.VISIBLE);


                RatingBar ratingBarValue = (RatingBar) rootview.findViewById(R.id.ratingBarValue_finish);
                SmileRating smile_rating = (SmileRating) rootview.findViewById(R.id.smile_rating_finish);
                TextView TVValue = (TextView) rootview.findViewById(R.id.TVValue_finish);
                addRatingListener(smile_rating, ratingBarValue, TVValue);

                break;
            }
            case 4: {
                LinearLayout View_Rate_Tast_Value = (LinearLayout) rootview.findViewById(R.id.View_Rate_Finish_Value);
                LinearLayout View_Rate_Taste_Result = (LinearLayout) rootview.findViewById(R.id.View_Rate_Finish_Result);
                LinearLayout View_Finish = (LinearLayout) rootview.findViewById(R.id.View_Balance);


                View_Rate_Tast_Value.setVisibility(View.GONE);
                View_Rate_Taste_Result.setVisibility(View.VISIBLE);

                View_Finish.setVisibility(View.VISIBLE);


                RatingBar ratingBarValue = (RatingBar) rootview.findViewById(R.id.ratingBarValue_balance);
                SmileRating smile_rating = (SmileRating) rootview.findViewById(R.id.smile_rating_balance);
                TextView TVValue = (TextView) rootview.findViewById(R.id.TVValue_balance);
                addRatingListener(smile_rating, ratingBarValue, TVValue);

                break;
            }
            case 5: {
                LinearLayout View_Rate_Balance_Value = (LinearLayout) rootview.findViewById(R.id.View_Rate_Balance_Value);
                LinearLayout View_Rate_Balance_Result = (LinearLayout) rootview.findViewById(R.id.View_Rate_Balance_Result);
                LinearLayout View_Note = (LinearLayout) rootview.findViewById(R.id.View_Note);
                LinearLayout View_Rate_Save = (LinearLayout) rootview.findViewById(R.id.View_Rate_Save);
                LinearLayout View_Rate_next = (LinearLayout) rootview.findViewById(R.id.View_Rate_next);


                View_Rate_Balance_Value.setVisibility(View.GONE);
                View_Rate_next.setVisibility(View.GONE);
                View_Rate_Balance_Result.setVisibility(View.VISIBLE);
                View_Rate_Save.setVisibility(View.VISIBLE);

                View_Note.setVisibility(View.VISIBLE);


/*                RatingBar ratingBarValue = (RatingBar) rootview.findViewById(R.id.ratingBarValue_balance);
                SmileRating smile_rating = (SmileRating) rootview.findViewById(R.id.smile_rating_balance);
                TextView TVValue = (TextView) rootview.findViewById(R.id.TVValue_balance);
                addRatingListener(smile_rating,ratingBarValue,TVValue);
*/
                break;
            }
            default: {

            }

        }
        setInformatioText(type);


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_home) {
            FragmentManager fragmentManager = getFragmentManager();
            Fragment_Menu menu = new Fragment_Menu();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, menu)
                    .commit();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem item;

        item = menu.findItem(R.id.action_settings);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_new_Search);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_add_wish_list);
        if (item != null) {
            item.setVisible(false);
        }
        item = menu.findItem(R.id.action_update);
        if (item != null) {
            item.setVisible(false);
        }



    }


}